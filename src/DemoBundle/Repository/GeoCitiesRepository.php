<?php
namespace DemoBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GeoCitiesRepository extends EntityRepository
{

    /**
     *
     * Get all cities by country code
     */
    public function getCitiesByCountry($countryCode)
    {
        $sql = $this->getEntityManager()->createQueryBuilder('d')
            ->select('d.id, d.name')
            ->from('DemoBundle:GeoCities', 'd')
            ->where('d.countryCode = :code')
            ->andwhere('d.published = 1')
            ->setParameter(':code', $countryCode)
            ->groupBy('d.id')
            ->orderBy('d.name', 'ASC');
            
        $query  = $sql->getQuery();
        $result = $query->getResult();
        return $result;
    }
}
