<?php
namespace DemoBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GeoCountriesRepository extends EntityRepository
{
    /**
     *
     * Get all countries lists
     */
    public function getCountriesList()
    {
        $sql = $this->getEntityManager()->createQueryBuilder('d')
            ->select('d.code, d.name')
            ->from('DemoBundle:GeoCountries', 'd')
            ->where('d.published = 1')
            ->orderBy('d.name', 'ASC');
        $query  = $sql->getQuery();
        $result = $query->getResult();
        return $result;
    }
}