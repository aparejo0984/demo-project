<?php

namespace DemoBundle\Entity;

/**
 * Cities
 *
 * @ORM\Table(name="geocities")
 * @ORM\Entity(repositoryClass="DemoBundle\Repository\GeoCitiesRepository")
 */
class GeoCities
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var string
     */
    private $stateCode;

    /**
     * @var string
     */
    private $accent;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var string
     */
    private $timezoneid;

    /**
     * @var integer
     */
    private $orderDisplay;

    /**
     * @var string
     */
    private $fromSource;

    /**
     * @var integer
     */
    private $popularity;

    /**
     * @var \DateTime
     */
    private $lastModified;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=false)
     */
    private $published;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return WebGeoCities
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set stateCode
     *
     * @param string $stateCode
     *
     * @return WebGeoCities
     */
    public function setStateCode($stateCode)
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * Get stateCode
     *
     * @return string
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }

    /**
     * Set accent
     *
     * @param string $accent
     *
     * @return WebGeoCities
     */
    public function setAccent($accent)
    {
        $this->accent = $accent;

        return $this;
    }

    /**
     * Get accent
     *
     * @return string
     */
    public function getAccent()
    {
        return $this->accent;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WebGeoCities
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return WebGeoCities
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return WebGeoCities
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set timezoneid
     *
     * @param string $timezoneid
     *
     * @return WebGeoCities
     */
    public function setTimezoneid($timezoneid)
    {
        $this->timezoneid = $timezoneid;

        return $this;
    }

    /**
     * Get timezoneid
     *
     * @return string
     */
    public function getTimezoneid()
    {
        return $this->timezoneid;
    }

    /**
     * Set orderDisplay
     *
     * @param integer $orderDisplay
     *
     * @return WebGeoCities
     */
    public function setOrderDisplay($orderDisplay)
    {
        $this->orderDisplay = $orderDisplay;

        return $this;
    }

    /**
     * Get orderDisplay
     *
     * @return integer
     */
    public function getOrderDisplay()
    {
        return $this->orderDisplay;
    }

    /**
     * Set fromSource
     *
     * @param string $fromSource
     *
     * @return WebGeoCities
     */
    public function setFromSource($fromSource)
    {
        $this->fromSource = $fromSource;

        return $this;
    }

    /**
     * Get fromSource
     *
     * @return string
     */
    public function getFromSource()
    {
        return $this->fromSource;
    }

    /**
     * Set popularity
     *
     * @param integer $popularity
     *
     * @return WebGeoCities
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;

        return $this;
    }

    /**
     * Get popularity
     *
     * @return integer
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     *
     * @return WebGeoCities
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set published
     *
     * @param string $published
     *
     * @return string
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get flag icon
     *
     * @return string
     */
    public function getPublished()
    {
        return $this->published;
    }
}

