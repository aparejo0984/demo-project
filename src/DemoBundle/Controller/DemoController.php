<?php

namespace DemoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends DefaultController
{

    /**
     * Landing page
     * 
     */
    public function forecastAction(Request $request)
    {
        $this->data['countries'] = $this->get('DemoService')->getCountriesList();
        return $this->render('@DemoBundle/Resources/views/index.html.twig', $this->data);
    }

    /**
     * AJAX Call for getting cities
     * 
     */
    public function getCitiesAction(Request $request)
    {
        $post   = $request->request->all();
        $response = $this->get('DemoService')->getCitiesByCountry($post['country']);

        $result = array();
        $result['success'] = $response['success'];
        $result['html'] = $response['option'];
        $response          = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * AJAX Call for getting weather forecast
     * 
     */
    public function getWeatherForecastAction(Request $request)
    {
        $post   = $request->request->all();
        $response = $this->get('DemoService')->getAPICallResponse($post);

        $result = array();
        $result['success'] = $response['success'];
        if($response['success']){
            $this->data['results'] = $response;
            $result['html'] = $return['output'] = $this->render('@DemoBundle/Resources/views/datatable.html.twig', $this->data)->getContent();
        }else{
            $result['message'] = 'No results found';
        }
        
        $response          = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
