<?php

namespace DemoBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class DemoService
{
    protected $container;
    protected $em;
    
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container    = $container;
        $this->em = $em;
    }
    
    /**
     *
     * Get all countries lists
     */
    public function getCountriesList()
    {
        return $this->em->getRepository('DemoBundle:GeoCountries')->getCountriesList();
    }

    /**
     *
     * Get cities by country code
     */
    public function getCitiesByCountry($countryCode)
    {
        $result = array();
        $result['success'] = true;

        $cities = $this->em->getRepository('DemoBundle:GeoCities')->getCitiesByCountry($countryCode);

        $option = '<option value=""> Select City </option>';
        if($cities){
            foreach($cities as $key => $value){
                $option .= '<option value="'.$value['id'].'">'.$value['name'].'</option>'; 
            }
        }else{
            $result['success'] = false;
            $option = '<option value=""> No cities found </option>'; 
        }

        $result['option'] = $option;
        return $result;
    }

    /**
     *
     * Get API response
     */
    public function getAPICallResponse($params)
    {
        if(isset($params['city']) && $params['city']){
            $city = $params['city'];
            $url = "http://api.openweathermap.org/data/2.5/forecast?id=$city&APPID=8654b0cebff1c1674f2c5b679feb62e7&units=metric&mode=xml";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
            $data = curl_exec($ch);
            curl_close($ch);

            return $this->formatXMLResponse($data);
        }else{
            $result = array();
            $result['success'] = false;
            return $result;
        }
    }

    /**
     * Format XML response
     * 
     */
    public function formatXMLResponse($xmlresponse)
    {
        $result = array();
        $result['success'] = true;

        $domDoc = new \DOMDocument('1.0', 'UTF-8');
        try {
            $domDoc->loadXml($xmlresponse);
        } catch (\Exception $e) {
            $result['success'] = false;
            return $result;
        }
        $xpath = new \DOMXPath($domDoc);

        $location = $domDoc->getElementsByTagName('location');

        $result['cityname'] = $xpath->evaluate('string(./name)', $location->item(0));
        $result['countryName'] = $xpath->evaluate('string(./country)', $location->item(0));
        $result['longitude'] = $xpath->evaluate('string(./location/@longitude)', $location->item(0));
        $result['latitude'] = $xpath->evaluate('string(./location/@latitude)', $location->item(0));
        $result['altitude'] = $xpath->evaluate('string(./location/@altitude)', $location->item(0));

        $sunrise = $xpath->evaluate('string(/weatherdata/sun/@rise)');
        $sunset = $xpath->evaluate('string(/weatherdata/sun/@set)');

        $result['sunrise'] = date_format(new \DateTime($sunrise),"h:i A");
        $result['sunset'] = date_format(new \DateTime($sunset),"h:i A");

        $forecastItems = array();
        $forecast = $domDoc->getElementsByTagName('forecast');
        $forecastTimes = $forecast->item(0)->getElementsByTagName('time');

        if ($forecastTimes && $forecastTimes->length > 0) {
            $items = array();
            foreach ($forecastTimes as $num => $time) {
                $from  = $xpath->evaluate('string(./@from)', $time);
                $to  = $xpath->evaluate('string(./@to)', $time);

                $dateFromObj = new \DateTime($from);
                $dateFrom = date_format($dateFromObj,'Y-m-d');
                $dayFrom = date_format($dateFromObj,"D");
                $timeFrom = date_format($dateFromObj,"h:i A");
                $items['from']  = $dayFrom . ' ' . $dateFrom. ' ' . $timeFrom;

                $dateToObj = new \DateTime($to);
                $dateTo = date_format($dateToObj,'Y-m-d');
                $dayTo = date_format($dateToObj,"D");
                $timeTo = date_format($dateToObj,"h:i A");
                $items['to'] = $dayTo . ' ' . $dateTo. ' ' . $timeTo;
                
                $windDirectiondeg  = $xpath->evaluate('string(./windDirection/@deg)', $time);
                $windDirectionname  = $xpath->evaluate('string(./windDirection/@name)', $time);
                $items['windDirection'] = $windDirectionname . ' ' . $windDirectiondeg;

                $windSpeedmps  = $xpath->evaluate('string(./windSpeed/@mps)', $time);
                $windSpeedname  = $xpath->evaluate('string(./windSpeed/@name)', $time);
                $items['windSpeed'] = $windSpeedname . ' ' . $windSpeedmps;

                $temperatureunit  = $xpath->evaluate('string(./temperature/@unit)', $time);
                $temperaturevalue  = $xpath->evaluate('string(./temperature/@value)', $time);
                $items['temperature'] = $temperaturevalue . ' ' . $temperatureunit;

                $pressureunit  = $xpath->evaluate('string(./pressure/@unit)', $time);
                $pressurevalue  = $xpath->evaluate('string(./pressure/@value)', $time);
                $items['pressure'] = $pressurevalue . ' ' . $pressureunit;

                $humidityunit  = $xpath->evaluate('string(./humidity/@unit)', $time);
                $humidityvalue  = $xpath->evaluate('string(./humidity/@value)', $time);
                $items['humidity']  = $humidityvalue . '' . $humidityunit;

                $items['clouds']  = ucfirst($xpath->evaluate('string(./clouds/@value)', $time));

                $forecastItems[] = $items;
            }
        }else{
            $result['success'] = false;
            $result['message'] = 'No Results Found';
        }
        $result['forecastItems'] = array_slice($forecastItems,-5);
        return $result;
    }
}
