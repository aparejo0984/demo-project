-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 04:35 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `geocities`
--

CREATE TABLE `geocities` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `state_code` varchar(20) DEFAULT NULL,
  `accent` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `timezoneid` varchar(255) DEFAULT NULL,
  `order_display` int(11) NOT NULL DEFAULT '0',
  `from_source` varchar(10) NOT NULL DEFAULT 'old',
  `popularity` int(11) NOT NULL DEFAULT '0',
  `last_modified` datetime DEFAULT NULL,
  `name_fulltext` varchar(255) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geocities`
--

INSERT INTO `geocities` (`id`, `country_code`, `state_code`, `accent`, `name`, `latitude`, `longitude`, `timezoneid`, `order_display`, `from_source`, `popularity`, `last_modified`, `name_fulltext`, `published`) VALUES
(1, 'AD', '04', 'Xixerella', 'Xixerella', 42.553298, 1.48736, 'Europe/Andorra                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(2, 'AD', '03', 'Vila', 'Vila', 42.531799, 1.56654, 'Europe/Andorra                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(3, 'AD', '05', 'SornÃ s', 'Sornas', 42.565399, 1.528699, 'Europe/Andorra                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(4, 'AD', '02', 'Soldeu', 'Soldeu', 42.5769, 1.66769, 'Europe/Andorra                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(5, 'BR', '21', 'Rocinha', 'Rocinha', -22.990999, -43.2499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(6, 'BR', '21', 'Vidigal', 'Vidigal', -22.9939, -43.240299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(7, 'BR', '21', 'Jardim BotÃ£nico', 'Jardim Botanico', -22.962099, -43.2233, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(8, 'BR', '21', 'HumaitÃ¡', 'Humaita', -22.957399, -43.199199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(9, 'BR', '21', 'Quinta Da Boa Vista', 'Quinta Da Boa Vista', -22.9057, -43.2247, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(10, 'BR', '21', 'Vasco Da Gama', 'Vasco Da Gama', -22.891799, -43.227798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(11, 'BR', '21', 'Benfica', 'Benfica', -22.8917, -43.240898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(12, 'BR', '21', 'MarÃ©', 'Mare', -22.856, -43.243499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(13, 'BR', '21', 'Santo Cristo', 'Santo Cristo', -22.898599, -43.201301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(14, 'BR', '21', 'Gamboa', 'Gamboa', -22.8976, -43.192798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(15, 'BR', '21', 'Cidade Nova', 'Cidade Nova', -22.9116, -43.2005, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(16, 'BR', '21', 'EstÃ¡cio', 'Estacio', -22.918199, -43.203998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(17, 'BR', '21', 'GragoatÃ¡', 'Gragoata', -22.899, -43.1296, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(18, 'BR', '21', 'Jardim Guanabara', 'Jardim Guanabara', -22.815599, -43.199798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(19, 'BR', '21', 'BancÃ¡rios', 'Bancarios', -22.7915, -43.18, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(20, 'BR', '21', 'TauÃ¡', 'Taua', -22.7943, -43.186298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(21, 'BR', '21', 'Jardim Carioca', 'Jardim Carioca', -22.8029, -43.192501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(22, 'BR', '21', 'Cacuia', 'Cacuia', -22.8097, -43.190898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(23, 'BR', '21', 'MonerÃ³', 'Monero', -22.7971, -43.199298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(24, 'BR', '21', 'Parque Anchieta', 'Parque Anchieta', -22.8271, -43.415199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(25, 'BR', '21', 'Catete', 'Catete', -22.925399, -43.1814, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(26, 'BR', '21', 'PrÃ¡Ã§a Da Bandeira', 'Praca Da Bandeira', -22.911899, -43.214801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(27, 'BR', '21', 'Campos De Membeca', 'Campos De Membeca', -22.425899, -44.484199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(28, 'BR', '21', 'AcÃ¡cias', 'Acacias', -22.44, -44.322799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(29, 'BR', '21', 'Aman', 'Aman', -22.433399, -44.4062, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(30, 'BR', '21', 'Ponte Do Souza', 'Ponte Do Souza', -22.2674, -44.394199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(31, 'BR', '21', 'SÃ­tio Pedra Selada', 'Sitio Pedra Selada', -22.276199, -44.438999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(32, 'BR', '21', 'Campo Alegre', 'Campo Alegre', -22.293899, -44.482398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(33, 'BR', '21', 'SÃ­tio Elias', 'Sitio Elias', -22.309499, -44.4319, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(34, 'BR', '21', 'SÃ­tio Boa EsperanÃ§a', 'Sitio Boa Esperanca', -22.3437, -44.467201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(35, 'BR', '21', 'Santa Isabel', 'Santa Isabel', -22.4769, -44.487098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(36, 'BR', '21', 'Bairro SÃ£o Caetano', 'Bairro Sao Caetano', -22.4584, -44.478401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(37, 'BR', '21', 'Bairro De Itapuca', 'Bairro De Itapuca', -22.474, -44.478298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(38, 'BR', '21', 'Bairro Surubi', 'Bairro Surubi', -22.4664, -44.4323, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(39, 'BR', '21', 'SÃ­tio Do Tucum', 'Sitio Do Tucum', -22.491699, -44.4449, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(40, 'BR', '21', 'SÃ­tio Do IpÃª', 'Sitio Do Ipe', -22.448299, -44.4109, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(41, 'BR', '21', 'SÃ­tio Palmeiras', 'Sitio Palmeiras', -22.316799, -44.347801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(42, 'BR', '21', 'SÃ­tio Quilombo', 'Sitio Quilombo', -22.3131, -44.391101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(43, 'BR', '21', 'SÃ­tio Serrinha', 'Sitio Serrinha', -22.2929, -44.408699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(44, 'BR', '21', 'SÃ­tio Aldeia Dos Ãndios', 'Sitio Aldeia Dos Indios', -22.281999, -44.323699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(45, 'BR', '21', 'SÃ­tio Bela Vista', 'Sitio Bela Vista', -22.2816, -44.312599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(46, 'BR', '21', 'SÃ­tio Boa Vista', 'Sitio Boa Vista', -22.4234, -44.279701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(47, 'BR', '21', 'SÃ­tio Mato Dentro', 'Sitio Mato Dentro', -22.757099, -44.165298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(48, 'BR', '21', 'SÃ­tio Bom Retiro', 'Sitio Bom Retiro', -22.769699, -44.166999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(49, 'BR', '21', 'SÃ­tio SÃ£o CristÃ³vÃ£o', 'Sitio Sao Cristovao', -22.8033, -44.186199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(50, 'BR', '21', 'IbicuÃ­', 'Ibicui', -22.960599, -44.026599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(51, 'BR', '21', 'Saco', 'Saco', -22.9414, -44.0419, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(52, 'BR', '27', 'Loteamento Chcaras Mont Clair', 'Loteamento Chcaras Mont Clair', -23.254699, -46.230598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(53, 'BR', '27', 'Stio Quintas Da Lagoa', 'Stio Quintas Da Lagoa', -23.2625, -46.232299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(54, 'BR', '27', 'Stio Pequeno Mundo', 'Stio Pequeno Mundo', -23.2674, -46.216098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(55, 'BR', '27', 'Stio Novo Horizonte', 'Stio Novo Horizonte', -23.2672, -46.214, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(56, 'BR', '27', 'Stio Pau Torto', 'Stio Pau Torto', -23.258699, -46.201301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(57, 'BR', '27', 'Stio So Roque', 'Stio So Roque', -23.257999, -46.189998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(58, 'BR', '27', 'Stio Bernardo', 'Stio Bernardo', -23.251399, -46.183998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(59, 'BR', '27', 'Stio Joaquim Fernandes', 'Stio Joaquim Fernandes', -23.257099, -46.180099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(60, 'BR', '27', 'Stio Joo Felcio', 'Stio Joo Felcio', -23.265499, -46.182598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(61, 'BR', '27', 'Stio Augusto Fernandes', 'Stio Augusto Fernandes', -23.274099, -46.1833, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(62, 'BR', '27', 'Stio Cachoeira', 'Stio Cachoeira', -23.2779, -46.182098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(63, 'BR', '27', 'Stio Avelino Rodrigues', 'Stio Avelino Rodrigues', -23.2812, -46.1758, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(64, 'BR', '27', 'Bairro Varadouro', 'Bairro Varadouro', -23.277, -46.189998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(65, 'BR', '27', 'Vila Sangamon', 'Vila Sangamon', -23.2868, -46.212299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(66, 'BR', '27', 'Loteamento Miguel Cruz', 'Loteamento Miguel Cruz', -23.274599, -46.2304, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(67, 'BR', '27', 'Stio Bela Vista', 'Stio Bela Vista', -23.2805, -46.236698, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(68, 'BR', '27', 'Stio Dos Piles', 'Stio Dos Piles', -23.2782, -46.239398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(69, 'BR', '27', 'Ouro Fino', 'Ouro Fino', -23.282899, -46.245098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(70, 'BR', '27', 'Stio Ouro Fino', 'Stio Ouro Fino', -23.2898, -46.247001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(71, 'BR', '27', 'Residencial Eldorado', 'Residencial Eldorado', -23.3068, -46.214298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(72, 'BR', '27', 'Jardim Portugal', 'Jardim Portugal', -23.303499, -46.215599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(73, 'BR', '27', 'Estncia Zelia', 'Estncia Zelia', -23.2989, -46.224498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(74, 'BR', '27', 'Chcaras Eldorado', 'Chcaras Eldorado', -23.2922, -46.2159, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(75, 'BR', '27', 'Stio Santa Edwiges', 'Stio Santa Edwiges', -23.295499, -46.200599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(76, 'BR', '27', 'Varadouro', 'Varadouro', -23.2991, -46.1996, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(77, 'BR', '27', 'Stio Gacho', 'Stio Gacho', -23.302, -46.202098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(78, 'BR', '27', 'Stio Carlos Rangue', 'Stio Carlos Rangue', -23.290899, -46.170101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(79, 'BR', '27', 'Stio Das Palmeiras', 'Stio Das Palmeiras', -23.299999, -46.182701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(80, 'BR', '27', 'Stio Santa Gertrudes', 'Stio Santa Gertrudes', -23.319299, -46.177101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(81, 'BR', '27', 'Stio Morro Grande', 'Stio Morro Grande', -23.3236, -46.195301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(82, 'BR', '27', 'Stio Santo Antnio Da Bela Vista', 'Stio Santo Antnio Da Bela Vista', -23.322, -46.2047, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(83, 'BR', '27', 'Jardim Cristina', 'Jardim Cristina', -23.320999, -46.209201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(84, 'BR', '27', 'Jardim Das Accias', 'Jardim Das Accias', -23.3148, -46.2042, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(85, 'BR', '27', 'Monte Negro', 'Monte Negro', -23.313199, -46.197601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(86, 'BR', '27', 'Vila Osris', 'Vila Osris', -23.314699, -46.231601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(87, 'BR', '27', 'Stio Joana Da Arc', 'Stio Joana Da Arc', -23.317699, -46.246898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(88, 'BR', '27', 'Stio Das Moendas', 'Stio Das Moendas', -23.325, -46.241798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(89, 'BR', '27', 'Stio Porta Do Cu', 'Stio Porta Do Cu', -23.334299, -46.2467, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(90, 'BR', '27', 'Stio Cantagalo', 'Stio Cantagalo', -23.3369, -46.246498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(91, 'BR', '27', 'Stio Rancho Fundo', 'Stio Rancho Fundo', -23.339399, -46.249198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(92, 'BR', '27', 'Stio Berro Dâ€™ Agua', 'Stio Berro D\' Agua', -23.342199, -46.239398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(93, 'BR', '27', 'Geremuniz', 'Geremuniz', -23.341999, -46.222099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(94, 'BR', '27', 'Jardim Novo Dem', 'Jardim Novo Dem', -23.337499, -46.2243, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(95, 'BR', '27', 'Stio Trs Chicos', 'Stio Trs Chicos', -23.326499, -46.192798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(96, 'BR', '27', 'Stio Portela', 'Stio Portela', -23.3288, -46.187, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(97, 'BR', '27', 'Stio Chegancia', 'Stio Chegancia', -23.345399, -46.182098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(98, 'BR', '27', 'Veraneio Cury', 'Veraneio Cury', -23.356399, -46.1926, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(99, 'BR', '27', 'Stio Santa Ceclia', 'Stio Santa Ceclia', -23.3609, -46.175201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(100, 'BR', '27', 'Stio So Martins', 'Stio So Martins', -23.3581, -46.200901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(101, 'BR', '27', 'Stio So Pedro', 'Stio So Pedro', -23.3575, -46.204898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(102, 'BR', '27', 'Bairro Geremuniz', 'Bairro Geremuniz', -23.3554, -46.208198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(103, 'BR', '27', 'Stio Santa Teresinha', 'Stio Santa Teresinha', -23.344299, -46.209899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(104, 'BR', '27', 'Stio Santo Antnio', 'Stio Santo Antnio', -23.346799, -46.2178, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(105, 'BR', '27', 'Stio So Jos', 'Stio So Jos', -23.3579, -46.215, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(106, 'BR', '27', 'Bairro Cafundo', 'Bairro Cafundo', -23.356399, -46.235, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(107, 'BR', '27', 'Stio Dâ€™ Anna', 'Stio D\' Anna', -23.36, -46.248298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(108, 'BR', '27', 'Stio Santa Cruz', 'Stio Santa Cruz', -23.358299, -46.247398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(109, 'BR', '27', 'Chcaras Boa Vista', 'Chcaras Boa Vista', -23.3546, -46.2402, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(110, 'BR', '27', 'Recanto Sete Estrelas', 'Recanto Sete Estrelas', -23.3479, -46.240001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(111, 'BR', '27', 'Stio Aguas Lucema', 'Stio Aguas Lucema', -23.361499, -46.2476, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(112, 'BR', '27', 'Stio Gluglu', 'Stio Gluglu', -23.378099, -46.2448, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(113, 'BR', '27', 'Stio Riacho', 'Stio Riacho', -23.377099, -46.229301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(114, 'BR', '27', 'Rancho Da Pamonha', 'Rancho Da Pamonha', -23.365499, -46.224899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(115, 'BR', '27', 'Recanto Trs Marias', 'Recanto Trs Marias', -23.362199, -46.2159, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(116, 'BR', '27', 'Stio So Bernardino', 'Stio So Bernardino', -23.362699, -46.207698, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(117, 'BR', '27', 'Bairro Lambari', 'Bairro Lambari', -23.367099, -46.180099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(118, 'BR', '27', 'Stio Cerro Azul', 'Stio Cerro Azul', -23.3696, -46.180801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(119, 'BR', '27', 'Stio Morro Verde', 'Stio Morro Verde', -23.3854, -46.188301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(120, 'BR', '27', 'Bairro Tabuo', 'Bairro Tabuo', -23.398599, -46.239601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(121, 'BR', '27', 'Stio Papa-Vento', 'Stio Papa-Vento', -23.397699, -46.180801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(122, 'BR', '27', 'Stio Rosane', 'Stio Rosane', -23.408199, -46.234401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(123, 'BR', '27', 'Stio Rosana', 'Stio Rosana', -23.422599, -46.176898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(124, 'BR', '27', 'Stio Icari', 'Stio Icari', -23.422899, -46.1804, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(125, 'BR', '27', 'Stio Ip', 'Stio Ip', -23.422399, -46.189998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(126, 'BR', '27', 'Tabuo', 'Tabuo', -23.421199, -46.239799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(127, 'BR', '27', 'Bairro So Bento', 'Bairro So Bento', -23.436599, -46.2425, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(128, 'BR', '27', 'Stio Gondim', 'Stio Gondim', -23.441699, -46.2476, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(129, 'BR', '27', 'Stio Betnia', 'Stio Betnia', -23.442899, -46.246601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(130, 'BR', '27', 'Stio So Bento', 'Stio So Bento', -23.443899, -46.2397, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(131, 'BR', '27', 'Stio Florana', 'Stio Florana', -23.4454, -46.201801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(132, 'BR', '27', 'Stio Do Carmo', 'Stio Do Carmo', -23.4484, -46.207199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(133, 'BR', '27', 'Stio Solidariedade', 'Stio Solidariedade', -23.446199, -46.192401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(134, 'BR', '27', 'Stio Rancho Grande', 'Stio Rancho Grande', -23.451599, -46.177101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(135, 'BR', '27', 'Stio Santa Rita', 'Stio Santa Rita', -23.4531, -46.1991, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(136, 'BR', '27', 'Stio Do Professor', 'Stio Do Professor', -23.4547, -46.228298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(137, 'BR', '27', 'Stio Santa Helena', 'Stio Santa Helena', -23.461099, -46.226398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(138, 'BR', '27', 'Stio Ouro Verde', 'Stio Ouro Verde', -23.464399, -46.227199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(139, 'BR', '27', 'Stio Caracol', 'Stio Caracol', -23.4526, -46.240501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(140, 'BR', '27', 'Stio Moinho Velho', 'Stio Moinho Velho', -23.4701, -46.226799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(141, 'BR', '27', 'Stio Do Vov', 'Stio Do Vov', -23.4843, -46.2271, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(142, 'BR', '27', 'Stio Quatro Estrelas', 'Stio Quatro Estrelas', -23.480499, -46.216999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(143, 'BR', '27', 'Stio Lagoa De Itapeti', 'Stio Lagoa De Itapeti', -23.488599, -46.180999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(144, 'BR', '27', 'Jardim Maric', 'Jardim Maric', -23.495399, -46.1814, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(145, 'BR', '27', 'Stio Dois Irmos', 'Stio Dois Irmos', -23.4948, -46.192901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(146, 'BR', '27', 'Stio Rosa-Dos-Ventos', 'Stio Rosa-Dos-Ventos', -23.492, -46.203498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(147, 'BR', '27', 'Stio Serra Do Itapeti', 'Stio Serra Do Itapeti', -23.4934, -46.217399, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(148, 'BR', '27', 'Stio Itapeti', 'Stio Itapeti', -23.4986, -46.217998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(149, 'BR', '27', 'Bairro Da Capelinha', 'Bairro Da Capelinha', -23.4887, -46.231098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(150, 'BR', '27', 'Vila Suia', 'Vila Suia', -23.499099, -46.145198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(151, 'BR', '27', 'Stio Recanto Alegre', 'Stio Recanto Alegre', -23.489799, -46.095901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(152, 'BR', '27', 'Stio So Sebastio', 'Stio So Sebastio', -23.481599, -46.098701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(153, 'BR', '27', 'Stio Capelinha', 'Stio Capelinha', -23.4831, -46.126899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(154, 'BR', '27', 'Vila So Paulo', 'Vila So Paulo', -23.4806, -46.127201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(155, 'BR', '27', 'Loteamento Chcaras Cruzeiro Do Sul', 'Loteamento Chcaras Cruzeiro Do Sul', -23.462299, -46.130901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(156, 'BR', '27', 'Stio Florida', 'Stio Florida', -23.468, -46.123199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(157, 'BR', '27', 'Stio Da Gara', 'Stio Da Gara', -23.468099, -46.1193, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(158, 'BR', '27', 'Stio Eldorado', 'Stio Eldorado', -23.462499, -46.112499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(159, 'BR', '27', 'Itapeti Do Salto', 'Itapeti Do Salto', -23.448999, -46.097301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(160, 'BR', '27', 'Stio So Francisco De Assis', 'Stio So Francisco De Assis', -23.440399, -46.093299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(161, 'BR', '27', 'Stio Itapetizinho', 'Stio Itapetizinho', -23.447599, -46.117198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(162, 'BR', '27', 'Stio Marli', 'Stio Marli', -23.450199, -46.133899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(163, 'BR', '27', 'Stio So Joo', 'Stio So Joo', -23.435699, -46.151, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(164, 'BR', '27', 'Bairro Beija-Flor', 'Bairro Beija-Flor', -23.434099, -46.152999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(165, 'BR', '27', 'Stio So Lzaro', 'Stio So Lzaro', -23.442499, -46.166198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(166, 'BR', '27', 'Stio Cruzeiro', 'Stio Cruzeiro', -23.4503, -46.1678, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(167, 'BR', '27', 'Stio So Francisco', 'Stio So Francisco', -23.4244, -46.164199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(168, 'BR', '27', 'Stio Vov Joo', 'Stio Vov Joo', -23.4218, -46.164501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(169, 'BR', '27', 'Stio Marieta', 'Stio Marieta', -23.417699, -46.161598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(170, 'BR', '27', 'Stio Vista Alegre', 'Stio Vista Alegre', -23.430099, -46.1049, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(171, 'BR', '27', 'Bairro Itapeti', 'Bairro Itapeti', -23.4328, -46.104099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(172, 'BR', '27', 'Stio Das Laranjeiras', 'Stio Das Laranjeiras', -23.4234, -46.071701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(173, 'BR', '27', 'Stio Trs Lagos', 'Stio Trs Lagos', -23.408, -46.120899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(174, 'BR', '27', 'Stio Toli', 'Stio Toli', -23.4141, -46.125801, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(175, 'BR', '27', 'Stio Fonte Da Serra', 'Stio Fonte Da Serra', -23.399, -46.1263, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(176, 'BR', '27', 'Stio A. Colossal', 'Stio A. Colossal', -23.387599, -46.162601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(177, 'BR', '27', 'Chcara Kabu', 'Chcara Kabu', -23.365499, -46.158, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(178, 'BR', '27', 'Colonia Itapeti', 'Colonia Itapeti', -23.3633, -46.173198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(179, 'BR', '27', 'Stio Senhora Do Pranto', 'Stio Senhora Do Pranto', -23.3735, -46.145999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(180, 'BR', '27', 'Capela Do Bom Jesus', 'Capela Do Bom Jesus', -23.3761, -46.140499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(181, 'BR', '27', 'Stio Turco', 'Stio Turco', -23.3628, -46.131, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(182, 'BR', '27', 'Stio Pedro Brasilio', 'Stio Pedro Brasilio', -23.370199, -46.121601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(183, 'BR', '27', 'Parque Agrinco', 'Parque Agrinco', -23.377099, -46.104301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(184, 'BR', '27', 'Stio Guapor', 'Stio Guapor', -23.3687, -46.095798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(185, 'BR', '27', 'Stio Francisco Missina', 'Stio Francisco Missina', -23.37, -46.092498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(186, 'BR', '27', 'Stio Trs Netinhos', 'Stio Trs Netinhos', -23.371599, -46.086299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(187, 'BR', '27', 'Bairro Goiabal', 'Bairro Goiabal', -23.3505, -46.081501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(188, 'BR', '27', 'Recanto Taor Mina', 'Recanto Taor Mina', -23.3435, -46.080501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(189, 'BR', '27', 'Stio Do Joca', 'Stio Do Joca', -23.353799, -46.100898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(190, 'BR', '27', 'Stio Branca De Neve', 'Stio Branca De Neve', -23.3469, -46.1305, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(191, 'BR', '27', 'Stio Marianos', 'Stio Marianos', -23.348899, -46.1333, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(192, 'BR', '27', 'Bairro Maracatu', 'Bairro Maracatu', -23.3574, -46.120201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(193, 'BR', '27', 'Stio Do Portugus', 'Stio Do Portugus', -23.347999, -46.165699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(194, 'BR', '27', 'Stio Sawada', 'Stio Sawada', -23.3269, -46.116401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(195, 'BR', '27', 'Stio Alvorada', 'Stio Alvorada', -23.3292, -46.117, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(196, 'BR', '27', 'Stio So Lus', 'Stio So Lus', -23.3369, -46.113201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(197, 'BR', '27', 'Stio Sohiro Fuji', 'Stio Sohiro Fuji', -23.326499, -46.100601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(198, 'BR', '27', 'Stio Divineas', 'Stio Divineas', -23.329799, -46.094799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(199, 'BR', '27', 'Stio Jos Siqueira', 'Stio Jos Siqueira', -23.330499, -46.092899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(200, 'BR', '27', 'Chcara Tsurumatsu Seki', 'Chcara Tsurumatsu Seki', -23.3421, -46.087398, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(201, 'BR', '27', 'Recanto Campestre', 'Recanto Campestre', -23.342399, -46.082199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(202, 'BR', '27', 'Recanto Pingol', 'Recanto Pingol', -23.3192, -46.094001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(203, 'BR', '27', 'Bairro Jardim Parate', 'Bairro Jardim Parate', -23.318, -46.105701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(204, 'BR', '27', 'Stio So Cristvo', 'Stio So Cristvo', -23.323299, -46.111698, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(205, 'BR', '27', 'Stio Pouso De Iemanj', 'Stio Pouso De Iemanj', -23.311, -46.1175, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(206, 'BR', '27', 'Stio Primavera', 'Stio Primavera', -23.308399, -46.139598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(207, 'BR', '27', 'Stio Santa Maria', 'Stio Santa Maria', -23.308, -46.157901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(208, 'BR', '27', 'Bairro Figueira', 'Bairro Figueira', -23.290899, -46.127101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(209, 'BR', '27', 'Stio Nelson Melo', 'Stio Nelson Melo', -23.296699, -46.123199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(210, 'BR', '27', 'Stio Geraldo Monteiro', 'Stio Geraldo Monteiro', -23.306999, -46.117099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(211, 'BR', '27', 'Stio Jos Pereira', 'Stio Jos Pereira', -23.3034, -46.108699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(212, 'BR', '27', 'Stio Jos Raposo', 'Stio Jos Raposo', -23.3052, -46.1063, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(213, 'BR', '27', 'Stio Beija-Flor', 'Stio Beija-Flor', -23.2866, -46.086299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(214, 'BR', '27', 'Stio Pedro S. Lima', 'Stio Pedro S. Lima', -23.2851, -46.111, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(215, 'BR', '27', 'Stio Nossa Senhora Das Brotas', 'Stio Nossa Senhora Das Brotas', -23.2826, -46.111198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(216, 'BR', '27', 'Stio Maria B. Vilela', 'Stio Maria B. Vilela', -23.2803, -46.119998, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(217, 'BR', '27', 'Stio Joo Salvador Do Prado', 'Stio Joo Salvador Do Prado', -23.275499, -46.127498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(218, 'BR', '27', 'Bairro Pinhal', 'Bairro Pinhal', -23.271999, -46.1305, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(219, 'BR', '27', 'Stio Francisco Ramos', 'Stio Francisco Ramos', -23.2777, -46.1492, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(220, 'BR', '27', 'Stio Edgar Ramos', 'Stio Edgar Ramos', -23.272699, -46.162899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(221, 'BR', '27', 'Stio Maria R. Soares', 'Stio Maria R. Soares', -23.277, -46.159999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(222, 'BR', '27', 'Stio Schavoa', 'Stio Schavoa', -23.2887, -46.153198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(223, 'BR', '27', 'Stio Joo C. Morais', 'Stio Joo C. Morais', -23.287399, -46.1655, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(224, 'BR', '27', 'Stio Santa Filomena', 'Stio Santa Filomena', -23.284, -46.169498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(225, 'BR', '27', 'Stio Noel Ramos', 'Stio Noel Ramos', -23.2786, -46.1669, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(226, 'BR', '27', 'Stio Camilo S. Barbosa', 'Stio Camilo S. Barbosa', -23.267999, -46.167598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(227, 'BR', '27', 'Stio Euderico Fernandes', 'Stio Euderico Fernandes', -23.2644, -46.171901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(228, 'BR', '27', 'Bairro Santo Angelo', 'Bairro Santo Angelo', -23.269399, -46.1552, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(229, 'BR', '27', 'Stio Regina', 'Stio Regina', -23.253299, -46.160999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(230, 'BR', '27', 'Stio Paulo Rodrigues', 'Stio Paulo Rodrigues', -23.2607, -46.152698, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(231, 'BR', '27', 'Stio Jos Silvrio Neto', 'Stio Jos Silvrio Neto', -23.2572, -46.1459, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(232, 'BR', '27', 'Stio Anibal Rodrigues', 'Stio Anibal Rodrigues', -23.256, -46.138099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(233, 'BR', '27', 'Stio Boa Esperana', 'Stio Boa Esperana', -23.270599, -46.137798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(234, 'BR', '27', 'Stio Josino B. Santos', 'Stio Josino B. Santos', -23.258399, -46.128799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(235, 'BR', '27', 'Stio Homero Evangelista', 'Stio Homero Evangelista', -23.2516, -46.109298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(236, 'BR', '27', 'Stio Pedro M. Gonalves', 'Stio Pedro M. Gonalves', -23.260499, -46.117599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(237, 'BR', '27', 'Stio Pedro Gandu', 'Stio Pedro Gandu', -23.264499, -46.1203, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(238, 'BR', '27', 'Stio Domingos Elias De Oliveira', 'Stio Domingos Elias De Oliveira', -23.2672, -46.1095, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(239, 'BR', '27', 'Stio Vicente L. Silva', 'Stio Vicente L. Silva', -23.2623, -46.111301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(240, 'BR', '27', 'Stio Clio Jos Dos Santos', 'Stio Clio Jos Dos Santos', -23.257999, -46.113601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(241, 'BR', '27', 'Stio Mrcio Silvrio', 'Stio Mrcio Silvrio', -23.2509, -46.096298, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(242, 'BR', '27', 'Stio Benedito Aparecido Rodrigues', 'Stio Benedito Aparecido Rodrigues', -23.252799, -46.090499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(243, 'BR', '27', 'Stio Boa Vista', 'Stio Boa Vista', -23.2637, -46.082698, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(244, 'BR', '27', 'Stio Santa Luzia', 'Stio Santa Luzia', -23.266099, -46.0596, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(245, 'BR', '27', 'Stio Mariano De Sousa', 'Stio Mariano De Sousa', -23.263599, -46.050201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(246, 'BR', '27', 'Stio Yoshio Kinoghita', 'Stio Yoshio Kinoghita', -23.266, -46.051898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(247, 'BR', '27', 'Stio Koki Fukugana', 'Stio Koki Fukugana', -23.264299, -46.026699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(248, 'BR', '27', 'Stio Antnio Dionzio', 'Stio Antnio Dionzio', -23.2614, -46.030101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(249, 'BR', '27', 'Stio Jos Benedito De Oliveira', 'Stio Jos Benedito De Oliveira', -23.2555, -46.0209, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(250, 'BR', '27', 'Stio Narcsio De Oliveira', 'Stio Narcsio De Oliveira', -23.2518, -46.0228, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(251, 'BR', '27', 'Stio Veso Ferreira', 'Stio Veso Ferreira', -23.270599, -46.018501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(252, 'BR', '27', 'Stio Sonezuzi Ozeki', 'Stio Sonezuzi Ozeki', -23.2845, -46.005401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(253, 'BR', '27', 'Bairro Lagoinhas', 'Bairro Lagoinhas', -23.2894, -46.0055, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(254, 'BR', '27', 'Stio Bonanza', 'Stio Bonanza', -23.2887, -46.017299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(255, 'BR', '27', 'Stio Irineu H. Aquino', 'Stio Irineu H. Aquino', -23.2838, -46.0289, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(256, 'BR', '27', 'Stio Nadir Rustan', 'Stio Nadir Rustan', -23.2861, -46.036201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(257, 'BR', '27', 'Stio Das Flores', 'Stio Das Flores', -23.272499, -46.0401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(258, 'BR', '27', 'Stio Ana Martins', 'Stio Ana Martins', -23.2754, -46.0489, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(259, 'BR', '27', 'Stio Sheiva Anashiro', 'Stio Sheiva Anashiro', -23.2816, -46.044799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(260, 'BR', '27', 'Bairro Santa Ana', 'Bairro Santa Ana', -23.29, -46.058799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(261, 'BR', '27', 'Recanto Tanaka', 'Recanto Tanaka', -23.2956, -46.061901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(262, 'BR', '27', 'Recanto Amarelo', 'Recanto Amarelo', -23.3027, -46.063499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(263, 'BR', '27', 'Stio Geraldo A. Alvino', 'Stio Geraldo A. Alvino', -23.3019, -46.059501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(264, 'BR', '27', 'Recanto Santa Ana', 'Recanto Santa Ana', -23.2975, -46.0512, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(265, 'BR', '27', 'Loteamento Ijal', 'Loteamento Ijal', -23.2915, -46.0456, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(266, 'BR', '27', 'Stio Antnio H. Aquino', 'Stio Antnio H. Aquino', -23.29, -46.0293, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(267, 'BR', '27', 'Stio Joo Vayne', 'Stio Joo Vayne', -23.2926, -46.019599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(268, 'BR', '27', 'Esperana', 'Esperana', -23.315, -46.011199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(269, 'BR', '27', 'Matadouro', 'Matadouro', -23.3178, -46.0223, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(270, 'BR', '27', 'Stio So Judas Tadeu', 'Stio So Judas Tadeu', -23.3155, -46.020401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(271, 'BR', '27', 'Stio So Jorge', 'Stio So Jorge', -23.309299, -46.019599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(272, 'BR', '27', 'Stio Quatro Cantos', 'Stio Quatro Cantos', -23.3155, -46.028301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(273, 'BR', '27', 'Stio Pinga-Pinga', 'Stio Pinga-Pinga', -23.323799, -46.031501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(274, 'BR', '27', 'Stio Joo B. Camargo', 'Stio Joo B. Camargo', -23.319299, -46.073898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(275, 'BR', '27', 'Stio Tabapu', 'Stio Tabapu', -23.336999, -46.060798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(276, 'BR', '27', 'Stio Chapado', 'Stio Chapado', -23.333499, -46.056301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(277, 'BR', '27', 'Stio Rosa Maria Santana', 'Stio Rosa Maria Santana', -23.332099, -46.034999, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(278, 'BR', '27', 'Stio Dacio Piazza', 'Stio Dacio Piazza', -23.3365, -46.0288, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(279, 'BR', '27', 'Stio Agenor', 'Stio Agenor', -23.341299, -46.027301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(280, 'BR', '27', 'Stio Antnio Luis Siqueira', 'Stio Antnio Luis Siqueira', -23.3299, -46.0153, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(281, 'BR', '27', 'Stio Pacincia', 'Stio Pacincia', -23.3546, -46.009601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(282, 'BR', '27', 'Stio Piratininga', 'Stio Piratininga', -23.3561, -46.0111, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(283, 'BR', '27', 'Stio Edmundo Sousa', 'Stio Edmundo Sousa', -23.35, -46.022899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(284, 'BR', '27', 'Stio Orlando Dos Santos', 'Stio Orlando Dos Santos', -23.3462, -46.025299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(285, 'BR', '27', 'Stio Avelino Motta', 'Stio Avelino Motta', -23.3533, -46.0232, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(286, 'BR', '27', 'Stio Marclio Navarro', 'Stio Marclio Navarro', -23.358699, -46.0242, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(287, 'BR', '27', 'Stio Josefa M. Conceio', 'Stio Josefa M. Conceio', -23.3586, -46.0415, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(288, 'BR', '27', 'Stio Oliveira', 'Stio Oliveira', -23.346099, -46.0457, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(289, 'BR', '27', 'Stio Amrico Correa', 'Stio Amrico Correa', -23.345899, -46.056701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(290, 'BR', '27', 'Stio Do Valter', 'Stio Do Valter', -23.357799, -46.043498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(291, 'BR', '27', 'Stio Antnio Monteiro', 'Stio Antnio Monteiro', -23.3521, -46.067699, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(292, 'BR', '27', 'Cerejeiras', 'Cerejeiras', -23.3647, -46.060001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(293, 'BR', '27', 'Stio Rumo Certo', 'Stio Rumo Certo', -23.371599, -46.0657, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(294, 'BR', '27', 'Stio Palmeiras', 'Stio Palmeiras', -23.371599, -46.055599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(295, 'BR', '27', 'Stio Santa Isabel', 'Stio Santa Isabel', -23.373899, -46.0592, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(296, 'BR', '27', 'Stio Das Ilhas', 'Stio Das Ilhas', -23.376499, -46.064601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(297, 'BR', '27', 'Stio So Benedito', 'Stio So Benedito', -23.369899, -46.049499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(298, 'BR', '27', 'Stio Joaquim Nunes', 'Stio Joaquim Nunes', -23.366699, -46.038501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(299, 'BR', '27', 'Stio Ana Maria Conceio', 'Stio Ana Maria Conceio', -23.3633, -46.0405, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(300, 'BR', '27', 'So Silvestre Do Jacare', 'So Silvestre Do Jacare', -23.3644, -46.0172, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(301, 'BR', '27', 'Itapema', 'Itapema', -23.3642, -46.010601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(302, 'BR', '27', 'Stio Pedro Morais', 'Stio Pedro Morais', -23.3665, -46.009101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(303, 'BR', '27', 'Stio Jardim Do Edem', 'Stio Jardim Do Edem', -23.392099, -46.0032, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(304, 'BR', '27', 'Stio Botuquara', 'Stio Botuquara', -23.3952, -46.047401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(305, 'BR', '27', 'Stio Groto', 'Stio Groto', -23.391199, -46.0419, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(306, 'BR', '27', 'Stio Brejo Seco', 'Stio Brejo Seco', -23.3973, -46.0485, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(307, 'BR', '27', 'Stio Tapera', 'Stio Tapera', -23.395399, -46.074798, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(308, 'BR', '27', 'Stio Do Barreiro', 'Stio Do Barreiro', -23.388299, -46.059898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(309, 'BR', '27', 'Recanto So Jos', 'Recanto So Jos', -23.3859, -46.061, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(310, 'BR', '27', 'Bairro Fregueisa Da Escada', 'Bairro Fregueisa Da Escada', -23.384399, -46.058101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(311, 'BR', '27', 'Stio Caverna', 'Stio Caverna', -23.381299, -46.058601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(312, 'BR', '27', 'Stio Jos Marques', 'Stio Jos Marques', -23.400499, -46.013401, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(313, 'BR', '27', 'Rancho Prola Do Vale', 'Rancho Prola Do Vale', -23.401899, -46.014301, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(314, 'BR', '27', 'Stio Bocaina', 'Stio Bocaina', -23.405799, -46.001098, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(315, 'BR', '27', 'Estncia Guarema', 'Estncia Guarema', -23.4081, -46.008899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(316, 'BR', '27', 'Paio', 'Paio', -23.414499, -46.007701, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(317, 'BR', '27', 'Stio Aurora', 'Stio Aurora', -23.4074, -46.024101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(318, 'BR', '27', 'Stio Dos Marrons', 'Stio Dos Marrons', -23.410699, -46.056198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(319, 'BR', '27', 'Merenda', 'Merenda', -23.425899, -46.069198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0);
INSERT INTO `geocities` (`id`, `country_code`, `state_code`, `accent`, `name`, `latitude`, `longitude`, `timezoneid`, `order_display`, `from_source`, `popularity`, `last_modified`, `name_fulltext`, `published`) VALUES
(320, 'BR', '27', 'Stio Merenda', 'Stio Merenda', -23.4328, -46.042499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(321, 'BR', '27', 'Bairro Dos Nogueiras', 'Bairro Dos Nogueiras', -23.4316, -46.039001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(322, 'BR', '27', 'Stio Nossa Senhora Aparecida', 'Stio Nossa Senhora Aparecida', -23.422599, -46.049598, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(323, 'BR', '27', 'Stio Trs Mosqueteiros', 'Stio Trs Mosqueteiros', -23.4183, -46.051498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(324, 'BR', '27', 'Bairro Do Paiol', 'Bairro Do Paiol', -23.4178, -46.001899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(325, 'BR', '27', 'Capoeira', 'Capoeira', -23.451499, -46.037399, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(326, 'BR', '27', 'Stio Jandaia', 'Stio Jandaia', -23.450399, -46.048599, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(327, 'BR', '27', 'Stio So Carlos', 'Stio So Carlos', -23.449699, -46.058101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(328, 'BR', '27', 'Itapeti', 'Itapeti', -23.447799, -46.074501, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(329, 'BR', '27', 'Luis Carlos', 'Luis Carlos', -23.4573, -46.058399, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(330, 'BR', '27', 'Stio Santa Rosa', 'Stio Santa Rosa', -23.4591, -46.059799, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(331, 'BR', '27', 'Bairro Convento', 'Bairro Convento', -23.4542, -46.0704, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(332, 'BR', '27', 'Stio So Lus Do Salim', 'Stio So Lus Do Salim', -23.465999, -46.064498, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(333, 'BR', '27', 'Stio Maranata', 'Stio Maranata', -23.466999, -46.076499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(334, 'BR', '27', 'Stio Bom Jesus', 'Stio Bom Jesus', -23.485799, -46.003101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(335, 'BR', '27', 'Stio Do Zizi', 'Stio Do Zizi', -23.488, -46.0135, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(336, 'BR', '27', 'Vila Matias', 'Vila Matias', -23.4839, -46.079299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(337, 'BR', '27', 'Stio Das Paineiras', 'Stio Das Paineiras', -23.485599, -46.083198, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(338, 'BR', '27', 'Stio Peras', 'Stio Peras', -23.473899, -46.092201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(339, 'BR', '27', 'Stio Nossa Senhora De Lourdes', 'Stio Nossa Senhora De Lourdes', -23.464899, -46.094299, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(340, 'BR', '27', 'Stio Jacarand', 'Stio Jacarand', -23.467199, -46.082901, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(341, 'BR', '27', 'Stio Xangri-L', 'Stio Xangri-L', -23.465, -46.080001, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(342, 'BR', '27', 'Stio Da Colina', 'Stio Da Colina', -23.462499, -46.086898, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(343, 'BR', '27', 'Stio Dos Quatro Sacis', 'Stio Dos Quatro Sacis', -23.4899, -46.091201, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(344, 'BR', '27', 'Botuquara', 'Botuquara', -23.4902, -46.053199, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(345, 'BR', '27', 'Stio Santa Teresa', 'Stio Santa Teresa', -23.4937, -46.052101, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(346, 'BR', '27', 'Lagoa Nova', 'Lagoa Nova', -23.4948, -46.0121, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(347, 'BR', '27', 'Pouso Alegre', 'Pouso Alegre', -23.258899, -46.2406, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(348, 'BR', '27', 'Stio Piraraquara', 'Stio Piraraquara', -23.375499, -46.0592, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(349, 'BR', '27', 'Vila Gumercindo', 'Vila Gumercindo', -23.319799, -46.222099, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(350, 'BR', '27', 'Stio Barna', 'Stio Barna', -23.410499, -46.230899, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(351, 'BR', '27', 'Lambari', 'Lambari', -23.360599, -46.148601, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(352, 'BR', '27', 'Stio Antnio Piral', 'Stio Antnio Piral', -23.288499, -46.186, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(353, 'BR', '27', 'Stio Vila Real', 'Stio Vila Real', -23.312799, -46.167499, 'America/Sao_Paulo              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(354, 'MX', '28', 'La Isla De Ocampo', 'La Isla De Ocampo', 22.8833, -99.050003, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(355, 'MX', '28', 'HÃ©roes De Chapultepec', 'Heroes De Chapultepec', 22.9456, -99.037002, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(356, 'MX', '28', 'Rancho Alegre', 'Rancho Alegre', 22.899999, -99.1333, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(357, 'MX', '28', 'Plan De Guadalupe', 'Plan De Guadalupe', 22.958599, -99.119499, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(358, 'MX', '28', 'Rancho Nuevo', 'Rancho Nuevo', 22.8759, -99.1968, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(359, 'MX', '28', 'Paso Real De Morelos', 'Paso Real De Morelos', 22.7721, -99.2807, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(360, 'MX', '28', 'La Alhajilla', 'La Alhajilla', 22.752, -99.204399, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(361, 'MX', '28', 'Tanlajas', 'Tanlajas', 22.216699, -99.199996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(362, 'MX', '28', 'Las Cortadas', 'Las Cortadas', 22.722499, -99.266403, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(363, 'MX', '28', 'El Pensil', 'El Pensil', 22.703699, -99.400199, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(364, 'MX', '28', 'El Zopilote', 'El Zopilote', 22.783, -99.346702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(365, 'MX', '28', 'Las Canoas', 'Las Canoas', 22.907499, -99.379302, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(366, 'MX', '28', 'Aniceto Medrano', 'Aniceto Medrano', 22.9414, -99.519897, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(367, 'MX', '28', 'Santa MarÃ­a De Guadalupe', 'Santa Maria De Guadalupe', 22.9074, -99.488998, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(368, 'MX', '28', 'El PeÃ±on', 'El Penon', 22.839399, -99.560401, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(369, 'MX', '28', 'Alberto Carrera Torres', 'Alberto Carrera Torres', 22.733299, -99.583297, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(370, 'MX', '28', 'Lucio Vasquez', 'Lucio Vasquez', 22.733299, -99.699996, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(371, 'MX', '28', 'San Pedro', 'San Pedro', 22.75, -99.683296, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(372, 'MX', '28', 'E. Aztecas', 'E. Aztecas', 22.7667, -99.0167, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(373, 'MX', '28', 'Valle De Cieneguilla', 'Valle De Cieneguilla', 22.716699, -99.599998, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(374, 'MX', '28', 'Ahuacales', 'Ahuacales', 22.914199, -99.5764, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(375, 'MX', '28', 'Veinte De Noviembre', 'Veinte De Noviembre', 22.961799, -99.791397, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(376, 'MX', '28', 'Tanque Blanco', 'Tanque Blanco', 22.9538, -99.767601, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(377, 'MX', '28', 'Pastores', 'Pastores', 22.721799, -99.735801, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(378, 'MX', '28', 'Magdaleno Cedillo', 'Magdaleno Cedillo', 22.7894, -99.936698, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(379, 'MX', '28', 'El Gavital', 'El Gavital', 22.893199, -99.950698, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(380, 'MX', '28', 'Alfonzo Terrones Benitez', 'Alfonzo Terrones Benitez', 22.929899, -99.819503, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(381, 'MX', '28', 'Santa Ana De Arriba', 'Santa Ana De Arriba', 22, -99.875, 'America/Mexico_City            ', 0, 'old', 1, '2015-10-23 14:43:52', '', 0),
(382, 'MX', '28', 'El Boludo', 'El Boludo', 22, -99.900001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(383, 'MX', '28', 'XicotÃ©ncatl', 'Xicotencatl', 22.9957, -98.943199, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(384, 'MX', '28', 'San Francisco El Alto', 'San Francisco El Alto', 23.428699, -98.828102, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(385, 'MX', '28', 'Joya De Manantiales', 'Joya De Manantiales', 23.0468, -99.2518, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(386, 'MX', '28', 'La Florida', 'La Florida', 23.381, -99.500503, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(387, 'MX', '28', 'San Juan', 'San Juan', 22.9986, -99.788902, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(388, 'MX', '28', 'Pedro RuÃ­z Molina', 'Pedro Ruiz Molina', 23.1431, -99.690803, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(389, 'MX', '28', 'La Joya Del Berrendo', 'La Joya Del Berrendo', 23.048, -100.001998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(390, 'MX', '28', 'JesÃºs RamÃ­rez', 'Jesus Ramirez', 23.964, -98.235801, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(391, 'MX', '28', 'Viento Libre', 'Viento Libre', 23.7933, -98.985298, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(392, 'MX', '28', 'Adolfo LÃ³pez Mateos', 'Adolfo Lopez Mateos', 23.573999, -98.970703, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(393, 'MX', '28', 'AcatlÃ¡n', 'Acatlan', 23.968399, -99.130096, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(394, 'MX', '28', 'Los Caballos', 'Los Caballos', 23.983299, -99.483299, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(395, 'MX', '28', 'Estanque De Eguia', 'Estanque De Eguia', 23.5625, -99.930397, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(396, 'MX', '28', 'Verde Chico', 'Verde Chico', 24.02, -97.946601, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(397, 'MX', '28', 'Lopez Mateos', 'Lopez Mateos', 24.006599, -98.343696, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(398, 'MX', '28', 'El Cinco', 'El Cinco', 24.4097, -98.207, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(399, 'MX', '28', 'El Elefante', 'El Elefante', 24.266099, -98.466598, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(400, 'MX', '28', 'Poblado Allende', 'Poblado Allende', 24.156999, -98.414901, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(401, 'MX', '28', 'Nuevo Dolores', 'Nuevo Dolores', 24.063699, -98.393798, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(402, 'MX', '28', 'El Herradero', 'El Herradero', 24.0795, -98.4841, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(403, 'MX', '28', 'Big Bass', 'Big Bass', 24.045299, -98.706596, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(404, 'MX', '28', 'MarÃ­a Ubalda', 'Maria Ubalda', 24.1417, -98.570297, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(405, 'MX', '28', 'La Libertad Campesina', 'La Libertad Campesina', 24.1485, -98.558502, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(406, 'MX', '28', 'Mata Verde', 'Mata Verde', 24.253, -98.734901, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(407, 'MX', '28', 'Tres De Abril', 'Tres De Abril', 24.263299, -98.584297, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(408, 'MX', '28', 'General Luis Caballero', 'General Luis Caballero', 24.3285, -98.674499, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(409, 'MX', '28', 'La FÃ© Del Golfo', 'La Fe Del Golfo', 24.4083, -98.498901, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(410, 'MX', '28', 'La Selva', 'La Selva', 24.351699, -98.947601, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(411, 'MX', '28', 'San Leonardo', 'San Leonardo', 24.2728, -98.865997, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(412, 'MX', '28', 'Los Picachos', 'Los Picachos', 24.2635, -98.905601, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(413, 'MX', '28', 'San Patricio', 'San Patricio', 24.065799, -99.082496, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(414, 'MX', '28', 'El Tablero', 'El Tablero', 24.065599, -99.000602, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(415, 'MX', '28', 'La Soledad', 'La Soledad', 24.079999, -99.077903, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(416, 'MX', '28', 'Los Brasiles', 'Los Brasiles', 24.085699, -99.091102, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(417, 'MX', '28', 'El Terronal', 'El Terronal', 24.143699, -99.025497, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(418, 'MX', '28', 'Santa Enriqueta', 'Santa Enriqueta', 24.168899, -99.043601, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(419, 'MX', '28', 'Tres Hermanas', 'Tres Hermanas', 24.1615, -99.071098, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(420, 'MX', '28', 'Magueycitos', 'Magueycitos', 24.184099, -99.134399, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(421, 'MX', '28', 'Las Sabinas', 'Las Sabinas', 24.228799, -99.122398, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(422, 'MX', '28', 'San Pablo', 'San Pablo', 24.361499, -99.103698, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(423, 'MX', '28', 'El Faro', 'El Faro', 24.456399, -99.295196, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(424, 'MX', '28', 'Pedro JosÃ© Mendez', 'Pedro Jose Mendez', 24.409599, -99.194099, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(425, 'MX', '28', 'Praxedis Balboa', 'Praxedis Balboa', 24.3414, -99.1483, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(426, 'MX', '28', 'PeÃ±ascoso', 'Penascoso', 24.2828, -99.214797, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(427, 'MX', '28', 'La Volanta', 'La Volanta', 24.288499, -99.266899, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(428, 'MX', '28', 'Las Tinajas', 'Las Tinajas', 24.226699, -99.230201, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(429, 'MX', '28', 'El Quilacho', 'El Quilacho', 24.173799, -99.207702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(430, 'MX', '28', 'El Enchilado', 'El Enchilado', 24.1702, -99.303802, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(431, 'MX', '28', 'Vicente Guerrero', 'Vicente Guerrero', 24.066699, -99.199996, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(432, 'MX', '28', 'La ConcepciÃ³n', 'La Concepcion', 24.2744, -99.3488, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(433, 'MX', '28', 'El Paisano', 'El Paisano', 24.3069, -99.305198, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(434, 'MX', '28', 'JesÃºs GonzÃ¡lez Ortega', 'Jesus Gonzalez Ortega', 24.4046, -99.411399, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(435, 'MX', '28', 'Mariana Escobedo', 'Mariana Escobedo', 24.381999, -99.501296, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(436, 'MX', '28', 'Margaritas', 'Margaritas', 24.355199, -99.560997, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(437, 'MX', '28', 'Carbajal', 'Carbajal', 24.513099, -97.739799, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(438, 'MX', '28', 'San Isidro Sur', 'San Isidro Sur', 24.8588, -97.902999, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(439, 'MX', '28', 'Las Palmas', 'Las Palmas', 24.8596, -97.946701, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(440, 'MX', '28', 'Pragedes Balboa', 'Pragedes Balboa', 24.755199, -97.833099, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(441, 'MX', '28', 'SimÃ³n BolÃ­var', 'Simon Bolivar', 24.5683, -97.912498, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(442, 'MX', '28', 'Nuevo Tlaxcala', 'Nuevo Tlaxcala', 24.817899, -98.061302, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(443, 'MX', '28', 'Nuevo Sevilla', 'Nuevo Sevilla', 24.927, -97.973701, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(444, 'MX', '28', 'El Huapango', 'El Huapango', 24.633199, -98.1623, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(445, 'MX', '28', 'El Limonal', 'El Limonal', 24.710599, -98.3358, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(446, 'MX', '28', 'La Primavera', 'La Primavera', 24.9057, -98.307701, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(447, 'MX', '28', 'Las Escobas', 'Las Escobas', 24.964399, -98.343002, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(448, 'MX', '28', 'El Capote', 'El Capote', 24.878, -98.456596, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(449, 'MX', '28', 'Soledad De Los Reyes', 'Soledad De Los Reyes', 24.8192, -98.482902, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(450, 'MX', '28', 'El JapÃ³n', 'El Japon', 24.7187, -98.3563, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(451, 'MX', '28', 'La Estajadilla', 'La Estajadilla', 24.6492, -98.707397, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(452, 'MX', '28', 'San JosÃ© De La Presa', 'San Jose De La Presa', 24.9839, -98.690597, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(453, 'MX', '28', 'Las Amarillas', 'Las Amarillas', 24.9421, -98.709999, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(454, 'MX', '28', 'El Alto Del Chaparral', 'El Alto Del Chaparral', 24.856899, -98.723098, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(455, 'MX', '28', 'Las Encinas', 'Las Encinas', 24.7942, -98.681602, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(456, 'MX', '28', 'El Antrisco', 'El Antrisco', 24.5984, -98.827003, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(457, 'MX', '28', 'MÃ©rida', 'Merida', 24.542499, -98.909698, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(458, 'MX', '28', 'El Aguame', 'El Aguame', 24.816699, -98.8833, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(459, 'MX', '28', 'El Quiote', 'El Quiote', 24.9822, -98.9029, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(460, 'MX', '28', 'SegoveÃ±o', 'Segoveno', 25.0135, -99.0456, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(461, 'MX', '28', 'Las Jaras', 'Las Jaras', 24.8502, -99.183097, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(462, 'MX', '28', 'La Argentina', 'La Argentina', 24.7938, -99.182899, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(463, 'MX', '28', 'La Alberquita', 'La Alberquita', 24.672199, -99.195396, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(464, 'MX', '28', 'La Chorrera', 'La Chorrera', 24.564699, -99.2788, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(465, 'MX', '28', 'NicolÃ¡s Flores', 'Nicolas Flores', 24.527, -99.473197, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(466, 'MX', '28', 'Veintiuno De Enero', 'Veintiuno De Enero', 24.6954, -99.392402, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(467, 'MX', '28', 'Belisario Dominguez', 'Belisario Dominguez', 24.670499, -99.464897, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(468, 'MX', '28', 'Venustiano Carranza', 'Venustiano Carranza', 24.526399, -99.698303, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(469, 'MX', '28', 'Guajardo', 'Guajardo', 25.3694, -97.401397, 'America/Matamoros              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(470, 'MX', '13', 'San JosÃ©', 'San Jose', 20.3458, -98.697502, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(471, 'MX', '15', 'San JosÃ©', 'San Jose', 19.816699, -99.599998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(472, 'MX', '17', 'San JosÃ©', 'San Jose', 18.866699, -99.050003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(473, 'MX', '23', 'San JosÃ©', 'San Jose', 18.7, -88.216697, 'America/Cancun                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(474, 'MX', '21', 'San JosÃ©', 'San Jose', 18.504999, -98.705299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(475, 'MX', '11', 'San JosÃ©', 'San Jose', 16.738399, -99.243301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(476, 'MX', '05', 'San JosÃ©', 'San Jose', 16.014799, -91.819999, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(477, 'MX', '30', 'San Jorge Atitla', 'San Jorge Atitla', 18.718399, -96.821403, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(478, 'MX', '05', 'San Jorge', 'San Jorge', 16.95, -93.216697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(479, 'MX', '28', 'San JoaquÃ­n', 'San Joaquin', 26.049999, -98.166702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(480, 'MX', '22', 'San JoaquÃ­n', 'San Joaquin', 20.914699, -99.565597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(481, 'MX', '13', 'San JoaquÃ­n', 'San Joaquin', 20.502899, -99.775199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(482, 'MX', '31', 'San JoaquÃ­n', 'San Joaquin', 20.399999, -89.550003, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(483, 'MX', '29', 'San JoaquÃ­n', 'San Joaquin', 19.3731, -98.401603, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(484, 'MX', '30', 'San JoaquÃ­n', 'San Joaquin', 18.85, -96.783302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(485, 'MX', '20', 'San JoaquÃ­n', 'San Joaquin', 16.5921, -96.756599, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(486, 'MX', '05', 'San Joaquin', 'San Joaquin', 14.9833, -92.183296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(487, 'MX', '20', 'San JimÃ©nez', 'San Jimenez', 16.316699, -95.066703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(488, 'MX', '31', 'San JesÃºs', 'San Jesus', 20.8833, -89.166702, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(489, 'MX', '20', 'San JerÃ³nimo Zoochina', 'San Jeronimo Zoochina', 17.216699, -95.25, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(490, 'MX', '15', 'San JerÃ³nimo Zacapexco', 'San Jeronimo Zacapexco', 19.722, -99.476303, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(491, 'MX', '15', 'San JerÃ³nimo XonacahuacÃ¡n', 'San Jeronimo Xonacahuacan', 19.7478, -98.951301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(492, 'MX', '21', 'San JerÃ³nimo XayacatlÃ¡n', 'San Jeronimo Xayacatlan', 18.221, -97.914703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(493, 'MX', '15', 'San JerÃ³nimo Tepetlacalco', 'San Jeronimo Tepetlacalco', 19.5167, -99.199996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(494, 'MX', '21', 'San JerÃ³nimo Tecuanipan', 'San Jeronimo Tecuanipan', 19.013599, -98.398902, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(495, 'MX', '20', 'San JerÃ³nimo Tecoatl', 'San Jeronimo Tecoatl', 18.1679, -96.9132, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(496, 'MX', '20', 'San JerÃ³nimo Taviche', 'San Jeronimo Taviche', 16.7124, -96.593902, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(497, 'MX', '20', 'San JerÃ³nimo Sosola', 'San Jeronimo Sosola', 17.3673, -97.032897, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(498, 'MX', '20', 'San JerÃ³nimo Silacayoapilla', 'San Jeronimo Silacayoapilla', 17.810499, -97.845199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(499, 'MX', '20', 'San JerÃ³nimo Progreso', 'San Jeronimo Progreso', 17.4454, -98.154998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(500, 'MX', '21', 'San JerÃ³nimo OcotitlÃ¡n', 'San Jeronimo Ocotitlan', 19.0319, -97.967399, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(501, 'MX', '15', 'San JerÃ³nimo Ixtapantongo', 'San Jeronimo Ixtapantongo', 19.5263, -99.760398, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(502, 'MX', '20', 'San JerÃ³nimo CoatlÃ¡n', 'San Jeronimo Coatlan', 16.2317, -96.868598, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(503, 'MX', '21', 'San JerÃ³nimo AxochitlÃ¡n', 'San Jeronimo Axochitlan', 18.2833, -97.2667, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(504, 'MX', '15', 'San JerÃ³nimo Aculco', 'San Jeronimo Aculco', 20.0832, -99.861801, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(505, 'MX', '31', 'San JerÃ³nimo', 'San Jeronimo', 20.533399, -89.0391, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(506, 'MX', '13', 'San JerÃ³nimo', 'San Jeronimo', 20.4846, -98.157798, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(507, 'MX', '15', 'San JerÃ³nimo', 'San Jeronimo', 19.616699, -99.416702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(508, 'MX', '15', 'San JerÃ³nimo Chicahualco', 'San Jeronimo Chicahualco', 19.2828, -99.592903, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(509, 'MX', '04', 'San JerÃ³nimo', 'San Jeronimo', 18.899999, -90.900001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(510, 'MX', '30', 'San JerÃ³nimo', 'San Jeronimo', 17.984399, -95.701499, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(511, 'MX', '27', 'San JerÃ³nimo', 'San Jeronimo', 17.949199, -91.886901, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(512, 'MX', '05', 'San JerÃ³nimo', 'San Jeronimo', 17.149999, -92.816703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(513, 'MX', '28', 'San Javier', 'San Javier', 27.2677, -99.496696, 'America/Chicago                ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(514, 'MX', '19', 'San Javier', 'San Javier', 26.2637, -99.419197, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(515, 'MX', '30', 'San Javier', 'San Javier', 21.876399, -97.852798, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(516, 'MX', '13', 'San Javier', 'San Javier', 20.233299, -98.199996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(517, 'MX', '15', 'San Javier', 'San Javier', 19.85, -98.816703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(518, 'MX', '05', 'San Javier', 'San Javier', 16.8157, -91.115402, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(519, 'MX', '20', 'San Jacinto Tlacotepec', 'San Jacinto Tlacotepec', 16.519899, -97.3833, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(520, 'MX', '20', 'San Jacinto OcotlÃ¡n', 'San Jacinto Ocotlan', 16.799999, -96.716697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(521, 'MX', '19', 'San Jacinto', 'San Jacinto', 24.985399, -99.2135, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(522, 'MX', '31', 'San Jacinto', 'San Jacinto', 21.2, -89.116699, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(523, 'MX', '21', 'San Isidro Xicalahuata', 'San Isidro Xicalahuata', 19.641799, -97.6137, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(524, 'MX', '31', 'San Isidro Miramar', 'San Isidro Miramar', 20.9167, -90.3833, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(525, 'MX', '05', 'San Isidro De Platanar', 'San Isidro De Platanar', 17.6333, -93.25, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(526, 'MX', '28', 'San Isidro De Abajo', 'San Isidro De Abajo', 26.049999, -97.983299, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(527, 'MX', '20', 'San Isidro Apango', 'San Isidro Apango', 15.808699, -96.358299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(528, 'MX', '24', 'San Isidro', 'San Isidro', 21.9076, -99.960403, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(529, 'MX', '30', 'San Isidro', 'San Isidro', 21.366699, -98.033302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(530, 'MX', '13', 'Almolo', 'Almolo', 21.066099, -98.865097, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(531, 'MX', '31', 'San Isidro', 'San Isidro', 21.0333, -88.166702, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(532, 'MX', '13', 'San Isidro', 'San Isidro', 20.149999, -98.366699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(533, 'MX', '27', 'San Isidro', 'San Isidro', 18.399999, -92.599998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(534, 'MX', '20', 'San Isidro', 'San Isidro', 16.5, -94.349998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(535, 'MX', '05', 'San Isidro', 'San Isidro', 15.55, -92.400001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(536, 'MX', '20', 'San Ildefonso Villa Alta', 'San Ildefonso Villa Alta', 17.338399, -96.152397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(537, 'MX', '20', 'San Ildefonso AmatlÃ¡n', 'San Ildefonso Amatlan', 16.3372, -96.489501, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(538, 'MX', '15', 'San Ildefonso', 'San Ildefonso', 19.5578, -99.787803, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(539, 'MX', '31', 'San Ignacio Xtuc', 'San Ignacio Xtuc', 20.399999, -89.5167, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(540, 'MX', '21', 'San Ignacio Xixipesco', 'San Ignacio Xixipesco', 19.5732, -97.566596, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(541, 'MX', '19', 'San Ignacio', 'San Ignacio', 25.214599, -99.187103, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(542, 'MX', '28', 'San Ignacio', 'San Ignacio', 24.333299, -98.566703, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(543, 'MX', '31', 'San Ignacio', 'San Ignacio', 21.160299, -89.655197, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(544, 'MX', '15', 'San Ignacio', 'San Ignacio', 20.1333, -99.699996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(545, 'MX', '17', 'San Ignacio', 'San Ignacio', 18.5781, -98.742698, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(546, 'MX', '27', 'San Ignacio', 'San Ignacio', 17.7434, -91.2957, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(547, 'MX', '21', 'San HipÃ³lito Xochiltenango', 'San Hipolito Xochiltenango', 18.941699, -97.873497, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(548, 'MX', '20', 'San Guillermo', 'San Guillermo', 16.4192, -96.724403, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(549, 'MX', '24', 'Sanguijuela', 'Sanguijuela', 21.9181, -99.8451, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(550, 'MX', '20', 'San Gregorio Ozolotepec', 'San Gregorio Ozolotepec', 16.130599, -96.330497, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(551, 'MX', '15', 'San Gregorio Macapexco', 'San Gregorio Macapexco', 19.6667, -99.5, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(552, 'MX', '15', 'San Gregorio Cuautzingo', 'San Gregorio Cuautzingo', 19.259599, -98.857398, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(553, 'MX', '05', 'San Gregorio Chamic', 'San Gregorio Chamic', 15.8493, -91.962097, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(554, 'MX', '21', 'San Gregorio Atzompa', 'San Gregorio Atzompa', 19.0282, -98.352096, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(555, 'MX', '09', 'San Gregorio Atlapulco', 'San Gregorio Atlapulco', 19.256, -99.0596, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(556, 'MX', '31', 'San Gregorio', 'San Gregorio', 21.1667, -88.783302, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(557, 'MX', '13', 'San Gregorio', 'San Gregorio', 20.4986, -98.018302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(558, 'MX', '30', 'San Gregorio', 'San Gregorio', 18, -95.25, 'America/Mexico_City            ', 0, 'old', 1, '2015-10-23 14:43:52', '', 0),
(559, 'MX', '05', 'San Gregorio', 'San Gregorio', 15.9167, -92.166702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(560, 'MX', '28', 'San GermÃ¡n', 'San German', 25.2168, -97.921897, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(561, 'MX', '15', 'San Gaspar Tonatico', 'San Gaspar Tonatico', 18.806699, -99.669197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(562, 'MX', '20', 'San Gabriel Mixtepec', 'San Gabriel Mixtepec', 16.0951, -97.081596, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(563, 'MX', '20', 'San Gabriel Etla', 'San Gabriel Etla', 17.215, -96.769699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(564, 'MX', '21', 'San Gabriel Chilac', 'San Gabriel Chilac', 18.326, -97.347801, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(565, 'MX', '20', 'San Gabriel Casa Blanca', 'San Gabriel Casa Blanca', 18.151699, -97.137702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(566, 'MX', '19', 'San Gabriel', 'San Gabriel', 24.960599, -99.305603, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(567, 'MX', '28', 'San Gabriel', 'San Gabriel', 23.0638, -98.788696, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(568, 'MX', '30', 'San Gabriel', 'San Gabriel', 21.399999, -98.349998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(569, 'MX', '13', 'San Gabriel', 'San Gabriel', 20.173999, -99.335197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(570, 'MX', '17', 'San Gabriel', 'San Gabriel', 18.616699, -99.349998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(571, 'MX', '04', 'San Gabriel', 'San Gabriel', 18.25, -90.716697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(572, 'MX', '20', 'San Gabriel', 'San Gabriel', 17.728599, -95.894203, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(573, 'MX', '05', 'San Gabriel', 'San Gabriel', 16.8456, -93.015098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(574, 'MX', '15', 'San Francisquito', 'San Francisquito', 20.1664, -99.705001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(575, 'MX', '15', 'San Francisco Zacacalco', 'San Francisco Zacacalco', 19.9314, -98.982696, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(576, 'MX', '20', 'San Francisco YateÃ©', 'San Francisco Yatee', 17.3057, -96.164199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(577, 'MX', '21', 'San Francisco Xochiteopan', 'San Francisco Xochiteopan', 18.805999, -98.624099, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(578, 'MX', '31', 'San Francisco Tzon', 'San Francisco Tzon', 20.983499, -89.145202, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(579, 'MX', '20', 'San Francisco Tutepetongo', 'San Francisco Tutepetongo', 17.7667, -96.8833, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(580, 'MX', '21', 'San Francisco Totimehuacan', 'San Francisco Totimehuacan', 18.966899, -98.185096, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(581, 'MX', '15', 'San Francisco Tlalcilalcalpan', 'San Francisco Tlalcilalcalpan', 19.2935, -99.767303, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(582, 'MX', '29', 'San Francisco Tetlanohca', 'San Francisco Tetlanohca', 19.260099, -98.163101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(583, 'MX', '15', 'San Francisco Tepexoxuca', 'San Francisco Tepexoxuca', 19.059999, -99.547897, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(584, 'MX', '05', 'San Francisco Tepecuapan', 'San Francisco Tepecuapan', 16.866699, -92.099998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(585, 'MX', '20', 'San Francisco TeopÃ¡n', 'San Francisco Teopan', 17.8486, -97.492698, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(586, 'MX', '20', 'San Francisco Telixtlahuaca', 'San Francisco Telixtlahuaca', 17.2968, -96.905296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(587, 'MX', '20', 'San Francisco Sola', 'San Francisco Sola', 16.5158, -96.974197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(588, 'MX', '20', 'San Francisco Sayultepec', 'San Francisco Sayultepec', 16.5953, -98.129997, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(589, 'MX', '13', 'San Francisco Sacachichilco', 'San Francisco Sacachichilco', 20.487499, -99.4085, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(590, 'MX', '20', 'San Francisco Ozolotepec', 'San Francisco Ozolotepec', 16.1012, -96.222602, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(591, 'MX', '15', 'San Francisco Oxtotilpan', 'San Francisco Oxtotilpan', 19.168699, -99.902801, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(592, 'MX', '05', 'San Francisco Ocotal', 'San Francisco Ocotal', 16.116699, -93.816703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(593, 'MX', '21', 'San Francisco Mixtla', 'San Francisco Mixtla', 18.9025, -97.895797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(594, 'MX', '29', 'San Francisco Mitepec', 'San Francisco Mitepec', 19.4384, -98.4477, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(595, 'MX', '13', 'San Francisco Londres', 'San Francisco Londres', 19.9167, -98.466697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(596, 'MX', '20', 'San Francisco Loqueche', 'San Francisco Loqueche', 16.3542, -96.376502, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(597, 'MX', '20', 'San Francisco LachigolÃ³', 'San Francisco Lachigolo', 17.015899, -96.600402, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(598, 'MX', '21', 'San Francisco Jalapexco', 'San Francisco Jalapexco', 18.8393, -98.299301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(599, 'MX', '21', 'Huilango', 'Huilango', 18.8428, -98.583503, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(600, 'MX', '20', 'San Francisco HuehuetlÃ¡n', 'San Francisco Huehuetlan', 18.197799, -96.943298, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(601, 'MX', '20', 'San Francisco Huapanapan', 'San Francisco Huapanapan', 18.140199, -97.672401, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(602, 'MX', '19', 'San Francisco Don Abraham', 'San Francisco Don Abraham', 25.066699, -99.166702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(603, 'MX', '28', 'San Francisco De Zorrilla', 'San Francisco De Zorrilla', 23.4691, -99.340599, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(604, 'MX', '15', 'San Francisco De Las Tablas', 'San Francisco De Las Tablas', 19.765499, -99.529403, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(605, 'MX', '20', 'San Francisco De Arriba', 'San Francisco De Arriba', 16.1378, -97.578002, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(606, 'MX', '20', 'San Francisco De Abajo', 'San Francisco De Abajo', 16.0939, -97.609703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(607, 'MX', '09', 'San Francisco CulhuacÃ¡n', 'San Francisco Culhuacan', 19.3325, -99.101303, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(608, 'MX', '15', 'San Francisco Cuautliquixco', 'San Francisco Cuautliquixco', 19.6818, -98.981903, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(609, 'MX', '20', 'San Francisco Cotahuixtla', 'San Francisco Cotahuixtla', 17.529499, -96.910797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(610, 'MX', '20', 'San Francisco CoatlÃ¡n', 'San Francisco Coatlan', 16.1868, -96.7705, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(611, 'MX', '20', 'San Francisco ChindÃºa', 'San Francisco Chindua', 17.428899, -97.311599, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(612, 'MX', '15', 'San Francisco ChejÃ©', 'San Francisco Cheje', 19.681499, -99.752098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(613, 'MX', '20', 'San Francisco Chapulapa', 'San Francisco Chapulapa', 17.940399, -96.755599, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(614, 'MX', '20', 'San Francisco Cajonos', 'San Francisco Cajonos', 17.1721, -96.25, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(615, 'MX', '20', 'San Francisco CahuacuÃ¡', 'San Francisco Cahuacua', 16.896499, -97.305999, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(616, 'MX', '23', 'San Francisco Botes', 'San Francisco Botes', 18.113899, -88.717903, 'America/Cancun                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(617, 'MX', '13', 'San Francisco Bojay', 'San Francisco Bojay', 20.095399, -99.347099, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(618, 'MX', '29', 'San Francisco Atezcatzinco', 'San Francisco Atezcatzinco', 19.4696, -98.1482, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(619, 'MX', '15', 'San Francisco Acuautla', 'San Francisco Acuautla', 19.3432, -98.855598, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(620, 'MX', '28', 'San Francisco', 'San Francisco', 23.6399, -98.588897, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(621, 'MX', '30', 'Juan De Dios Peza', 'Juan De Dios Peza', 21.390499, -97.604103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(622, 'MX', '31', 'San Francisco', 'San Francisco', 20.5, -89.6333, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(623, 'MX', '04', 'San Francisco', 'San Francisco', 19.7, -90.5167, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(624, 'MX', '09', 'San Francisco Tlalneplanta', 'San Francisco Tlalneplanta', 19.197399, -99.122001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(625, 'MX', '15', 'San Francisco', 'San Francisco', 18.9078, -99.929298, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(626, 'MX', '30', 'San Francisco', 'San Francisco', 18.0704, -94.418197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(627, 'MX', '27', 'San Francisco', 'San Francisco', 17.927299, -92.8031, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(628, 'MX', '12', 'San Francisco', 'San Francisco', 17.925399, -99.338996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(629, 'MX', '05', 'San Francisco', 'San Francisco', 16.901399, -93.266799, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(630, 'MX', '30', 'San Florentino', 'San Florentino', 20.4333, -97.199996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(631, 'MX', '20', 'San Fernando De Matamoros', 'San Fernando De Matamoros', 16.842399, -97.153701, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(632, 'MX', '05', 'San Fernando CahuacÃ¡n', 'San Fernando Cahuacan', 14.721199, -92.272102, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(633, 'MX', '28', 'San Fernando', 'San Fernando', 27.175899, -99.616302, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(634, 'MX', '31', 'San Fernando', 'San Fernando', 21.5764, -87.872001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(635, 'MX', '23', 'San Fernando', 'San Fernando', 18.8833, -88.566703, 'America/Cancun                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(636, 'MX', '27', 'San Fernando', 'San Fernando', 17.980899, -93.626998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(637, 'MX', '05', 'San Fernando', 'San Fernando', 16.871999, -93.209396, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(638, 'MX', '28', 'San FernandeÃ±o', 'San Fernandeno', 26, -98.050003, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(639, 'MX', '28', 'San FermÃ­n', 'San Fermin', 24.408899, -99.245002, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0);
INSERT INTO `geocities` (`id`, `country_code`, `state_code`, `accent`, `name`, `latitude`, `longitude`, `timezoneid`, `order_display`, `from_source`, `popularity`, `last_modified`, `name_fulltext`, `published`) VALUES
(640, 'MX', '21', 'San FÃ©lix Hidalgo', 'San Felix Hidalgo', 18.906999, -98.371902, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(641, 'MX', '20', 'San Felipe Usila', 'San Felipe Usila', 17.8864, -96.526199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(642, 'MX', '21', 'San Felipe Tepemaxalco', 'San Felipe Tepemaxalco', 18.733299, -98.599998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(643, 'MX', '21', 'TepatlÃ¡n', 'Tepatlan', 20.0911, -97.796203, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(644, 'MX', '21', 'San Felipe Teotlalcingo', 'San Felipe Teotlalcingo', 19.2334, -98.501701, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(645, 'MX', '21', 'San Felipe Tenextepec', 'San Felipe Tenextepec', 19.0314, -97.875099, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(646, 'MX', '27', 'San Felipe RÃ­o Nuevo', 'San Felipe Rio Nuevo', 18.149999, -93.716697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(647, 'MX', '21', 'San Felipe Otlaltepec', 'San Felipe Otlaltepec', 18.4011, -97.908096, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(648, 'MX', '20', 'San Felipe Jalapa De DÃ­az', 'San Felipe Jalapa De Diaz', 18.071599, -96.535202, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(649, 'MX', '29', 'San Felipe Ixtacuixtla', 'San Felipe Ixtacuixtla', 19.326599, -98.377502, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(650, 'MX', '21', 'San Felipe Hueyotlipan', 'San Felipe Hueyotlipan', 19.076099, -98.210296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(651, 'MX', '15', 'San Felipe Del Progreso', 'San Felipe Del Progreso', 19.694299, -99.957298, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(652, 'MX', '20', 'San Felipe Del Castillo', 'San Felipe Del Castillo', 18.0799, -96.148399, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(653, 'MX', '20', 'San Felipe Del Agua', 'San Felipe Del Agua', 17.107599, -96.711402, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(654, 'MX', '30', 'San Felipe De JesÃºs', 'San Felipe De Jesus', 17.7833, -95.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(655, 'MX', '15', 'San Felipe Coamango', 'San Felipe Coamango', 19.852899, -99.5914, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(656, 'MX', '19', 'San Felipe', 'San Felipe', 25.149999, -99.199996, 'America/Monterrey              ', 0, 'old', 30, '2017-10-04 06:05:26', '', 0),
(657, 'MX', '31', 'San Felipe', 'San Felipe', 21.5662, -88.232299, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(658, 'MX', '13', 'San Felipe', 'San Felipe', 20.799999, -99.433296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(659, 'MX', '15', 'San Felipe Santiago', 'San Felipe Santiago', 19.607599, -99.653297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(660, 'MX', '30', 'San Felipe', 'San Felipe', 19.146299, -96.693901, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(661, 'MX', '04', 'San Felipe', 'San Felipe', 18.1667, -91.983299, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(662, 'MX', '27', 'San Felipe', 'San Felipe', 17.7, -91.366699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(663, 'MX', '05', 'San Felipe', 'San Felipe', 16.724899, -92.6735, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(664, 'MX', '27', 'San Faustino', 'San Faustino', 18.0333, -93.366699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(665, 'MX', '28', 'San Eugenio', 'San Eugenio', 23.270299, -98.584701, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(666, 'MX', '29', 'San Esteban TizatlÃ¡n', 'San Esteban Tizatlan', 19.339599, -98.214599, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(667, 'MX', '21', 'San Esteban Necoxcalco', 'San Esteban Necoxcalco', 18.4556, -97.298797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(668, 'MX', '15', 'Cuecuecuatitla', 'Cuecuecuatitla', 19.0167, -98.8469, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(669, 'MX', '21', 'San Esteban Cuautempan', 'San Esteban Cuautempan', 19.9136, -97.795501, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(670, 'MX', '20', 'San Esteban Atatlahuca', 'San Esteban Atatlahuca', 17.0671, -97.6781, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(671, 'MX', '28', 'San Esteban', 'San Esteban', 22.940099, -97.995399, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(672, 'MX', '13', 'San Esteban', 'San Esteban', 20.5445, -98.059898, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(673, 'MX', '20', 'San Esteban', 'San Esteban', 15.883299, -97, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(674, 'MX', '05', 'San Esteban', 'San Esteban', 15.2333, -92.716697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(675, 'MX', '22', 'San Erasto', 'San Erasto', 21.566699, -99.2667, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(676, 'MX', '07', 'San Enrique', 'San Enrique', 27.7814, -99.848701, 'America/Chicago                ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(677, 'MX', '28', 'San Enrique', 'San Enrique', 23.797, -97.884803, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(678, 'MX', '31', 'San Enrique', 'San Enrique', 20.366699, -89.5, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(679, 'MX', '04', 'San Enrique', 'San Enrique', 19.25, -90.733299, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(680, 'MX', '05', 'San Enrique', 'San Enrique', 15.131099, -92.330101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(681, 'MX', '28', 'San ElÃ­as', 'San Elias', 23.45, -99.366699, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(682, 'MX', '31', 'San Eduardo', 'San Eduardo', 21.2332, -89.2761, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(683, 'MX', '29', 'San Dionisio Yauhquemehcan', 'San Dionisio Yauhquemehcan', 19.411899, -98.187797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(684, 'MX', '20', 'San Dionisio Viejo', 'San Dionisio Viejo', 16.2667, -94.849998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(685, 'MX', '20', 'San Dionisio OcotlÃ¡n', 'San Dionisio Ocotlan', 16.746999, -96.680397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(686, 'MX', '20', 'San Dionisio Ocotepec', 'San Dionisio Ocotepec', 16.804899, -96.392097, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(687, 'MX', '20', 'San Dionisio Del Mar', 'San Dionisio Del Mar', 16.322799, -94.757896, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(688, 'MX', '05', 'San Dionisio', 'San Dionisio', 16.4333, -93.666702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(689, 'MX', '31', 'San Dimas', 'San Dimas', 21.333299, -88.599998, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(690, 'MX', '04', 'San Dimas', 'San Dimas', 19.316699, -90.416702, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(691, 'MX', '15', 'San Dieguito Xochimanca', 'San Dieguito Xochimanca', 19.4894, -98.822898, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(692, 'MX', '24', 'San Dieguito', 'San Dieguito', 22.009099, -99.222396, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(693, 'MX', '15', 'San Diego Linares', 'San Diego Linares', 19.3794, -99.648597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(694, 'MX', '21', 'San Diego La Mesa Tochimiltzingo', 'San Diego La Mesa Tochimiltzingo', 18.809, -98.331802, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(695, 'MX', '15', 'San Diego Huehuecalco', 'San Diego Huehuecalco', 19.0921, -98.7639, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(696, 'MX', '15', 'San Diego De Los Padres', 'San Diego De Los Padres', 19.3724, -99.605697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(697, 'MX', '11', 'San Diego De Las Pitayas', 'San Diego De Las Pitayas', 21.3833, -100, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(698, 'MX', '21', 'San Diego Chalma', 'San Diego Chalma', 18.441999, -97.3675, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(699, 'MX', '29', 'San Diego Buena Vista', 'San Diego Buena Vista', 19.1667, -98.166702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(700, 'MX', '21', 'San Diego', 'San Diego', 18.954, -98.240798, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(701, 'MX', '19', 'San Diego', 'San Diego', 25.5816, -99.7835, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(702, 'MX', '28', 'San Diego', 'San Diego', 23.2171, -98.717796, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(703, 'MX', '30', 'San Diego', 'San Diego', 21.5, -98.366699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(704, 'MX', '15', 'San Diego', 'San Diego', 19.4608, -99.607597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(705, 'MX', '05', 'San Diego', 'San Diego', 15.766699, -93.300003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(706, 'MX', '28', 'SandÃ­a', 'Sandia', 26.089, -98.917701, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(707, 'MX', '13', 'SanctÃ³rum', 'Sanctorum', 20.301599, -98.7826, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(708, 'MX', '29', 'SanctÃ³rum', 'Sanctorum', 19.493099, -98.471801, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(709, 'MX', '21', 'SanctÃ³rum', 'Sanctorum', 19.1, -98.25, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(710, 'MX', '29', 'San CristÃ³bal Zacacalco', 'San Cristobal Zacacalco', 19.567399, -98.6613, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(711, 'MX', '21', 'San CristÃ³bal Tepetiopan', 'San Cristobal Tepetiopan', 18.532199, -97.527397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(712, 'MX', '20', 'San CristÃ³bal Suchixtlahuaca', 'San Cristobal Suchixtlahuaca', 17.727899, -97.367797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(713, 'MX', '15', 'San CristÃ³bal Los BaÃ±os', 'San Cristobal Los Banos', 19.6692, -99.835197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(714, 'MX', '20', 'San CristÃ³bal Lachirioag', 'San Cristobal Lachirioag', 17.3365, -96.164802, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(715, 'MX', '05', 'San CristÃ³bal De Las Casas', 'San Cristobal De Las Casas', 16.737899, -92.638198, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(716, 'MX', '20', 'San CristÃ³bal Amoltepec', 'San Cristobal Amoltepec', 17.2833, -97.573898, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(717, 'MX', '20', 'San CristÃ³bal AmatlÃ¡n', 'San Cristobal Amatlan', 16.318399, -96.411697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(718, 'MX', '28', 'San CristÃ³bal', 'San Cristobal', 25.7833, -97.533302, 'America/Matamoros              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(719, 'MX', '24', 'San CristÃ³bal', 'San Cristobal', 21.6802, -98.827598, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(720, 'MX', '31', 'San CristÃ³bal', 'San Cristobal', 20.45, -89.483299, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(721, 'MX', '15', 'San CristÃ³bal', 'San Cristobal', 19.7, -99.8833, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(722, 'MX', '15', 'San CrisÃ³bal Texcalucan', 'San Crisobal Texcalucan', 19.4081, -99.3274, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(723, 'MX', '27', 'San CristÃ³bal', 'San Cristobal', 18.566699, -92.699996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(724, 'MX', '30', 'San CristÃ³bal', 'San Cristobal', 17.809099, -94.539703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(725, 'MX', '20', 'San CristÃ³bal', 'San Cristobal', 17.7833, -96.233299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(726, 'MX', '12', 'San CristÃ³bal', 'San Cristobal', 17.276599, -99.942001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(727, 'MX', '31', 'San Crisanto', 'San Crisanto', 21.3533, -89.176696, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(728, 'MX', '31', 'San Cornelio', 'San Cornelio', 20.394699, -98.312698, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(729, 'MX', '05', 'San Clemente', 'San Clemente', 16.4311, -93.713699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(730, 'MX', '27', 'San Claudio', 'San Claudio', 17.316699, -91.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(731, 'MX', '24', 'San Ciro De Acosta', 'San Ciro De Acosta', 21.6518, -99.819099, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(732, 'MX', '28', 'EstaciÃ³n SÃ¡nchez', 'Estacion Sanchez', 27.475099, -99.647399, 'America/Matamoros              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(733, 'MX', '31', 'SancÃ©n', 'Sancen', 20.85, -88.2667, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(734, 'MX', '19', 'San Cayetano', 'San Cayetano', 26.25, -99.349998, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(735, 'MX', '28', 'San Cayetano', 'San Cayetano', 23.9074, -99.095596, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(736, 'MX', '30', 'San Cayetano', 'San Cayetano', 21.1667, -97.466697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(737, 'MX', '27', 'San Cayetano', 'San Cayetano', 18.347499, -93.205802, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(738, 'MX', '20', 'San Carlos Yautepec', 'San Carlos Yautepec', 16.4976, -96.106101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(739, 'MX', '05', 'San Carlos Del RÃ­o', 'San Carlos Del Rio', 16.2224, -91.465301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(740, 'MX', '28', 'San Carlos', 'San Carlos', 24.582199, -98.9421, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(741, 'MX', '31', 'San Carlos', 'San Carlos', 20.838199, -89.300102, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(742, 'MX', '04', 'San Carlos', 'San Carlos', 17.816699, -90.533302, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(743, 'MX', '30', 'San Carlos', 'San Carlos', 17.6833, -94.716697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(744, 'MX', '27', 'San Carlos', 'San Carlos', 17.5522, -91.154098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(745, 'MX', '05', 'San Carlos', 'San Carlos', 17.4333, -91.5, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(746, 'MX', '20', 'San Carlos', 'San Carlos', 17.194099, -94.924201, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(747, 'MX', '19', 'San Carlitos', 'San Carlitos', 24.771499, -99.724403, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(748, 'MX', '05', 'San Caralampio De Las Palmas', 'San Caralampio De Las Palmas', 15.9467, -92.039703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(749, 'MX', '05', 'San Caralampio', 'San Caralampio', 16.7383, -91.425003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(750, 'MX', '27', 'San CÃ¡ndido RÃ­o Seco', 'San Candido Rio Seco', 18.1, -93.349998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(751, 'MX', '13', 'San Buenaventura', 'San Buenaventura', 19.8171, -99.321899, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(752, 'MX', '29', 'San Buenaventura', 'San Buenaventura', 19.6093, -98.327697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(753, 'MX', '15', 'San Buenaventura', 'San Buenaventura', 19.264099, -99.674598, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(754, 'MX', '31', 'San Bruno', 'San Bruno', 21.316699, -89.400001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(755, 'MX', '28', 'San Blas', 'San Blas', 26.9995, -99.543502, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(756, 'MX', '29', 'San Blas', 'San Blas', 19.526399, -98.394699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(757, 'MX', '20', 'San Blas', 'San Blas', 17.3833, -96.800003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(758, 'MX', '20', 'San Bernardo Mixtepec', 'San Bernardo Mixtepec', 16.825899, -96.900299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(759, 'MX', '19', 'San Bernardo', 'San Bernardo', 25.6448, -98.934898, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(760, 'MX', '13', 'San Bernardo', 'San Bernardo', 20.819099, -98.725097, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(761, 'MX', '31', 'San Bernardo', 'San Bernardo', 20.233299, -89.349998, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(762, 'MX', '27', 'San Bernardo', 'San Bernardo', 17.4167, -92.783302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(763, 'MX', '21', 'San Bernardino Lagunas', 'San Bernardino Lagunas', 18.6042, -97.265403, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(764, 'MX', '31', 'San Bernardino', 'San Bernardino', 20.895, -89.398902, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(765, 'MX', '15', 'San Bernardino', 'San Bernardino', 19.4773, -98.895103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(766, 'MX', '20', 'San Bernardino', 'San Bernardino', 18.142, -97.010597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(767, 'MX', '29', 'Amaxac De Guerrero', 'Amaxac De Guerrero', 19.3463, -98.170196, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(768, 'MX', '31', 'San BernabÃ©', 'San Bernabe', 21.083299, -89.150001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(769, 'MX', '31', 'San Benito', 'San Benito', 21.214099, -89.512199, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(770, 'MX', '21', 'San Benito', 'San Benito', 19.0676, -98.427398, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(771, 'MX', '30', 'San Benito', 'San Benito', 18.1627, -95.380302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(772, 'MX', '20', 'San Benito', 'San Benito', 16.483299, -96.083297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(773, 'MX', '20', 'San Bartolo Yautepec', 'San Bartolo Yautepec', 16.4309, -95.974296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(774, 'MX', '13', 'San Bartolo Tutotepec', 'San Bartolo Tutotepec', 20.399299, -98.201103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(775, 'MX', '21', 'San Bartolo Teontepec', 'San Bartolo Teontepec', 18.4892, -97.526, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(776, 'MX', '15', 'San Bartolo OxtotitlÃ¡n', 'San Bartolo Oxtotitlan', 19.6142, -99.6175, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(777, 'MX', '20', 'San BartolomÃ© YucuaÃ±e', 'San Bartolome Yucuane', 17.238399, -97.449401, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(778, 'MX', '27', 'San BartolomÃ© Metlalohcan', 'San Bartolome Metlalohcan', 19.448499, -98.126098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(779, 'MX', '20', 'San BartolomÃ© Loxicha', 'San Bartolome Loxicha', 15.969699, -96.707496, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(780, 'MX', '29', 'San BartolomÃ© Del Monte', 'San Bartolome Del Monte', 19.562, -98.585601, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(781, 'MX', '20', 'San BartolomÃ© Ayautla', 'San Bartolome Ayautla', 18.0324, -96.669998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(782, 'MX', '05', 'San BartolomÃ©', 'San Bartolome', 16.577499, -93.927101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(783, 'MX', '15', 'San Bartolo Del Llano', 'San Bartolo Del Llano', 19.5998, -99.703796, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(784, 'MX', '28', 'San Bartolo De Benavides', 'San Bartolo De Benavides', 26.9167, -99.6333, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(785, 'MX', '15', 'San Bartolo Cuautlalpan', 'San Bartolo Cuautlalpan', 19.815099, -99.010398, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(786, 'MX', '20', 'San Bartolo Coyotepec', 'San Bartolo Coyotepec', 17.549999, -97.283302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(787, 'MX', '09', 'San Bartolo AtepehuacÃ¡n', 'San Bartolo Atepehuacan', 19.5, -99.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(788, 'MX', '09', 'San Bartolo Amayalco', 'San Bartolo Amayalco', 19.331199, -99.269798, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(789, 'MX', '28', 'San Bartolo', 'San Bartolo', 25.750099, -98.119796, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(790, 'MX', '13', 'San Bartolo', 'San Bartolo', 20.233299, -99.483299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(791, 'MX', '15', 'San Bartolo', 'San Bartolo', 19.866699, -99.830001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(792, 'MX', '04', 'San Bartolo', 'San Bartolo', 19.799999, -90.583297, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(793, 'MX', '09', 'San Bartolo Xicomulco', 'San Bartolo Xicomulco', 19.205299, -99.068397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(794, 'MX', '15', 'San Bartolito', 'San Bartolito', 19.083299, -99.666702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(795, 'MX', '20', 'San Baltazar Loxicha', 'San Baltazar Loxicha', 16.079799, -96.79, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(796, 'MX', '20', 'San Baltazar Guelavila', 'San Baltazar Guelavila', 16.796499, -96.306701, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(797, 'MX', '20', 'San Baltazar Chichicapan', 'San Baltazar Chichicapan', 16.7614, -96.4887, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(798, 'MX', '21', 'San Baltasar Xochitlaxco', 'San Baltasar Xochitlaxco', 19.9333, -97.849998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(799, 'MX', '21', 'San Baltasar Temaxcalac', 'San Baltasar Temaxcalac', 19.2698, -98.406997, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(800, 'MX', '20', 'San Baltasar Chivaguela', 'San Baltasar Chivaguela', 16.457799, -96.190498, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(801, 'MX', '21', 'San Baltasar Atlimeyaya', 'San Baltasar Atlimeyaya', 18.966699, -98.466697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(802, 'MX', '21', 'San Baltasar', 'San Baltasar', 19.0333, -98.199996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(803, 'MX', '20', 'Sanate', 'Sanate', 16.25, -97.683296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(804, 'MX', '31', 'San Arcadio', 'San Arcadio', 20.566699, -90.1333, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(805, 'MX', '21', 'San Aparicio', 'San Aparicio', 19.1, -98.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(806, 'MX', '15', 'San Antonio Zoyatzingo', 'San Antonio Zoyatzingo', 19.0914, -98.783798, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(807, 'MX', '15', 'San Antonio Zomeyucan', 'San Antonio Zomeyucan', 19.449699, -99.252899, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(808, 'MX', '13', 'San Antonio Zaragoza', 'San Antonio Zaragoza', 20.253999, -98.998497, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(809, 'MX', '05', 'San Antonio Zanja Seca', 'San Antonio Zanja Seca', 16.011299, -92.059501, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(810, 'MX', '31', 'San Antonio Xluch', 'San Antonio Xluch', 20.898799, -89.651, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(811, 'MX', '04', 'San Antonio Xkix', 'San Antonio Xkix', 19.7525, -90.137901, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(812, 'MX', '31', 'San Antonio Xiol', 'San Antonio Xiol', 20.9167, -89.5, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(813, 'MX', '13', 'San Antonio Tezoquipan', 'San Antonio Tezoquipan', 20.474199, -99.468498, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(814, 'MX', '21', 'San Antonio Texcala', 'San Antonio Texcala', 18.3971, -97.444702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(815, 'MX', '29', 'San Antonio Techalote', 'San Antonio Techalote', 19.527799, -98.357597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(816, 'MX', '31', 'San Antonio Sodzil', 'San Antonio Sodzil', 20.538999, -89.6054, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(817, 'MX', '20', 'San Antonio Sinicahua', 'San Antonio Sinicahua', 17.148, -97.571098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(818, 'MX', '31', 'San Antonio SihÃ³', 'San Antonio Siho', 20.4871, -90.164596, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(819, 'MX', '04', 'San Antonio SahcabchÃ©n', 'San Antonio Sahcabchen', 20.3068, -90.138, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(820, 'MX', '31', 'San Antonio SacnictÃ©', 'San Antonio Sacnicte', 20.899999, -89.816703, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(821, 'MX', '13', 'San Antonio Regla', 'San Antonio Regla', 20.233299, -98.550003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(822, 'MX', '28', 'San Antonio RayÃ³n', 'San Antonio Rayon', 22.4202, -98.4169, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(823, 'MX', '21', 'San Antonio RayÃ³n', 'San Antonio Rayon', 20.1121, -97.482597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(824, 'MX', '05', 'San Antonio Providencia', 'San Antonio Providencia', 17.866699, -93.216697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(825, 'MX', '19', 'San Antonio PeÃ±a Nevada', 'San Antonio Pena Nevada', 23.7411, -99.9822, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(826, 'MX', '13', 'San Antonio Oztoyuca', 'San Antonio Oztoyuca', 19.9288, -98.669296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(827, 'MX', '20', 'San Antonio Ozolotepec', 'San Antonio Ozolotepec', 16.079399, -96.291198, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(828, 'MX', '15', 'San Antonio Ometusco', 'San Antonio Ometusco', 19.7786, -98.628997, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(829, 'MX', '30', 'San Antonio Ojital', 'San Antonio Ojital', 20.45, -97.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(830, 'MX', '20', 'San Antonio OcotlÃ¡n', 'San Antonio Ocotlan', 16.6483, -98.166397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(831, 'MX', '28', 'San Antonio Nogalar', 'San Antonio Nogalar', 23.059999, -98.408996, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(832, 'MX', '20', 'San Antonio Nanahuatipan', 'San Antonio Nanahuatipan', 18.1357, -97.123497, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(833, 'MX', '05', 'San Antonio Miramar', 'San Antonio Miramar', 15.383899, -92.458297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(834, 'MX', '05', 'San Antonio La Pinada', 'San Antonio La Pinada', 15.617799, -92.323196, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(835, 'MX', '13', 'San Antonio La Palma', 'San Antonio La Palma', 20.25, -98.3833, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(836, 'MX', '05', 'San Antonio La Junta', 'San Antonio La Junta', 15.583299, -92.066703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(837, 'MX', '05', 'San Antonio JatÃ³n', 'San Antonio Jaton', 16.1872, -92.108901, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(838, 'MX', '28', 'San Antonio Grande', 'San Antonio Grande', 24.483299, -99.583297, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(839, 'MX', '13', 'San Antonio FrÃ­as', 'San Antonio Frias', 20.1, -98.400001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(840, 'MX', '20', 'San Antonio Encinal', 'San Antonio Encinal', 18.066699, -96.116699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(841, 'MX', '05', 'San Antonio El Porvenir', 'San Antonio El Porvenir', 15.942999, -91.899597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(842, 'MX', '20', 'San Antonio El Alto', 'San Antonio El Alto', 16.8157, -97.025596, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(843, 'MX', '31', 'San Antonio Dziskal', 'San Antonio Dziskal', 20.9333, -89.650001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(844, 'MX', '28', 'San Antonio De Reina', 'San Antonio De Reina', 26.0816, -99.065299, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(845, 'MX', '28', 'San Antonio Del RÃ­o', 'San Antonio Del Rio', 24.366699, -99.566703, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(846, 'MX', '21', 'San Antonio Del Puente', 'San Antonio Del Puente', 18.927, -98.192398, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(847, 'MX', '04', 'San Antonio De La Sabana', 'San Antonio De La Sabana', 18.1177, -91.695098, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(848, 'MX', '28', 'San Antonio De La PeÃ±a', 'San Antonio De La Pena', 24.361999, -98.858596, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(849, 'MX', '28', 'San Antonio De Abajo', 'San Antonio De Abajo', 24.5, -99.533302, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(850, 'MX', '29', 'Coaxomulco', 'Coaxomulco', 19.3519, -98.095596, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(851, 'MX', '13', 'San Antonio Corrales', 'San Antonio Corrales', 20.407199, -99.369796, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(852, 'MX', '31', 'San Antonio Chun', 'San Antonio Chun', 20.877799, -89.860397, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(853, 'MX', '05', 'San Antonio Chinchilla', 'San Antonio Chinchilla', 16.3826, -92.689903, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(854, 'MX', '31', 'San Antonio Castellanos', 'San Antonio Castellanos', 21.116699, -88.866699, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(855, 'MX', '21', 'San Antonio CaÃ±ada', 'San Antonio Canada', 18.5144, -97.293601, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(856, 'MX', '31', 'San Antonio CÃ¡mara', 'San Antonio Camara', 21.1805, -88.859497, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(857, 'MX', '20', 'San Antonio Buenavista', 'San Antonio Buenavista', 16.9393, -96.632202, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(858, 'MX', '05', 'San Antonio Buena Vista', 'San Antonio Buena Vista', 15.9833, -92.066703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(859, 'MX', '05', 'San Antonio BoquerÃ³n', 'San Antonio Boqueron', 16.7049, -93.179603, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(860, 'MX', '31', 'San Antonio Baas', 'San Antonio Baas', 21.083299, -88.866699, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(861, 'MX', '21', 'San Antonio Arcos', 'San Antonio Arcos', 19.0536, -97.488899, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(862, 'MX', '20', 'San Antonio Analco', 'San Antonio Analco', 17.84, -96.557197, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(863, 'MX', '21', 'Laguna De Alchichica', 'Laguna De Alchichica', 19.4067, -97.395599, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(864, 'MX', '20', 'San Antonio Acutla', 'San Antonio Acutla', 17.649999, -97.466697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(865, 'MX', '20', 'San Antonio Abad', 'San Antonio Abad', 17.898099, -97.451103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(866, 'MX', '28', 'San Antonio', 'San Antonio', 23.95, -99.25, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(867, 'MX', '30', 'San Antonio', 'San Antonio', 21.620599, -97.748703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(868, 'MX', '11', 'San Antonio', 'San Antonio', 21.1, -99.533302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(869, 'MX', '31', 'San Antonio', 'San Antonio', 21.0652, -89.674598, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(870, 'MX', '31', 'San Antonio Millet', 'San Antonio Millet', 20.972799, -89.397598, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(871, 'MX', '04', 'San Antonio', 'San Antonio', 20.399999, -90.300003, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(872, 'MX', '13', 'San Antonio Tula', 'San Antonio Tula', 20.081899, -99.430397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(873, 'MX', '21', 'San Antonio Metzompa', 'San Antonio Metzompa', 20.053899, -97.379699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(874, 'MX', '15', 'San Antonio', 'San Antonio', 19.75, -99.833297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(875, 'MX', '21', 'San Antonio', 'San Antonio', 19.2707, -97.186203, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(876, 'MX', '15', 'San Antonio Buenavista', 'San Antonio Buenavista', 19.261899, -99.7098, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(877, 'MX', '05', 'San Antonio', 'San Antonio', 17.733299, -93.216697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(878, 'MX', '20', 'San Antonio', 'San Antonio', 17.531099, -97.725601, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(879, 'MX', '31', 'San Anselmo', 'San Anselmo', 20.2667, -89.400001, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(880, 'MX', '24', 'San Ãngel', 'San Angel', 22.1, -98.816703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(881, 'MX', '04', 'San Ãngel', 'San Angel', 18.086799, -91.879203, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(882, 'MX', '05', 'San Ãngel', 'San Angel', 16.613599, -93.857803, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(883, 'MX', '20', 'San AndrÃ©s Zautla', 'San Andres Zautla', 17.1868, -96.864601, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(884, 'MX', '20', 'San AndrÃ©s Zabache', 'San Andres Zabache', 16.833299, -96.733299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(885, 'MX', '20', 'San AndrÃ©s Yatuni', 'San Andres Yatuni', 17.251199, -96.401397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(886, 'MX', '20', 'San AndrÃ©s YaÃ¡', 'San Andres Yaa', 17.2931, -96.1538, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(887, 'MX', '30', 'San AndrÃ©s Tuxtla', 'San Andres Tuxtla', 18.448699, -95.213302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(888, 'MX', '15', 'Timilpan', 'Timilpan', 19.875799, -99.734199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(889, 'MX', '09', 'San AndrÃ©s Tetepilco', 'San Andres Tetepilco', 19.366699, -99.1333, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(890, 'MX', '20', 'San AndrÃ©s Tepetlapa', 'San Andres Tepetlapa', 17.666599, -98.391899, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(891, 'MX', '15', 'San AndrÃ©s TepetitlÃ¡n', 'San Andres Tepetitlan', 18.866699, -99.866699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(892, 'MX', '20', 'San AndrÃ©s Teotilalpan', 'San Andres Teotilalpan', 17.954299, -96.656097, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(893, 'MX', '30', 'San AndrÃ©s Tenejapa', 'San Andres Tenejapa', 18.785699, -97.085998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(894, 'MX', '20', 'San AndrÃ©s Solaga', 'San Andres Solaga', 17.2716, -96.234199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(895, 'MX', '20', 'San AndrÃ©s Sinaxtla', 'San Andres Sinaxtla', 17.468999, -97.281997, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(896, 'MX', '21', 'San AndrÃ©s Payuca', 'San Andres Payuca', 19.525699, -97.6156, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(897, 'MX', '20', 'San AndrÃ©s PaxtlÃ¡n', 'San Andres Paxtlan', 16.2161, -96.508102, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(898, 'MX', '20', 'San AndrÃ©s NuxiÃ±o', 'San Andres Nuxino', 17.2385, -97.105796, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(899, 'MX', '20', 'San AndrÃ©s Mixtepec', 'San Andres Mixtepec', 16.325799, -96.326301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(900, 'MX', '13', 'San AndrÃ©s Miraflores', 'San Andres Miraflores', 20.8017, -99.000999, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(901, 'MX', '21', 'San AndrÃ©s Mimiahuapan', 'San Andres Mimiahuapan', 18.6867, -97.8852, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(902, 'MX', '20', 'San AndrÃ©s Lagunas', 'San Andres Lagunas', 17, -97.949996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(903, 'MX', '20', 'San AndrÃ©s Ixtlahuaca', 'San Andres Ixtlahuaca', 17.071399, -96.825996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(904, 'MX', '21', 'San AndrÃ©s Hueyacatitla', 'San Andres Hueyacatitla', 19.256399, -98.536697, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(905, 'MX', '20', 'San AndrÃ©s Huayapan', 'San Andres Huayapan', 17.103099, -96.664199, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(906, 'MX', '20', 'San AndrÃ©s Huaxpaltepec', 'San Andres Huaxpaltepec', 16.3297, -97.916702, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(907, 'MX', '20', 'San AndrÃ©s El Alto', 'San Andres El Alto', 16.8448, -97.052101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(908, 'MX', '13', 'San AndrÃ©s DevoxthÃ¡', 'San Andres Devoxtha', 20.5242, -99.064498, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(909, 'MX', '21', 'San AndrÃ©s Calpan', 'San Andres Calpan', 19.1039, -98.458, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(910, 'MX', '21', 'San AndrÃ©s Cacaloapan', 'San Andres Cacaloapan', 18.582799, -97.584899, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(911, 'MX', '20', 'San AndrÃ©s Cabecera Nueva', 'San Andres Cabecera Nueva', 16.888799, -97.680297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(912, 'MX', '29', 'San AndrÃ©s Buenavista', 'San Andres Buenavista', 19.569299, -98.271896, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(913, 'MX', '28', 'San AndrÃ©s', 'San Andres', 23.804899, -98.958602, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(914, 'MX', '31', 'San AndrÃ©s', 'San Andres', 21.216699, -89.333297, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(915, 'MX', '21', 'San AndrÃ©s', 'San Andres', 20.366699, -98.083297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(916, 'MX', '13', 'San AndrÃ©s', 'San Andres', 20.2695, -98.851196, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(917, 'MX', '09', 'San AndrÃ©s Totoltepec', 'San Andres Totoltepec', 19.2511, -99.172897, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(918, 'MX', '04', 'San AndrÃ©s', 'San Andres', 18.1833, -91.050003, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(919, 'MX', '30', 'San AndrÃ©s', 'San Andres', 17.639099, -95.223396, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(920, 'MX', '30', 'San Anastasio', 'San Anastasio', 18.45, -95.800003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(921, 'MX', '13', 'San Ambrosio', 'San Ambrosio', 20.528699, -98.020698, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(922, 'MX', '31', 'San Amado', 'San Amado', 20.8833, -89.466697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(923, 'MX', '30', 'San Alfonso', 'San Alfonso', 21.436899, -97.872703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(924, 'MX', '21', 'San Alfonso', 'San Alfonso', 19.739099, -97.916496, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(925, 'MX', '07', 'San Alejo', 'San Alejo', 27.996299, -100.001998, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(926, 'MX', '13', 'San Alejo', 'San Alejo', 20.149999, -98.283302, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(927, 'MX', '04', 'San Alejandro', 'San Alejandro', 18.9167, -90.916702, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(928, 'MX', '31', 'SanaktÃ©', 'Sanakte', 20.216699, -89.716697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(929, 'MX', '31', 'Sanahcat', 'Sanahcat', 20.7702, -89.214401, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(930, 'MX', '13', 'San AgustÃ­n ZapotlÃ¡n', 'San Agustin Zapotlan', 19.874599, -98.714103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(931, 'MX', '13', 'San AgustÃ­n Tlaxiaca', 'San Agustin Tlaxiaca', 20.115999, -98.885597, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(932, 'MX', '13', 'San AgustÃ­n Tlalixticapa', 'San Agustin Tlalixticapa', 20.483299, -99.433296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(933, 'MX', '20', 'San AgustÃ­n Tlacotepec', 'San Agustin Tlacotepec', 17.2024, -97.515998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(934, 'MX', '12', 'San AgustÃ­n Oapan', 'San Agustin Oapan', 17.9333, -99.433296, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(935, 'MX', '20', 'San AgustÃ­n Montelobos', 'San Agustin Montelobos', 17.566699, -97.300003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(936, 'MX', '20', 'San AgustÃ­n Loxicha', 'San Agustin Loxicha', 16.016599, -96.616699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(937, 'MX', '21', 'San AgustÃ­n Huexaxtla', 'San Agustin Huexaxtla', 18.9333, -98.400001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(938, 'MX', '20', 'San AgustÃ­n Etla', 'San Agustin Etla', 17.188499, -96.770797, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(939, 'MX', '19', 'San Agustine', 'San Agustine', 24.8915, -99.785896, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(940, 'MX', '19', 'San Agustin De Los Arroyos', 'San Agustin De Los Arroyos', 25.299999, -99.800003, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(941, 'MX', '20', 'San AgustÃ­n Chayuco', 'San Agustin Chayuco', 17.25, -98.0167, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(942, 'MX', '20', 'San AgustÃ­n Amatengo', 'San Agustin Amatengo', 16.5114, -96.791397, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(943, 'MX', '28', 'San AgustÃ­n', 'San Agustin', 22.7639, -98.997596, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(944, 'MX', '13', 'San AgustÃ­n', 'San Agustin', 20.066699, -99.083297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(945, 'MX', '15', 'San AgustÃ­n Altamirano', 'San Agustin Altamirano', 19.381099, -99.973503, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(946, 'MX', '04', 'San AgustÃ­n', 'San Agustin', 18.3773, -91.298599, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(947, 'MX', '05', 'San AgustÃ­n', 'San Agustin', 17.900499, -91.942703, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(948, 'MX', '20', 'San AgustÃ­n', 'San Agustin', 16.5, -94.333297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(949, 'MX', '05', 'Samuel LeÃ³n', 'Samuel Leon', 15.506899, -92.972602, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(950, 'MX', '31', 'SambulÃ¡', 'Sambula', 20.7, -89.800003, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(951, 'MX', '05', 'Samayoa', 'Samayoa', 15.05, -92.099998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(952, 'MX', '31', 'Samahil', 'Samahil', 20.882499, -89.887901, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(953, 'MX', '05', 'Salvatierra', 'Salvatierra', 16.483299, -92.333297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(954, 'MX', '05', 'Salvador Urbina', 'Salvador Urbina', 15.7809, -92.817001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(955, 'MX', '09', 'Salvador DÃ­az MirÃ³n', 'Salvador Diaz Miron', 19.483299, -99.099998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0);
INSERT INTO `geocities` (`id`, `country_code`, `state_code`, `accent`, `name`, `latitude`, `longitude`, `timezoneid`, `order_display`, `from_source`, `popularity`, `last_modified`, `name_fulltext`, `published`) VALUES
(956, 'MX', '20', 'Salvador', 'Salvador', 16.2667, -94.150001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(957, 'MX', '12', 'Salto De Valadez', 'Salto De Valadez', 17.466699, -99.366699, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(958, 'MX', '30', 'Salto De La PeÃ±a', 'Salto De La Pena', 20.833299, -97.400001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(959, 'MX', '24', 'Salto Del Agua', 'Salto Del Agua', 22.581199, -99.403701, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(960, 'MX', '04', 'Salto De Agua', 'Salto De Agua', 18.1833, -91.099998, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(961, 'MX', '05', 'Salto De Agua', 'Salto De Agua', 17.5513, -92.340301, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(962, 'MX', '04', 'Salto Ahogado', 'Salto Ahogado', 18.212299, -91.050697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(963, 'MX', '21', 'Saltillo', 'Saltillo', 19.296899, -97.295402, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(964, 'MX', '30', 'Saltabarranca', 'Saltabarranca', 18.590599, -95.531898, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(965, 'MX', '28', 'Salsipuedes', 'Salsipuedes', 25.8833, -97.550003, 'America/Matamoros              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(966, 'MX', '30', 'Salsipuedes', 'Salsipuedes', 22.355899, -98.150802, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(967, 'MX', '04', 'Salsipuedes', 'Salsipuedes', 18.0818, -91.636497, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(968, 'MX', '30', 'SalomÃ³n', 'Salomon', 17.983299, -94.800003, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(969, 'MX', '30', 'Salmoral', 'Salmoral', 19.3423, -96.357101, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(970, 'MX', '15', 'Salitrillo', 'Salitrillo', 19.4333, -99.849998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(971, 'MX', '24', 'Salitrero', 'Salitrero', 21.799999, -99.949996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(972, 'MX', '24', 'Salitrera', 'Salitrera', 21.649999, -99.949996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(973, 'MX', '28', 'Salitre', 'Salitre', 23.1, -99.416702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(974, 'MX', '22', 'Salitre', 'Salitre', 20.7, -99.650001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(975, 'MX', '15', 'El Salitre De MaÃ±ones', 'El Salitre De Manones', 19.4388, -99.788696, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(976, 'MX', '30', 'Salitral', 'Salitral', 18, -94.699996, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(977, 'MX', '05', 'Salitral', 'Salitral', 15.815199, -92.935501, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(978, 'MX', '28', 'Salinillas', 'Salinillas', 26.5564, -99.252403, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(979, 'MX', '19', 'Salinillas', 'Salinillas', 25.665899, -99.597, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(980, 'MX', '28', 'Salinas', 'Salinas', 26.2833, -98.816703, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(981, 'MX', '19', 'Salinas', 'Salinas', 25.85, -99.166702, 'America/Monterrey              ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(982, 'MX', '24', 'Salinas', 'Salinas', 22.7, -99.849998, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(983, 'MX', '15', 'Salinas', 'Salinas', 19.8034, -98.798103, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(984, 'MX', '30', 'Salinas', 'Salinas', 18.890199, -95.943, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(985, 'MX', '20', 'Salinas', 'Salinas', 17.799999, -98.233299, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(986, 'MX', '20', 'Salina Cruz', 'Salina Cruz', 16.187999, -95.199501, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(987, 'MX', '24', 'Salcedo', 'Salcedo', 21.866699, -98.900001, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(988, 'MX', '15', 'Salazar', 'Salazar', 19.308399, -99.388298, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(989, 'MX', '20', 'Salazar', 'Salazar', 16.4167, -95.333297, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(990, 'MX', '31', 'Sanlahtah', 'Sanlahtah', 21.0422, -89.146102, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(991, 'MX', '31', 'Sal', 'Sal', 20.25, -88.983299, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(992, 'MX', '31', 'Sak Akal', 'Sak Akal', 20.083299, -88.966697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(993, 'MX', '04', 'Sakakal', 'Sakakal', 19.2, -90.683296, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(994, 'MX', '30', 'Sahualaca', 'Sahualaca', 17.7189, -94.547096, 'America/Mexico_City            ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(995, 'MX', '04', 'SahpukenhÃ¡', 'Sahpukenha', 18.983299, -90.216697, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(996, 'MX', '31', 'SahÃ©', 'Sahe', 20.922199, -89.462097, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(997, 'MX', '04', 'Sahcacal', 'Sahcacal', 19.85, -90.366699, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(998, 'MX', '31', 'SahcabchÃ©n', 'Sahcabchen', 20.2812, -88.729698, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(999, 'MX', '04', 'SahcabchÃ©n', 'Sahcabchen', 19.8743, -89.927597, 'America/Merida                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(1000, 'MX', '23', 'SahcabchÃ©n', 'Sahcabchen', 19.4207, -88.361503, 'America/Cancun                 ', 0, 'old', 1, '2017-10-04 06:05:26', '', 0),
(1001, 'FR', 'B4', 'Zuytpeene', 'Zuytpeene', 50.7947, 2.430269, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zuytpeene', 0),
(1002, 'FR', 'C1', 'Zutzendorf', 'Zutzendorf', 48.8549, 7.550489, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zutzendorf', 0),
(1003, 'FR', 'B4', 'Zutkerque', 'Zutkerque', 50.853, 2.06818, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zutkerque', 0),
(1004, 'FR', 'A5', 'Zuani', 'Zuani', 42.270099, 9.344779, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zuani', 0),
(1005, 'FR', 'A5', 'Zoza', 'Zoza', 41.717098, 9.07094, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zoza', 0),
(1006, 'FR', 'B2', 'Zoufftgen', 'Zoufftgen', 49.461601, 6.133269, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zoufftgen', 0),
(1007, 'FR', 'B4', 'Zoteux', 'Zoteux', 50.610698, 1.879299, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zoteux', 0),
(1008, 'FR', 'B6', 'Zoteux', 'Zoteux', 50.075, 1.689939, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zoteux', 0),
(1009, 'FR', 'B2', 'Zommange', 'Zommange', 48.824199, 6.804309, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zommange', 0),
(1010, 'FR', 'C1', 'Zollingen', 'Zollingen', 48.9099, 7.074339, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zollingen', 0),
(1011, 'FR', 'C1', 'Zœbersdorf', 'Zoebersdorf', 48.793598, 7.52538, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zoebersdorf', 0),
(1012, 'FR', 'B2', 'Zilling', 'Zilling', 48.7845, 7.21148, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zilling', 0),
(1013, 'FR', 'A5', 'Zilia', 'Zilia', 42.529701, 8.902729, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zilia', 0),
(1014, 'FR', 'A5', 'Zigliara', 'Zigliara', 41.846199, 8.99409, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zigliara', 0),
(1015, 'FR', 'A5', 'Zifignola', 'Zifignola', 41.754699, 8.803819, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zifignola', 0),
(1016, 'FR', 'A5', 'Zicavo', 'Zicavo', 41.9067, 9.129309, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zicavo', 0),
(1017, 'FR', 'A5', 'Zévaco', 'Zevaco', 41.8833, 9.05, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zevaco', 0),
(1018, 'FR', 'B4', 'Zermezeele', 'Zermezeele', 50.824798, 2.450949, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zermezeele', 0),
(1019, 'FR', 'B4', 'Zelucq', 'Zelucq', 50.510101, 1.692639, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zelucq', 0),
(1020, 'FR', 'C1', 'Zelsheim', 'Zelsheim', 48.2985, 7.643219, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zelsheim', 0),
(1021, 'FR', 'C1', 'Zellenberg', 'Zellenberg', 48.169498, 7.320069, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zellenberg', 0),
(1022, 'FR', 'C1', 'Zehnacker', 'Zehnacker', 48.671298, 7.450789, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zehnacker', 0),
(1023, 'FR', 'B4', 'Zegerscappel', 'Zegerscappel', 50.8833, 2.4, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zegerscappel', 0),
(1024, 'FR', '98', 'Zanière', 'Zaniere', 45.4332, 3.0768, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zaniere', 0),
(1025, 'FR', '98', 'Zagat', 'Zagat', 45.3843, 3.08692, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zagat', 0),
(1026, 'FR', 'C1', 'Zaessingue', 'Zaessingue', 47.625301, 7.36594, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Zaessingue', 0),
(1027, 'FR', '97', 'Yzosse', 'Yzosse', 43.710998, -1.013049, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yzosse', 0),
(1028, 'FR', 'A3', 'Yzeures-Sur-Creuse', 'Yzeures-Sur-Creuse', 46.786098, 0.871659, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yzeures-Sur-Creuse', 0),
(1029, 'FR', 'B9', 'Yzeron', 'Yzeron', 45.707298, 4.590449, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yzeron', 0),
(1030, 'FR', 'B6', 'Yzengremer', 'Yzengremer', 50.061401, 1.522289, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yzengremer', 0),
(1031, 'FR', 'B6', 'Yvrench', 'Yvrench', 50.1791, 2.00318, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvrench', 0),
(1032, 'FR', 'B5', 'Yvré-L\'Évêque', 'Yvre-L\'Eveque', 48.014198, 0.26636, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvre-L\'Eveque', 0),
(1033, 'FR', 'B2', 'Yvraumont', 'Yvraumont', 48.9323, 5.03984, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvraumont', 0),
(1034, 'FR', '99', 'Yvrandes', 'Yvrandes', 48.719398, -0.751359, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvrandes', 0),
(1035, 'FR', 'B7', 'Yvrac-Et-Malleyrand', 'Yvrac-Et-Malleyrand', 45.750198, 0.448339, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvrac-Et-Malleyrand', 0),
(1036, 'FR', '97', 'Yvrac', 'Yvrac', 44.877899, -0.4587, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvrac', 0),
(1037, 'FR', 'A2', 'Yvignac-La-Tour', 'Yvignac-La-Tour', 48.349998, -2.18333, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvignac-La-Tour', 0),
(1038, 'FR', 'A2', 'Yvias', 'Yvias', 48.714099, -3.052, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvias', 0),
(1039, 'FR', 'A8', 'Yvette', 'Yvette', 48.716701, 1.916669, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvette', 0),
(1040, 'FR', '99', 'Yvetot-Bocage', 'Yvetot-Bocage', 49.4901, -1.5031, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvetot-Bocage', 0),
(1041, 'FR', 'B7', 'Yves', 'Yves', 46.019199, -1.048329, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yves', 0),
(1042, 'FR', 'B7', 'Yversay', 'Yversay', 46.682701, 0.22843, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yversay', 0),
(1043, 'FR', 'A4', 'Yvernaumont', 'Yvernaumont', 49.678798, 4.66026, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvernaumont', 0),
(1044, 'FR', 'A8', 'Yvernailles', 'Yvernailles', 48.618698, 2.920209, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvernailles', 0),
(1045, 'FR', 'A8', 'Département Des Yvelines', 'Departement Des Yvelines', 48.783298, 1.86667, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Departement Des Yvelines', 0),
(1046, 'FR', 'A7', 'Yvelin', 'Yvelin', 49.6039, 0.83077, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yvelin', 0),
(1047, 'FR', 'B4', 'Ytres', 'Ytres', 50.066699, 3, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ytres', 0),
(1048, 'FR', '98', 'Arrondissement D\'Yssingeaux', 'Arrondissement D\'Yssingeaux', 45.166698, 4.166669, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Arrondissement D\'Yssingeaux', 0),
(1049, 'FR', 'B1', 'Yssandon', 'Yssandon', 45.210998, 1.36827, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yssandon', 0),
(1050, 'FR', '98', 'Yssamat', 'Yssamat', 45.2089, 3.832809, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yssamat', 0),
(1051, 'FR', '98', 'Yssac-La-Tourette', 'Yssac-La-Tourette', 45.934101, 3.092259, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yssac-La-Tourette', 0),
(1052, 'FR', 'A1', 'Yrouerre', 'Yrouerre', 47.7938, 3.946899, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yrouerre', 0),
(1053, 'FR', '99', 'Yquelon', 'Yquelon', 48.848098, -1.55611, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yquelon', 0),
(1054, 'FR', 'A7', 'Yquebeuf', 'Yquebeuf', 49.597698, 1.25443, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yquebeuf', 0),
(1055, 'FR', 'A7', 'Ypreville-Biville', 'Ypreville-Biville', 49.693698, 0.534259, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ypreville-Biville', 0),
(1056, 'FR', 'A7', 'Ypreville', 'Ypreville', 49.7709, 0.485639, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ypreville', 0),
(1057, 'FR', 'A7', 'Yport', 'Yport', 49.737201, 0.315369, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yport', 0),
(1058, 'FR', '98', 'Youx', 'Youx', 46.144401, 2.80004, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Youx', 0),
(1059, 'FR', 'B6', 'Yonville', 'Yonville', 49.980998, 1.80884, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yonville', 0),
(1060, 'FR', 'A1', 'Département De L\'Yonne', 'Departement De L\'Yonne', 47.916698, 3.75, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Departement De L\'Yonne', 0),
(1061, 'FR', '98', 'Yolet', 'Yolet', 44.926399, 2.531739, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yolet', 0),
(1062, 'FR', 'A3', 'Ymorville', 'Ymorville', 48.3483, 1.63697, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ymorville', 0),
(1063, 'FR', 'A3', 'Ymeray', 'Ymeray', 48.509998, 1.700289, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ymeray', 0),
(1064, 'FR', '98', 'Ygrande', 'Ygrande', 46.5527, 2.942389, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ygrande', 0),
(1065, 'FR', '97', 'Ygos-Saint-Saturnin', 'Ygos-Saint-Saturnin', 43.976501, -0.7378, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ygos-Saint-Saturnin', 0),
(1066, 'FR', 'A2', 'Yffiniac', 'Yffiniac', 48.484298, -2.67647, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yffiniac', 0),
(1067, 'FR', 'A3', 'Yèvres', 'Yevres', 48.2108, 1.18717, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yevres', 0),
(1068, 'FR', 'A3', 'Yèvre-Le-Châtel', 'Yevre-Le-Chatel', 48.159999, 2.332969, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yevre-Le-Chatel', 0),
(1069, 'FR', 'B4', 'Yeuse', 'Yeuse', 50.8152, 1.97782, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yeuse', 0),
(1070, 'FR', 'A7', 'Yerville', 'Yerville', 49.667198, 0.89594, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yerville', 0),
(1071, 'FR', 'A3', 'Yerville', 'Yerville', 48.250499, 1.63229, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yerville', 0),
(1072, 'FR', 'A8', 'Yerres', 'Yerres', 48.7178, 2.49338, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yerres', 0),
(1073, 'FR', 'A8', 'Yèbles', 'Yebles', 48.636398, 2.768059, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yebles', 0),
(1074, 'FR', '98', 'Ydes', 'Ydes', 45.333301, 2.46667, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ydes', 0),
(1075, 'FR', '98', 'Ychamp', 'Ychamp', 45.6333, 3.48333, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ychamp', 0),
(1076, 'FR', 'A7', 'Yainville', 'Yainville', 49.453701, 0.8292, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yainville', 0),
(1077, 'FR', 'B2', 'Xousse', 'Xousse', 48.650001, 5.71667, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xousse', 0),
(1078, 'FR', 'B2', 'Xonville', 'Xonville', 49.054901, 5.84917, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xonville', 0),
(1079, 'FR', 'B2', 'Xonrupt-Longemer', 'Xonrupt-Longemer', 48.082199, 6.92944, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xonrupt-Longemer', 0),
(1080, 'FR', 'B2', 'Xocourt', 'Xocourt', 48.907901, 6.378489, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xocourt', 0),
(1081, 'FR', 'B2', 'Xivray-Et-Marvoisin', 'Xivray-Et-Marvoisin', 48.865001, 5.743519, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xivray-Et-Marvoisin', 0),
(1082, 'FR', 'B2', 'Xermaménil', 'Xermamenil', 48.5321, 6.46218, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xermamenil', 0),
(1083, 'FR', 'B2', 'Xaronval', 'Xaronval', 48.375701, 6.186429, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xaronval', 0),
(1084, 'FR', 'B5', 'Xanton-Chassenon', 'Xanton-Chassenon', 46.4533, -0.69524, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xanton-Chassenon', 0),
(1085, 'FR', 'B2', 'Xammes', 'Xammes', 48.974498, 5.85473, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xammes', 0),
(1086, 'FR', 'B7', 'Xaintray', 'Xaintray', 46.493099, -0.47868, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xaintray', 0),
(1087, 'FR', '97', 'Xaintrailles', 'Xaintrailles', 44.206001, 0.258509, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xaintrailles', 0),
(1088, 'FR', 'A3', 'Xaintes', 'Xaintes', 47.050201, 1.968379, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xaintes', 0),
(1089, 'FR', 'B2', 'Xaffévillers', 'Xaffevillers', 48.409, 6.611289, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Xaffevillers', 0),
(1090, 'FR', 'B4', 'Wulverdinghe', 'Wulverdinghe', 50.831401, 2.25621, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Wulverdinghe', 0),
(1091, 'FR', 'C1', 'Wolschwiller', 'Wolschwiller', 47.461299, 7.406529, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Wolschwiller', 0),
(1092, 'FR', 'C1', 'Wolfisheim', 'Wolfisheim', 48.5872, 7.667079, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Wolfisheim', 0),
(1093, 'FR', 'B6', 'Woirel', 'Woirel', 49.9593, 1.823019, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woirel', 0),
(1094, 'FR', 'B2', 'Woippy', 'Woippy', 49.149501, 6.149089, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woippy', 0),
(1095, 'FR', 'B2', 'Woinville', 'Woinville', 48.8968, 5.661379, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woinville', 0),
(1096, 'FR', 'B6', 'Woignarue', 'Woignarue', 50.110198, 1.495319, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woignarue', 0),
(1097, 'FR', 'C1', 'Wœrth', 'Woerth', 48.939498, 7.74279, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woerth', 0),
(1098, 'FR', 'C1', 'Woellenheim', 'Woellenheim', 48.6795, 7.51019, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woellenheim', 0),
(1099, 'FR', 'B2', 'Wœlfling-Lès-Sarreguemines', 'Woelfling-Les-Sarreguemines', 49.083301, 7.18333, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Woelfling-Les-Sarreguemines', 0),
(1100, 'FR', 'C1', 'Wiwersheim', 'Wiwersheim', 48.640098, 7.606319, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Wiwersheim', 0),
(1101, 'PH', '40', 'General Mariano Alvarez', 'General Mariano Alvarez', 14.2959, 121.005996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'General Mariano Alvarez', 0),
(1102, 'PH', '40', 'Kabangaan', 'Kabangaan', 14.165699, 121.010002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kabangaan', 0),
(1103, 'PH', '40', 'Santo Tomas', 'Santo Tomas', 14.3182, 121.074996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Santo Tomas', 0),
(1104, 'PH', '40', 'Per Mon', 'Per Mon', 14.328399, 121.067001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Per Mon', 0),
(1105, 'PH', '40', 'Habagatan Rayap', 'Habagatan Rayap', 14.295399, 121.236999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Habagatan Rayap', 0),
(1106, 'PH', '40', 'Laram', 'Laram', 14.3295, 121.022003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Laram', 0),
(1107, 'PH', '40', 'T. Tiago', 'T. Tiago', 14.329799, 121.031997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'T. Tiago', 0),
(1108, 'PH', '40', 'G. De Jesus', 'G. De Jesus', 14.3303, 121.042999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'G. De Jesus', 0),
(1109, 'PH', '40', 'Ganado', 'Ganado', 14.279999, 121.082, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ganado', 0),
(1110, 'PH', '40', 'Uno', 'Uno', 14.2783, 121.125, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Uno', 0),
(1111, 'PH', '40', 'Dos', 'Dos', 14.275699, 121.121002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dos', 0),
(1112, 'PH', '40', 'Baclaran', 'Baclaran', 14.2382, 121.168998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Baclaran', 0),
(1113, 'PH', '40', 'Turan', 'Turan', 14.0052, 121.014999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Turan', 0),
(1114, 'PH', '40', 'Palsara', 'Palsara', 14.008099, 121.11, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Palsara', 0),
(1115, 'PH', '40', 'Luta Del Sur', 'Luta Del Sur', 14.0339, 121.161003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Luta Del Sur', 0),
(1116, 'PH', '40', 'Wani-Wani', 'Wani-Wani', 14.0282, 121.077003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Wani-Wani', 0),
(1117, 'PH', '40', 'Maria Paz', 'Maria Paz', 14.038499, 121.072998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Maria Paz', 0),
(1118, 'PH', '40', 'San Gregorio', 'San Gregorio', 14.044199, 121.124, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'San Gregorio', 0),
(1119, 'PH', '40', 'San Pedro Eleven', 'San Pedro Eleven', 14.0464, 121.130996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'San Pedro Eleven', 0),
(1120, 'PH', '40', 'Makban', 'Makban', 14.097999, 121.218002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Makban', 0),
(1121, 'PH', '40', 'Pagaspas', 'Pagaspas', 14.1062, 121.120002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Pagaspas', 0),
(1122, 'PH', '40', 'Janopol Oriental', 'Janopol Oriental', 14.0928, 121.115997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Janopol Oriental', 0),
(1123, 'PH', '40', 'Miranda', 'Miranda', 14.109399, 121.014999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Miranda', 0),
(1124, 'PH', '40', 'Altura Bata', 'Altura Bata', 14.123, 121.073997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Altura Bata', 0),
(1125, 'PH', '40', 'Altura Matanda', 'Altura Matanda', 14.137599, 121.067001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Altura Matanda', 0),
(1126, 'PH', '40', 'Pantay Na Bata', 'Pantay Na Bata', 14.127799, 121.116996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Pantay Na Bata', 0),
(1127, 'PH', '40', 'Makiling', 'Makiling', 14.1561, 121.138, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Makiling', 0),
(1128, 'PH', '40', 'Kay-Anlog', 'Kay-Anlog', 14.1631, 121.122001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kay-Anlog', 0),
(1129, 'PH', '40', 'Suplang', 'Suplang', 14.152199, 121.068, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Suplang', 0),
(1130, 'PH', '40', 'Tartaria', 'Tartaria', 14.1982, 121.022003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Tartaria', 0),
(1131, 'PH', '40', 'Mapagong', 'Mapagong', 14.228099, 121.127998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Mapagong', 0),
(1132, 'PH', '40', 'Paciano Rizal', 'Paciano Rizal', 14.218299, 121.129997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Paciano Rizal', 0),
(1133, 'PH', '40', 'Batino', 'Batino', 14.2051, 121.137001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Batino', 0),
(1134, 'PH', '40', 'Lawa', 'Lawa', 14.207099, 121.143997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lawa', 0),
(1135, 'PH', '40', 'Masili', 'Masili', 14.1812, 121.195999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Masili', 0),
(1136, 'PH', '40', 'Uwisan', 'Uwisan', 14.2367, 121.174003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Uwisan', 0),
(1137, 'PH', '06', 'Habana', 'Habana', 11.880599, 121.916999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Habana', 0),
(1138, 'PH', '06', 'Agbading', 'Agbading', 11.878399, 122.023002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Agbading', 0),
(1139, 'PH', '06', 'Tolingon', 'Tolingon', 11.9024, 122.004997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Tolingon', 0),
(1140, 'PH', '06', 'Paquilawa', 'Paquilawa', 11.9082, 121.994003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Paquilawa', 0),
(1141, 'PH', '06', 'Namao', 'Namao', 11.9187, 121.968002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Namao', 0),
(1142, 'PH', '06', 'Buenavista', 'Buenavista', 11.815899, 122.079002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Buenavista', 0),
(1143, 'PH', '06', 'Culmes', 'Culmes', 11.7727, 122.098999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Culmes', 0),
(1144, 'PH', '41', 'Canubos', 'Canubos', 12.3246, 121.941001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Canubos', 0),
(1145, 'PH', '41', 'Bataha', 'Bataha', 12.317999, 121.985, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bataha', 0),
(1146, 'PH', '41', 'Agtinga', 'Agtinga', 12.2676, 122.041999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Agtinga', 0),
(1147, 'PH', '41', 'Lende', 'Lende', 12.053, 121.939002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lende', 0),
(1148, 'PH', '41', 'Bio-Os', 'Bio-Os', 12.919599, 121.728996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bio-Os', 0),
(1149, 'PH', '41', 'Aluyan', 'Aluyan', 12.565299, 121.508003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Aluyan', 0),
(1150, 'PH', '02', 'Carunuan', 'Carunuan', 18.267299, 122.108001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Carunuan', 0),
(1151, 'PH', '02', 'Santa Clara', 'Santa Clara', 18.274999, 122.074996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Santa Clara', 0),
(1152, 'PH', '02', 'Nangalulutan', 'Nangalulutan', 18.308, 122.079002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Nangalulutan', 0),
(1153, 'PH', '02', 'Allotan', 'Allotan', 18.357799, 122.103996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Allotan', 0),
(1154, 'PH', '02', 'Limbos', 'Limbos', 18.389299, 122.137001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Limbos', 0),
(1155, 'PH', '02', 'Punti', 'Punti', 18.3962, 122.130996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Punti', 0),
(1156, 'PH', '02', 'Bato', 'Bato', 18.463199, 122.167999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bato', 0),
(1157, 'PH', '02', 'Parada', 'Parada', 18.466199, 122.156997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Parada', 0),
(1158, 'PH', '02', 'Laya', 'Laya', 18.2891, 122.107002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Laya', 0),
(1159, 'PH', '06', 'Baclan', 'Baclan', 9.9291, 122.538002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Baclan', 0),
(1160, 'PH', '06', 'Yao-Yao', 'Yao-Yao', 9.9259, 122.453002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Yao-Yao', 0),
(1161, 'PH', '06', 'Basak', 'Basak', 9.895799, 122.536003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Basak', 0),
(1162, 'PH', '06', 'Calamanda', 'Calamanda', 9.8877, 122.458, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Calamanda', 0),
(1163, 'PH', '06', 'Cabia-An', 'Cabia-An', 9.8535, 122.615997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Cabia-An', 0),
(1164, 'PH', '06', 'Taamina', 'Taamina', 9.8514, 122.600997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Taamina', 0),
(1165, 'PH', '06', 'Sura', 'Sura', 9.841699, 122.606002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Sura', 0),
(1166, 'PH', '06', 'Molobolo', 'Molobolo', 9.8491, 122.582, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Molobolo', 0),
(1167, 'PH', '06', 'Eliban', 'Eliban', 9.839099, 122.391998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Eliban', 0),
(1168, 'PH', '02', 'Gabuam', 'Gabuam', 17.4911, 122.196998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Gabuam', 0),
(1169, 'PH', '02', 'Sangay', 'Sangay', 17.440399, 122.216003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Sangay', 0),
(1170, 'PH', '02', 'Fely', 'Fely', 17.4274, 122.226997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Fely', 0),
(1171, 'PH', '02', 'Nita', 'Nita', 17.4136, 122.228996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Nita', 0),
(1172, 'PH', '02', 'Maplas', 'Maplas', 17.1035, 122.046997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Maplas', 0),
(1173, 'PH', '02', 'Batang Labang', 'Batang Labang', 17.0941, 122.037002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Batang Labang', 0),
(1174, 'PH', '02', 'Villa Imelda', 'Villa Imelda', 17.107799, 122.013999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Villa Imelda', 0),
(1175, 'PH', '02', 'Santa Catalina', 'Santa Catalina', 17.0858, 122.015998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Santa Catalina', 0),
(1176, 'PH', '02', 'Sabiang', 'Sabiang', 17.0713, 122.016998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Sabiang', 0),
(1177, 'PH', '02', 'Cabeseria Siete', 'Cabeseria Siete', 17.054899, 121.973999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Cabeseria Siete', 0),
(1178, 'PH', '02', 'Sindon Maridi', 'Sindon Maridi', 17.055299, 122.024002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Sindon Maridi', 0),
(1179, 'PH', '02', 'Villa Lucban', 'Villa Lucban', 17.0128, 122.04, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Villa Lucban', 0),
(1180, 'PH', '02', 'Bigao', 'Bigao', 17.0463, 122.084999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bigao', 0),
(1181, 'PH', '06', 'Bagon', 'Bagon', 10.647399, 122.176002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bagon', 0),
(1182, 'PH', '06', 'Tuib', 'Tuib', 10.598099, 122.156997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Tuib', 0),
(1183, 'PH', '06', 'Guinotan', 'Guinotan', 10.634599, 122.191001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Guinotan', 0),
(1184, 'PH', '06', 'Paraon', 'Paraon', 10.6561, 122.167999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Paraon', 0),
(1185, 'PH', '06', 'Salvacion', 'Salvacion', 10.6384, 122.231002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Salvacion', 0),
(1186, 'PH', '06', 'Cabatbugan', 'Cabatbugan', 10.658399, 122.292999, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Cabatbugan', 0),
(1187, 'PH', '02', 'Culasi', 'Culasi', 17.1214, 122.435997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Culasi', 0),
(1188, 'PH', '02', 'Dimulid', 'Dimulid', 17.127099, 122.427001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dimulid', 0),
(1189, 'PH', '02', 'Talaytay', 'Talaytay', 17.1086, 122.440002, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Talaytay', 0),
(1190, 'PH', '02', 'Tacnalan', 'Tacnalan', 17.0993, 122.462997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Tacnalan', 0),
(1191, 'PH', '02', 'Sabang', 'Sabang', 17.112199, 122.480003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Sabang', 0),
(1192, 'PH', '02', 'Dimatog', 'Dimatog', 17.130899, 122.501998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dimatog', 0),
(1193, 'PH', '02', 'Dicocotan', 'Dicocotan', 17.137399, 122.507003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dicocotan', 0),
(1194, 'PH', '02', 'Dipaguiden', 'Dipaguiden', 17.1021, 122.522003, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dipaguiden', 0),
(1195, 'PH', '02', 'Ditubong', 'Ditubong', 17.0872, 122.427001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ditubong', 0),
(1196, 'PH', '02', 'Centro Lual', 'Centro Lual', 17.064399, 122.429, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Centro Lual', 0),
(1197, 'PH', '02', 'Dimalicu-Licu', 'Dimalicu-Licu', 17.0648, 122.436996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dimalicu-Licu', 0),
(1198, 'PH', '02', 'Dibungco', 'Dibungco', 17.053499, 122.442001, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dibungco', 0),
(1199, 'PH', '02', 'Canarozo', 'Canarozo', 17.052099, 122.443, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Canarozo', 0),
(1200, 'PH', '02', 'Dialomanay', 'Dialomanay', 17.065, 122.380996, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Dialomanay', 0),
(1201, 'CA', '01', 'Ksituan', 'Ksituan', 55.900001, -119.018997, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Ksituan', 0),
(1202, 'CA', '14', 'Kugaaruk', 'Kugaaruk', 68.535797, -89.824699, 'America/Cambridge_Bay          ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kugaaruk', 0),
(1203, 'CA', '14', 'Kugluktuk', 'Kugluktuk', 67.8274, -115.096, 'America/Cambridge_Bay          ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kugluktuk', 0),
(1204, 'CA', '03', 'Kulish', 'Kulish', 51.565299, -100.557998, 'America/Winnipeg               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kulish', 0),
(1205, 'CA', '08', 'Kumpfville', 'Kumpfville', 43.685699, -80.756599, 'America/Toronto                ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kumpfville', 0),
(1206, 'CA', '02', 'Kung', 'Kung', 54.0494, -132.570007, 'America/Vancouver              ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kung', 0),
(1207, 'CA', '11', 'Kuroki', 'Kuroki', 51.866699, -103.484001, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kuroki', 0),
(1208, 'CA', '11', 'Kutawa', 'Kutawa', 51.416801, -104.218002, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kutawa', 0),
(1209, 'CA', '10', 'Kuujjuaq', 'Kuujjuaq', 58.100299, -68.398498, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kuujjuaq', 0),
(1210, 'CA', '10', 'Kuujjuarapik', 'Kuujjuarapik', 55.283599, -77.749496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kuujjuarapik', 0),
(1211, 'CA', '11', 'Kyle', 'Kyle', 50.8334, -108.035003, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kyle', 0),
(1212, 'CA', '11', 'Kylemore', 'Kylemore', 51.900001, -103.634002, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kylemore', 0),
(1213, 'CA', '12', 'Kynocks', 'Kynocks', 61.261901, -135.977005, 'America/Whitehorse             ', 0, 'old', 1, '2018-11-05 11:32:39', 'Kynocks', 0),
(1214, 'CA', '10', 'Labelle', 'Labelle', 46.283401, -74.732597, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Labelle', 0),
(1215, 'CA', '07', 'Labelle', 'Labelle', 44.316898, -64.832099, 'America/Halifax                ', 0, 'old', 1, '2018-11-05 11:32:39', 'Labelle', 0),
(1216, 'CA', '10', 'La Bostonnais', 'La Bostonnais', 47.516799, -72.699096, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Bostonnais', 0),
(1217, 'CA', '05', 'Labrador City', 'Labrador City', 52.9463, -66.911399, 'America/Goose_Bay              ', 0, 'old', 1, '2018-11-05 11:32:39', 'Labrador City', 0),
(1218, 'CA', '10', 'Labrecque', 'Labrecque', 48.666801, -71.532402, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Labrecque', 0),
(1219, 'CA', '03', 'La Broquerie', 'La Broquerie', 49.3428, -96.955596, 'America/Winnipeg               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Broquerie', 0),
(1220, 'CA', '01', 'Labuma', 'Labuma', 52.3334, -113.802001, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Labuma', 0),
(1221, 'CA', '10', 'Lac-Akonapwehikan', 'Lac-Akonapwehikan', 47.500099, -74.482498, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Akonapwehikan', 0),
(1222, 'CA', '10', 'Lac-Ashuapmushuan', 'Lac-Ashuapmushuan', 49.166801, -73.749099, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Ashuapmushuan', 0),
(1223, 'CA', '10', 'Lac-Au-Brochet', 'Lac-Au-Brochet', 49.666801, -69.599098, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Au-Brochet', 0),
(1224, 'CA', '10', 'Lac-Aux-Sables', 'Lac-Aux-Sables', 46.866798, -72.399101, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Aux-Sables', 0),
(1225, 'CA', '10', 'Lac-Bazinet', 'Lac-Bazinet', 47.450099, -74.765899, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Bazinet', 0),
(1226, 'CA', '01', 'Lac Bellevue', 'Lac Bellevue', 53.816898, -111.334999, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Bellevue', 0),
(1227, 'CA', '10', 'Lac-Berlinguet', 'Lac-Berlinguet', 48.683399, -74.115798, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Berlinguet', 0),
(1228, 'CA', '10', 'Lac-Blanc', 'Lac-Blanc', 47.283401, -72.0158, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Blanc', 0),
(1229, 'CA', '10', 'Lac-Bouchette', 'Lac-Bouchette', 48.250099, -72.182403, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Bouchette', 0),
(1230, 'CA', '10', 'Lac-Boulé', 'Lac-Boule', 46.883399, -73.615898, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Boule', 0),
(1231, 'CA', '10', 'Lac-Brome', 'Lac-Brome', 45.2168, -72.5158, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Brome', 0),
(1232, 'CA', '10', 'Lac-Cabasta', 'Lac-Cabasta', 47.533401, -74.549201, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Cabasta', 0),
(1233, 'CA', '10', 'Lac-Castor', 'Lac-Castor', 46.367, -74.8796, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Castor', 0),
(1234, 'CA', '10', 'Lac-Chat', 'Lac-Chat', 47.166801, -72.649101, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Chat', 0),
(1235, 'CA', '10', 'Lac-Chicobi', 'Lac-Chicobi', 48.883499, -78.499603, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Chicobi', 0),
(1236, 'CA', '10', 'Lac-De La Bidière', 'Lac-De La Bidiere', 47.433399, -75.082496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-De La Bidiere', 0),
(1237, 'CA', '10', 'Lac-De-La-Maison-De-Pierre', 'Lac-De-La-Maison-De-Pierre', 46.883399, -74.699203, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-De-La-Maison-De-Pierre', 0),
(1238, 'CA', '10', 'Lac-De-La-Pomme', 'Lac-De-La-Pomme', 47.183399, -74.515899, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-De-La-Pomme', 0),
(1239, 'CA', '10', 'Lac-Des-Dix-Milles', 'Lac-Des-Dix-Milles', 46.442001, -74.386398, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Des-Dix-Milles', 0),
(1240, 'CA', '10', 'Lac-Des-Moires', 'Lac-Des-Moires', 47.700099, -72.032402, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Des-Moires', 0),
(1241, 'CA', '10', 'Lac-Despinassy', 'Lac-Despinassy', 48.7835, -77.332801, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Despinassy', 0),
(1242, 'CA', '10', 'Lac-Des-Plages', 'Lac-Des-Plages', 46.000099, -74.899299, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Des-Plages', 0),
(1243, 'CA', '10', 'Lac-Des-Seize-Îles', 'Lac-Des-Seize-Iles', 45.9001, -74.465896, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Des-Seize-Iles', 0),
(1244, 'CA', '10', 'Lac-Devenyns', 'Lac-Devenyns', 47.0834, -73.832496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Devenyns', 0),
(1245, 'CA', '10', 'Lac-Douaire', 'Lac-Douaire', 47.166801, -75.799301, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Douaire', 0),
(1246, 'CA', '10', 'Lac-Drolet', 'Lac-Drolet', 45.7168, -70.848999, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Drolet', 0),
(1247, 'CA', '03', 'Lac Du Bonnet', 'Lac Du Bonnet', 50.25, -96.168098, 'America/Winnipeg               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Du Bonnet', 0),
(1248, 'CA', '10', 'Lac-Du-Cerf', 'Lac-Du-Cerf', 46.300098, -75.499298, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Du-Cerf', 0),
(1249, 'CA', '10', 'Lac-Duparquet', 'Lac-Duparquet', 48.466899, -79.266296, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Duparquet', 0),
(1250, 'CA', '10', 'Lac-Du-Taureau', 'Lac-Du-Taureau', 47.250099, -74.6492, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Du-Taureau', 0),
(1251, 'CA', '10', 'Lac-Édouard', 'Lac-Edouard', 47.6501, -72.2658, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Edouard', 0),
(1252, 'CA', '10', 'Lac-Ernest', 'Lac-Ernest', 46.183399, -75.199302, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Ernest', 0),
(1253, 'CA', '10', 'Lac-Fouillac', 'Lac-Fouillac', 48.0335, -78.299598, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Fouillac', 0),
(1254, 'CA', '10', 'Lac-Granet', 'Lac-Granet', 47.7835, -77.516197, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Granet', 0),
(1255, 'CA', '10', 'Lachute', 'Lachute', 45.6501, -74.332496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lachute', 0),
(1256, 'CA', '10', 'Lac-Jérôme', 'Lac-Jerome', 50.850101, -63.548599, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Jerome', 0),
(1257, 'CA', '10', 'Lac-Juillet', 'Lac-Juillet', 54.783401, -63.998298, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Juillet', 0),
(1258, 'CA', '01', 'Lac La Biche', 'Lac La Biche', 54.766899, -111.969001, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac La Biche', 0),
(1259, 'CA', '01', 'Lac La Nonne', 'Lac La Nonne', 53.916801, -114.302001, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac La Nonne', 0),
(1260, 'CA', '10', 'Lac-Lapeyrère', 'Lac-Lapeyrere', 47.2168, -72.365798, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Lapeyrere', 0),
(1261, 'CA', '10', 'Lac-Legendre', 'Lac-Legendre', 46.683399, -74.365898, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Legendre', 0),
(1262, 'CA', '02', 'Lac Le Jeune', 'Lac Le Jeune', 50.4831, -120.502998, 'America/Vancouver              ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Le Jeune', 0),
(1263, 'CA', '10', 'Lac-Lenôtre', 'Lac-Lenotre', 47.350101, -76.0326, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Lenotre', 0),
(1264, 'CA', '08', 'Laclu', 'Laclu', 49.783298, -94.700401, 'America/Winnipeg               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Laclu', 0),
(1265, 'CA', '10', 'Lac-Marguerite', 'Lac-Marguerite', 47.033401, -75.799301, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Marguerite', 0),
(1266, 'CA', '10', 'Lac-Masketsi', 'Lac-Masketsi', 47.000099, -72.549102, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Masketsi', 0),
(1267, 'CA', '10', 'Lac-Matawin', 'Lac-Matawin', 46.816799, -74.299201, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Matawin', 0),
(1268, 'CA', '10', 'Lac-Mégantic', 'Lac-Megantic', 45.5834, -70.882301, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Megantic', 0),
(1269, 'CA', '10', 'Lac-Metei', 'Lac-Metei', 47.829399, -76.791099, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Metei', 0),
(1270, 'CA', '10', 'Lac-Minaki', 'Lac-Minaki', 46.883399, -73.482498, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Minaki', 0),
(1271, 'CA', '10', 'Lac-Mondor', 'Lac-Mondor', 46.625598, -72.763496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Mondor', 0),
(1272, 'CA', '10', 'Lac-Moselle', 'Lac-Moselle', 47.516799, -75.582603, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Moselle', 0),
(1273, 'CA', '10', 'Lac-Nilgaut', 'Lac-Nilgaut', 46.600101, -77.249496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Nilgaut', 0),
(1274, 'CA', '10', 'Lac-Normand', 'Lac-Normand', 47.0834, -73.232398, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Normand', 0),
(1275, 'CA', '10', 'Lacolle', 'Lacolle', 45.0834, -73.365898, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lacolle', 0),
(1276, 'CA', '01', 'Lacombe', 'Lacombe', 52.4668, -113.735, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lacombe', 0),
(1277, 'CA', '01', 'Lacombe County', 'Lacombe County', 52.448699, -113.724998, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lacombe County', 0),
(1278, 'CA', '10', 'La Conception', 'La Conception', 46.1501, -74.699203, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Conception', 0),
(1279, 'CA', '07', 'Laconia', 'Laconia', 44.333499, -64.648803, 'America/Halifax                ', 0, 'old', 1, '2018-11-05 11:32:39', 'Laconia', 0),
(1280, 'CA', '11', 'Lacordaire', 'Lacordaire', 49.033401, -105.867996, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lacordaire', 0),
(1281, 'CA', '01', 'La Corey', 'La Corey', 54.450199, -110.767997, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Corey', 0),
(1282, 'CA', '10', 'La Corne', 'La Corne', 48.3502, -77.999496, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Corne', 0);
INSERT INTO `geocities` (`id`, `country_code`, `state_code`, `accent`, `name`, `latitude`, `longitude`, `timezoneid`, `order_display`, `from_source`, `popularity`, `last_modified`, `name_fulltext`, `published`) VALUES
(1283, 'CA', '10', 'Lac-Oscar', 'Lac-Oscar', 47.283401, -75.199203, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Oscar', 0),
(1284, 'CA', '10', 'La Côte-De-Gaspé', 'La Cote-De-Gaspe', 49.166801, -64.915397, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Cote-De-Gaspe', 0),
(1285, 'CA', '10', 'Lac-Pellerin', 'Lac-Pellerin', 47.783401, -74.615898, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Pellerin', 0),
(1286, 'CA', '11', 'Lac Pelletier', 'Lac Pelletier', 49.933399, -107.850997, 'America/Regina                 ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Pelletier', 0),
(1287, 'CA', '10', 'Lac-Pythonga', 'Lac-Pythonga', 46.383399, -76.432701, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Pythonga', 0),
(1288, 'CA', '01', 'La Crête', 'La Crete', 58.183498, -116.402999, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Crete', 0),
(1289, 'CA', '10', 'La Croche', 'La Croche', 47.600101, -72.732398, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'La Croche', 0),
(1290, 'CA', '10', 'Lac-Saguay', 'Lac-Saguay', 46.498901, -75.144897, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Saguay', 0),
(1291, 'CA', '10', 'Lac-Sainte-Marie', 'Lac-Sainte-Marie', 45.950099, -75.949302, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Sainte-Marie', 0),
(1292, 'CA', '10', 'Lac-Saint-Jean-Est', 'Lac-Saint-Jean-Est', 48.550098, -71.649101, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Saint-Jean-Est', 0),
(1293, 'CA', '10', 'Lac-Saint-Paul', 'Lac-Saint-Paul', 46.733398, -75.316001, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Saint-Paul', 0),
(1294, 'CA', '10', 'Lac-Santé', 'Lac-Sante', 46.616798, -74.482498, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Sante', 0),
(1295, 'CA', '08', 'Lac Seul', 'Lac Seul', 50.384899, -92.405296, 'America/Winnipeg               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Seul', 0),
(1296, 'CA', '10', 'Lac-Simon', 'Lac-Simon', 45.9001, -75.099296, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Simon', 0),
(1297, 'CA', '01', 'Lac Ste. Anne', 'Lac Ste. Anne', 53.683399, -114.434997, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Ste. Anne', 0),
(1298, 'CA', '01', 'Lac Ste. Anne County', 'Lac Ste. Anne County', 53.8465, -114.696998, 'America/Edmonton               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac Ste. Anne County', 0),
(1299, 'CA', '10', 'Lac-Supérieur', 'Lac-Superieur', 46.200099, -74.465896, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Superieur', 0),
(1300, 'CA', '10', 'Lac-Tourlay', 'Lac-Tourlay', 47.8334, -71.965797, 'America/Montreal               ', 0, 'old', 1, '2018-11-05 11:32:39', 'Lac-Tourlay', 0),
(1692184, 'PH', '11', 'Quiagot', 'Quiapo', 6.80806, 125.36, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Quiagot', 1),
(1692218, 'PH', '07', 'Bugang Sur', 'Quezon', 9.7501, 124.128997, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Bugang Sur', 1),
(1701667, 'PH', '41', 'Manila', 'Manila', 12.369, 121.335998, 'Asia/Manila                    ', 0, 'old', 1, '2018-11-05 11:32:39', 'Manila', 1),
(3017680, 'FR', '98', 'Forville', 'Forville', 45.188701, 2.85889, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Fortuniez', 1),
(6425695, 'FR', 'B8', 'Villefranche-Sur-Mer', 'Vallauris', 43.7042, 7.311669, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Villefranche-Sur-Mer', 1),
(6453748, 'FR', 'B1', 'Vierzon', 'Vierzon', 45.381099, 2.27111, 'Europe/Paris                   ', 0, 'old', 1, '2018-11-05 11:32:39', 'Neuvic', 1);

-- --------------------------------------------------------

--
-- Table structure for table `geocountries`
--

CREATE TABLE `geocountries` (
  `id` int(11) NOT NULL,
  `code` char(2) NOT NULL COMMENT 'Two-letter country code (ISO 3166-1 alpha-2)',
  `name` varchar(255) NOT NULL COMMENT 'English country name',
  `full_name` varchar(255) NOT NULL COMMENT 'Full English country name',
  `iso3` char(3) NOT NULL COMMENT 'Three-letter country code (ISO 3166-1 alpha-3)',
  `number` smallint(3) UNSIGNED ZEROFILL NOT NULL DEFAULT '000',
  `dialing_code` int(11) NOT NULL,
  `continent_code` char(2) NOT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `ioc_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `popularity` int(11) NOT NULL DEFAULT '1',
  `capital_city` bigint(20) UNSIGNED DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used_by_safe_charge` tinyint(1) NOT NULL DEFAULT '0',
  `bounding_box_north` decimal(10,6) DEFAULT NULL,
  `bounding_box_east` decimal(10,6) DEFAULT NULL,
  `bounding_box_south` decimal(10,6) DEFAULT NULL,
  `bounding_box_west` decimal(10,6) DEFAULT NULL,
  `bounding_box` polygon DEFAULT NULL,
  `flag_icon` varchar(20) NOT NULL,
  `search_hotels` tinyint(1) DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geocountries`
--

INSERT INTO `geocountries` (`id`, `code`, `name`, `full_name`, `iso3`, `number`, `dialing_code`, `continent_code`, `latitude`, `longitude`, `ioc_code`, `popularity`, `capital_city`, `last_modified`, `used_by_safe_charge`, `bounding_box_north`, `bounding_box_east`, `bounding_box_south`, `bounding_box_west`, `bounding_box`, `flag_icon`, `search_hotels`, `published`) VALUES
(1, 'AD', 'Andorra', 'Principality of Andorra', 'AND', 020, 376, 'EU', 42.546245, 1.601554, 'AND', 1, 1059905, '2019-11-22 02:53:12', 1, '42.656044', '1.786543', '42.428493', '1.407187', '\0\0\0\0\0\0\0\0\0\0\0\0\0]Þ®•ü?W^ò?ùSE@]Þ®•ü?PÈÎÛØ6E@ž³„Öƒö?PÈÎÛØ6E@ž³„Öƒö?W^ò?ùSE@]Þ®•ü?W^ò?ùSE@', 'country_flag_ad.png', 1, 0),
(2, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', 784, 971, 'AS', 23.424076, 53.847818, 'UAE', 1, 1060174, '2019-11-22 02:53:12', 1, '26.084160', '56.381660', '22.633329', '51.583328', '\0\0\0\0\0\0\0\0\0\0\0\0\0~!<Ú0L@¡¡‚‹:@~!<Ú0L@¿›nÙ!¢6@©kí}ªÊI@¿›nÙ!¢6@©kí}ªÊI@¡¡‚‹:@~!<Ú0L@¡¡‚‹:@', 'country_flag_ae.png', 1, 0),
(3, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', 004, 93, 'AS', 33.93911, 67.709953, 'AFG', 1, 1087919, '2019-11-21 08:30:07', 1, '38.483418', '74.879448', '29.377472', '60.478443', '\0\0\0\0\0\0\0\0\0\0\0\0\0¢CàH¸R@&\Z¤à=C@¢CàH¸R@Ý\'G¢`=@\0Çž==N@Ý\'G¢`=@\0Çž==N@&\Z¤à=C@¢CàH¸R@&\Z¤à=C@', 'country_flag_af.png', 0, 0),
(4, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', 028, 1268, 'NA', 17.060816, -61.796428, 'ANT', 1, NULL, '2019-11-21 08:30:07', 1, '17.729387', '-61.672421', '16.996979', '-61.906425', '\0\0\0\0\0\0\0\0\0\0\0\0\0b.äÖNÀ§ ?¹º1@b.äÖNÀ}Ì:ÿ0@n£¼ôNÀ}Ì:ÿ0@n£¼ôNÀ§ ?¹º1@b.äÖNÀ§ ?¹º1@', 'country_flag_ag.png', 1, 0),
(5, 'AI', 'Anguilla', 'Anguilla', 'AIA', 660, 1264, 'NA', 18.220554, -63.068615, 'AIA', 1, NULL, '2019-11-21 08:30:07', 1, '18.283424', '-62.971359', '18.166815', '-63.172901', '\0\0\0\0\0\0\0\0\0\0\0\0\0oÖà}U|OÀÉæªyŽH2@oÖà}U|OÀt{Ic´*2@\r9¶ž!–OÀt{Ic´*2@\r9¶ž!–OÀÉæªyŽH2@oÖà}U|OÀÉæªyŽH2@', 'country_flag_ai.png', 1, 0),
(6, 'AL', 'Albania', 'Republic of Albania', 'ALB', 008, 355, 'EU', 41.153332, 20.168331, 'ALB', 1, 1090498, '2019-11-21 08:30:07', 1, '42.665611', '21.068472', '39.648361', '19.293972', '\0\0\0\0\0\0\0\0\0\0\0\0\0±ˆa‡5@ÎmÂ½2UE@±ˆa‡5@=€E~ýÒC@‘ð½¿AK3@=€E~ýÒC@‘ð½¿AK3@ÎmÂ½2UE@±ˆa‡5@ÎmÂ½2UE@', 'country_flag_al.png', 1, 0),
(7, 'AM', 'Armenia', 'Republic of Armenia', 'ARM', 051, 374, 'AS', 40.069099, 45.038189, 'ARM', 1, 1092901, '2019-11-21 08:30:07', 1, '41.301834', '46.772435', '38.830528', '43.449780', '\0\0\0\0\0\0\0\0\0\0\0\0\0”¤k&ßbG@i¢¦D@”¤k&ßbG@Æ4Ó½NjC@‹2d’¹E@Æ4Ó½NjC@‹2d’¹E@i¢¦D@”¤k&ßbG@i¢¦D@', 'country_flag_am.png', 1, 0),
(8, 'AO', 'Angola', 'Republic of Angola', 'AGO', 024, 244, 'AF', -11.202692, 17.873887, 'ANG', 1, 1096668, '2019-11-21 08:30:07', 1, '-4.376826', '24.082119', '-18.042076', '11.679219', '\0\0\0\0\0\0\0\0\0\0\0\0\0Va3À8@G!É¬ÞÀVa3À8@Nò#~Å\n2ÀÆ¤¿—Â[\'@Nò#~Å\n2ÀÆ¤¿—Â[\'@G!É¬ÞÀVa3À8@G!É¬ÞÀ', 'country_flag_ao.png', 1, 0),
(9, 'AQ', 'Antarctica', 'Antarctica (the territory South of 60 deg S)', 'ATA', 010, 672, 'AN', -82.862752, -135, 'ATA', 1, NULL, '2019-11-21 08:30:07', 1, '-60.515533', '179.999900', '-89.999900', '-179.999900', '\0\0\0\0\0\0\0\0\0\0\0\0\0§èH.ÿf@?üüANÀ§èH.ÿf@NÑ‘\\þVÀ§èH.ÿfÀNÑ‘\\þVÀ§èH.ÿfÀ?üüANÀ§èH.ÿf@?üüANÀ', 'country_flag_aq.png', 0, 0),
(10, 'AR', 'Argentina', 'Argentine Republic', 'ARG', 032, 54, 'SA', -38.416097, -63.616672, 'ARG', 1, 1108510, '2019-11-21 08:30:07', 1, '-21.781277', '-53.591835', '-55.061314', '-73.582970', '\0\0\0\0\0\0\0\0\0\0\0\0\0hÐÐ?ÁËJÀóüÄÈ5ÀhÐÐ?ÁËJÀ»d#Ù‡KÀ%#gaOeRÀ»d#Ù‡KÀ%#gaOeRÀóüÄÈ5ÀhÐÐ?ÁËJÀóüÄÈ5À', 'country_flag_ar.png', 1, 0),
(11, 'AS', 'American Samoa', 'American Samoa', 'ASM', 016, 1684, 'OC', -14.305941, -170.6962, 'ASA', 1, 1114398, '2019-11-21 08:30:07', 1, '-11.049700', '-169.416077', '-14.382478', '-171.091888', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ås¶€P-eÀï8EGr&ÀÅs¶€P-eÀá\n(ÔÃ,À£\\\Z¿ðbeÀá\n(ÔÃ,À£\\\Z¿ðbeÀï8EGr&ÀÅs¶€P-eÀï8EGr&À', 'country_flag_as.png', 1, 0),
(12, 'AT', 'Austria', 'Republic of Austria', 'AUT', 040, 43, 'EU', 47.516231, 14.550072, 'AUT', 1, 1115785, '2019-11-21 08:30:07', 1, '49.021163', '17.162069', '46.372652', '9.530952', '\0\0\0\0\0\0\0\0\0\0\0\0\0²žZ})1@Pqxµ‚H@²žZ})1@þdŒ³/G@Ø}ÇðØ#@þdŒ³/G@Ø}ÇðØ#@Pqxµ‚H@²žZ})1@Pqxµ‚H@', 'country_flag_at.png', 1, 0),
(13, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', 036, 61, 'OC', -25.274398, 133.775136, 'AUS', 1, 1134797, '2019-11-22 03:20:46', 1, '-10.062805', '153.639252', '-43.643970', '112.911057', '\0\0\0\0\0\0\0\0\0\0\0\0\0â<œÀt4c@ò\Zú\' $Àâ<œÀt4c@uÍä›mÒEÀ®òÂN:\\@uÍä›mÒEÀ®òÂN:\\@ò\Zú\' $Àâ<œÀt4c@ò\Zú\' $À', 'country_flag_au.png', 1, 1),
(14, 'AW', 'Aruba', 'Aruba', 'ABW', 533, 297, 'NA', 12.52111, -69.968338, 'ARU', 1, 1136554, '2019-11-21 08:30:07', 1, '12.623718', '-69.865751', '12.411708', '-70.064474', '\0\0\0\0\0\0\0\0\0\0\0\0\0­ÞávhwQÀbÚ7÷W?)@­ÞávhwQÀÎ\0dËÒ(@O\\ŽW „QÀÎ\0dËÒ(@O\\ŽW „QÀbÚ7÷W?)@­ÞávhwQÀbÚ7÷W?)@', 'country_flag_aw.png', 1, 0),
(15, 'AX', 'Ã…land Islands', 'Ã…land Islands', 'ALA', 248, 358, 'EU', 60.338549, 20.271258, 'ALA', 1, 1136818, '2019-11-21 08:30:07', 1, '60.488861', '21.011862', '59.906750', '19.317694', '\0\0\0\0\0\0\0\0\0\0\0\0\0®Vc  5@\Z¥Kÿ’>N@®Vc  5@òÒMbôM@«\"ÜdTQ3@òÒMbôM@«\"ÜdTQ3@\Z¥Kÿ’>N@®Vc  5@\Z¥Kÿ’>N@', 'country_flag_ax.png', 0, 0),
(16, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', 031, 994, 'AS', 40.143105, 47.576927, 'AZE', 1, 1140809, '2019-11-21 08:30:07', 1, '41.905640', '50.370083', '38.389153', '44.774113', '\0\0\0\0\0\0\0\0\0\0\0\0\0ç6á^/I@‡ùòìóD@ç6á^/I@õøÃÏ1C@H4\"cF@õøÃÏ1C@H4\"cF@‡ùòìóD@ç6á^/I@‡ùòìóD@', 'country_flag_az.png', 1, 0),
(17, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', 070, 387, 'EU', 43.915886, 17.679076, 'BIH', 1, 1142380, '2019-11-21 08:30:07', 1, '45.239193', '19.622223', '42.546112', '15.718945', '\0\0\0\0\0\0\0\0\0\0\0\0\0«Ñ«JŸ3@P7PàžF@«Ñ«JŸ3@ú}ÿæEE@Õ>p/@ú}ÿæEE@Õ>p/@P7PàžF@«Ñ«JŸ3@P7PàžF@', 'country_flag_ba.png', 1, 0),
(18, 'BB', 'Barbados', 'Barbados', 'BRB', 052, 1246, 'NA', 13.193887, -59.543198, 'BAR', 1, 1156359, '2019-11-21 08:30:07', 1, '13.327257', '-59.420376', '13.039844', '-59.648922', '\0\0\0\0\0\0\0\0\0\0\0\0\0ûzáÎµMÀ.ÆÀ:Ž§*@ûzáÎµMÀ7âÉnf*@ÔÓGàÓMÀ7âÉnf*@ÔÓGàÓMÀ.ÆÀ:Ž§*@ûzáÎµMÀ.ÆÀ:Ž§*@', 'country_flag_bb.png', 1, 0),
(19, 'BD', 'Bangladesh', 'People\'s Republic of Bangladesh', 'BGD', 050, 880, 'AS', 23.684994, 90.356331, 'BAN', 1, 1156490, '2019-11-21 08:30:07', 1, '26.631945', '92.673668', '20.743334', '88.028336', '\0\0\0\0\0\0\0\0\0\0\0\0\0\'c`+W@èÞÃ%Ç¡:@\'c`+W@?#K¾4@*SÌAÐV@?#K¾4@*SÌAÐV@èÞÃ%Ç¡:@\'c`+W@èÞÃ%Ç¡:@', 'country_flag_bd.png', 1, 0),
(20, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', 056, 32, 'EU', 50.503887, 4.469936, 'BEL', 1, 1194681, '2019-11-21 08:30:07', 1, '51.505444', '6.403861', '49.493610', '2.546944', '\0\0\0\0\0\0\0\0\0\0\0\0\0ù„ì¼@Îú”c²ÀI@ù„ì¼@A}Ëœ.¿H@ˆò-$`@A}Ëœ.¿H@ˆò-$`@Îú”c²ÀI@ù„ì¼@Îú”c²ÀI@', 'country_flag_be.png', 1, 0),
(21, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', 854, 226, 'AF', 12.238333, -1.561593, 'BUR', 1, 1197899, '2019-11-21 08:30:07', 1, '15.082593', '2.405395', '9.401108', '-5.518916', '\0\0\0\0\0\0\0\0\0\0\0\0\0²×»?>@ÎÁ3¡I*.@²×»?>@ôS^Í\"@|E·^ÀôS^Í\"@|E·^ÀÎÁ3¡I*.@²×»?>@ÎÁ3¡I*.@', 'country_flag_bf.png', 1, 0),
(22, 'BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', 100, 359, 'EU', 42.733883, 25.48583, 'BUL', 1, 1204361, '2019-11-21 08:30:07', 1, '44.217640', '28.612167', '41.242084', '22.371166', '\0\0\0\0\0\0\0\0\0\0\0\0\0À°üù¶œ<@–&¥ ÛF@À°üù¶œ<@CqÇ›üžD@c\'¼_6@CqÇ›üžD@c\'¼_6@–&¥ ÛF@À°üù¶œ<@–&¥ ÛF@', 'country_flag_bg.png', 1, 0),
(23, 'BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', 048, 973, 'AS', 26.0667, 50.5577, 'BRN', 1, 1210427, '2019-11-21 08:30:07', 1, '26.282583', '50.664471', '25.796862', '50.454140', '\0\0\0\0\0\0\0\0\0\0\0\0\0ù¿b\rUI@Óg\\WH:@ù¿b\rUI@×lå%ÿË9@çoB!:I@×lå%ÿË9@çoB!:I@Óg\\WH:@ù¿b\rUI@Óg\\WH:@', 'country_flag_bh.png', 1, 0),
(24, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', 108, 257, 'AF', -3.373056, 29.918886, 'BDI', 1, 1212251, '2019-11-21 08:30:07', 1, '-2.310123', '30.847729', '-4.465713', '28.993061', '\0\0\0\0\0\0\0\0\0\0\0\0\0ìÞŠÄÙ>@6æuÄ!{ÀìÞŠÄÙ>@ÇIaÞãÜÀÝîå>9þ<@ÇIaÞãÜÀÝîå>9þ<@6æuÄ!{ÀìÞŠÄÙ>@6æuÄ!{À', 'country_flag_bi.png', 1, 0),
(25, 'BJ', 'Benin', 'Republic of Benin', 'BEN', 204, 229, 'AF', 9.30769, 2.315834, 'BEN', 1, 1216803, '2019-11-21 08:30:07', 1, '12.418347', '3.851701', '6.225748', '0.774575', '\0\0\0\0\0\0\0\0\0\0\0\0\0“Ã\'HÐ@jÂö“1Ö(@“Ã\'HÐ@µŒÔ{*ç@“©‚QÉè?µŒÔ{*ç@“©‚QÉè?jÂö“1Ö(@“Ã\'HÐ@jÂö“1Ö(@', 'country_flag_bj.png', 1, 0),
(26, 'BL', 'Saint BarthÃ©lemy', 'Saint BarthÃ©lemy', 'BLM', 652, 590, 'NA', 17.9, -62.833333, 'BLM', 1, 2449925, '2019-11-21 08:30:07', 1, '17.928809', '-62.788983', '17.878183', '-62.873912', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÌeýdOÀ\0;7mÆí1@ÌeýdOÀ6çà™Ðà1@}Ê1YÜoOÀ6çà™Ðà1@}Ê1YÜoOÀ\0;7mÆí1@ÌeýdOÀ\0;7mÆí1@', 'country_flag_bl.png', 0, 0),
(27, 'BM', 'Bermuda', 'Bermuda', 'BMU', 060, 1441, 'NA', 32.3078, -64.7505, 'BER', 1, 1216866, '2019-11-21 08:30:07', 1, '32.393833', '-64.651993', '32.246639', '-64.896050', '\0\0\0\0\0\0\0\0\0\0\0\0\0$Ù@º)PÀôŠ§i2@@$Ù@º)PÀ†uãÝ‘@@,eâX9PÀ†uãÝ‘@@,eâX9PÀôŠ§i2@@$Ù@º)PÀôŠ§i2@@', 'country_flag_bm.png', 1, 0),
(28, 'BN', 'Brunei Darussalam', 'Brunei Darussalam', 'BRN', 096, 673, 'AS', 4.535277, 114.727669, 'BRU', 1, 1216878, '2019-11-21 08:30:07', 1, '5.047167', '115.359444', '4.003083', '114.071442', '\0\0\0\0\0\0\0\0\0\0\0\0\0”/h!×\\@>ÍÉ‹L0@”/h!×\\@¼± 0(@åcw’„\\@¼± 0(@åcw’„\\@>ÍÉ‹L0@”/h!×\\@>ÍÉ‹L0@', 'country_flag_bn.png', 1, 0),
(29, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', 068, 591, 'SA', -16.290154, -63.588653, 'BOL', 1, 1218568, '2019-11-21 08:30:07', 1, '-9.680567', '-57.458096', '-22.896133', '-69.640762', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ö6Åã¢ºLÀByGs\\#ÀÖ6Åã¢ºLÀÊÝçøhå6À@¡ž>iQÀÊÝçøhå6À@¡ž>iQÀByGs\\#ÀÖ6Åã¢ºLÀByGs\\#À', 'country_flag_bo.png', 1, 0),
(30, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', 535, 599, 'NA', 12.178361, -68.238534, 'BES', 1, 1228717, '2019-11-21 08:30:07', 1, '12.304535', '-68.192307', '12.017149', '-68.416458', '\0\0\0\0\0\0\0\0\0\0\0\0\0®òÂNQÀ‡ùòì›(@®òÂNQÀvQôÀÇ(@Št?§\ZQÀvQôÀÇ(@Št?§\ZQÀ‡ùòì›(@®òÂNQÀ‡ùòì›(@', 'country_flag_bq.png', 1, 0),
(31, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', 076, 55, 'SA', -14.235004, -51.92528, 'BRA', 1, 1236876, '2019-11-22 03:21:05', 1, '5.264877', '-32.392998', '-33.750706', '-73.985535', '\0\0\0\0\0\0\0\0\0\0\0\0\0[²*ÂM2@À²Ù‘ê;@[²*ÂM2@À›t[\"à@À„dRÀ›t[\"à@À„dRÀ²Ù‘ê;@[²*ÂM2@À²Ù‘ê;@', 'country_flag_br.png', 1, 1),
(32, 'BS', 'Bahamas', 'Commonwealth of the Bahamas', 'BHS', 044, 1242, 'NA', 25.03428, -77.39628, 'BAH', 1, 1266451, '2019-11-21 08:30:07', 1, '26.919243', '-74.423874', '22.852743', '-78.995911', '\0\0\0\0\0\0\0\0\0\0\0\0\0ûçiÀ ›RÀ²^‚Së:@ûçiÀ ›RÀ—ä€]MÚ6@‚®}½¿SÀ—ä€]MÚ6@‚®}½¿SÀ²^‚Së:@ûçiÀ ›RÀ²^‚Së:@', 'country_flag_bs.png', 1, 0),
(33, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', 064, 975, 'AS', 27.514162, 90.433601, 'BHU', 1, 1266616, '2019-11-21 08:30:07', 1, '28.323778', '92.125191', '26.707640', '88.759720', '\0\0\0\0\0\0\0\0\0\0\0\0\0:°!W@*qãR<@:°!W@iW!å\'µ:@‡¢@Ÿ0V@iW!å\'µ:@‡¢@Ÿ0V@*qãR<@:°!W@*qãR<@', 'country_flag_bt.png', 1, 0),
(34, 'BV', 'Bouvet Island (Bouvetoya)', 'Bouvet Island (Bouvetoya)', 'BVT', 074, 47, 'AN', -54.423199, 3.413194, 'BVT', 1, NULL, '2019-11-21 08:30:07', 1, '-54.400322', '3.487976', '-54.462383', '3.335499', '\0\0\0\0\0\0\0\0\0\0\0\0\0ùÜ  ö_ç@FïTÀ=3KÀùÜ ö_ç@úœ»]/;KÀÃ¹†\Z¯\n@úœ»]/;KÀÃ¹†\Z¯\n@FïTÀ=3KÀùÜ ö_ç@FïTÀ=3KÀ', 'country_flag_bv.png', 0, 0),
(35, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', 072, 267, 'AF', -22.328474, 24.684866, 'BOT', 1, 1266848, '2019-11-21 08:30:07', 1, '-17.780813', '29.360781', '-26.907246', '19.999535', '\0\0\0\0\0\0\0\0\0\0\0\0\0§Ä$\\\\=@©J[\\ãÇ1À§Ä$\\\\=@DmFAè:À5˜†áÿ3@DmFAè:À5˜†áÿ3@©J[\\ãÇ1À§Ä$\\\\=@©J[\\ãÇ1À', 'country_flag_bw.png', 1, 0),
(36, 'BY', 'Belarus', 'Republic of Belarus', 'BLR', 112, 375, 'EU', 53.709807, 27.953389, 'BLR', 1, 1272345, '2019-11-21 08:30:07', 1, '56.165806', '32.770805', '51.256416', '23.176889', '\0\0\0\0\0\0\0\0\0\0\0\0\0ñKý¼©b@@„½‰!9L@ñKý¼©b@@èO=Ò I@«ö˜H-7@èO=Ò I@«ö˜H-7@„½‰!9L@ñKý¼©b@@„½‰!9L@', 'country_flag_by.png', 1, 0),
(37, 'BZ', 'Belize', 'Belize', 'BLZ', 084, 501, 'NA', 17.189877, -88.49765, 'BIZ', 1, 1287704, '2019-11-21 08:30:07', 1, '18.496557', '-87.776985', '15.889300', '-89.224815', '\0\0\0\0\0\0\0\0\0\0\0\0\0âKºñUÀ‘™\\2@âKºñUÀeª`TRÇ/@ž)t^cNVÀeª`TRÇ/@ž)t^cNVÀ‘™\\2@âKºñUÀ‘™\\2@', 'country_flag_bz.png', 1, 0),
(38, 'CA', 'Canada', 'Canada', 'CAN', 124, 1, 'NA', 56.130366, -106.346771, 'CAN', 1, 1292743, '2019-11-22 02:53:12', 1, '83.110626', '-52.636291', '41.675980', '-141.000000', '\0\0\0\0\0\0\0\0\0\0\0\0\0œÞÅûqQJÀ–ÇT@œÞÅûqQJÀ`<ƒ†ÖD@\0\0\0\0\0 aÀ`<ƒ†ÖD@\0\0\0\0\0 aÀ–ÇT@œÞÅûqQJÀ–ÇT@', 'country_flag_ca.png', 1, 0),
(39, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', 166, 61, 'AS', -12.17078, 96.841739, 'CCK', 1, 2449893, '2019-11-21 08:30:07', 1, '-12.072459', '96.929489', '-12.208726', '96.816941', '\0\0\0\0\0\0\0\0\0\0\0\0\0y?n¿|;X@š–X%(Ày?n¿|;X@Y¦_\"Þj(À½pçÂH4X@Y¦_\"Þj(À½pçÂH4X@š–X%(Ày?n¿|;X@š–X%(À', 'country_flag_cc.png', 0, 0),
(40, 'CD', 'Democratic Congo', 'Democratic Republic of the Congo', 'COD', 180, 243, 'AF', -0.228021, 15.827659, 'COD', 1, 1304989, '2019-11-21 08:30:07', 1, '5.386098', '31.305912', '-13.455675', '12.204144', '\0\0\0\0\0\0\0\0\0\0\0\0\06t³?PN?@;4,F]‹@6t³?PN?@¨5Í;Né*ÀhY÷…h(@¨5Í;Né*ÀhY÷…h(@;4,F]‹@6t³?PN?@;4,F]‹@', 'country_flag_cd.png', 1, 0),
(41, 'CF', 'Central African Republic', 'Central African Republic', 'CAF', 140, 236, 'AF', 6.611111, 20.939444, 'CAF', 1, 1317377, '2019-11-21 08:30:07', 1, '11.007569', '27.463421', '2.220514', '14.420097', '\0\0\0\0\0\0\0\0\0\0\0\0\0•G7Â¢v;@Fí~à&@•G7Â¢v;@-{ØœÃ@¡K8ô×,@-{ØœÃ@¡K8ô×,@Fí~à&@•G7Â¢v;@Fí~à&@', 'country_flag_cf.png', 0, 0),
(42, 'CG', 'Republic Congo', 'Republic of the Congo', 'COG', 178, 242, 'AF', -0.228021, 15.827659, 'CGO', 1, 1320844, '2019-11-21 08:30:07', 1, '3.703082', '18.649839', '-5.027223', '11.205009', '\0\0\0\0\0\0\0\0\0\0\0\0\0TªDÙ[¦2@@£téŸ\r@TªDÙ[¦2@ÉËšXàÀvÅŒðöh&@ÉËšXàÀvÅŒðöh&@@£téŸ\r@TªDÙ[¦2@@£téŸ\r@', 'country_flag_cg.png', 1, 0),
(43, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', 756, 41, 'EU', 46.818188, 8.227512, 'SUI', 1, 1327535, '2019-11-21 08:30:07', 1, '47.805332', '10.491472', '45.825695', '5.957472', '\0\0\0\0\0\0\0\0\0\0\0\0\0§ÌÍ7¢û$@\r6uçG@§ÌÍ7¢û$@A¼®_°éF@ÅW;ŠsÔ@A¼®_°éF@ÅW;ŠsÔ@\r6uçG@§ÌÍ7¢û$@\r6uçG@', 'country_flag_ch.png', 1, 0),
(44, 'CI', 'Cote d\'Ivoire', 'Republic of Cote d\'Ivoire', 'CIV', 384, 225, 'AF', 7.539989, -5.54708, 'CIV', 1, 1330744, '2019-11-21 08:30:07', 1, '10.736642', '-2.494897', '4.357067', '-8.599302', '\0\0\0\0\0\0\0\0\0\0\0\0\0¬ïŽŒõÀV¸å#)y%@¬ïŽŒõÀ_ì½ø¢m@ÓÚ4¶×2!À_ì½ø¢m@ÓÚ4¶×2!ÀV¸å#)y%@¬ïŽŒõÀV¸å#)y%@', 'country_flag_ci.png', 1, 0),
(45, 'CK', 'Cook Islands', 'Cook Islands', 'COK', 184, 682, 'OC', -21.236736, -159.777671, 'COK', 1, NULL, '2019-11-21 08:30:07', 1, '-10.023114', '-157.312134', '-21.944164', '-161.093658', '\0\0\0\0\0\0\0\0\0\0\0\0\0?q\0ý©cÀÔ($™Õ$À?q\0ý©cÀ€^»´ñ5ÀHà?ÿ\"dÀ€^»´ñ5ÀHà?ÿ\"dÀÔ($™Õ$À?q\0ý©cÀÔ($™Õ$À', 'country_flag_ck.png', 1, 0),
(46, 'CL', 'Chile', 'Republic of Chile', 'CHL', 152, 56, 'SA', -35.675147, -71.542969, 'CHI', 1, 1338946, '2019-11-21 08:30:07', 1, '-17.507553', '-66.417557', '-55.925623', '-80.785851', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÑÍþ@¹šPÀ˜üOþî1ÀÑÍþ@¹šPÀt¶€ÐzöKÀÙ!þaK2TÀt¶€ÐzöKÀÙ!þaK2TÀ˜üOþî1ÀÑÍþ@¹šPÀ˜üOþî1À', 'country_flag_cl.png', 1, 0),
(47, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', 120, 237, 'AF', 7.369722, 12.354722, 'CMR', 1, 1342472, '2019-11-21 08:30:07', 1, '13.078056', '16.192116', '1.652548', '8.494763', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ð  ¡ƒ.10@^ƒ¾ôö\'*@Ð  ¡ƒ.10@’ñ+Öpú?­Šp“Qý @’ñ+Öpú?­Šp“Qý @^ƒ¾ôö\'*@Ð  ¡ƒ.10@^ƒ¾ôö\'*@', 'country_flag_cm.png', 1, 0),
(48, 'CN', 'China', 'People\'s Republic of China', 'CHN', 156, 86, 'AS', 35.86166, 104.195397, 'CHN', 1, 1384846, '2019-11-21 08:30:07', 1, '53.560860', '134.773911', '15.775416', '73.557693', '\0\0\0\0\0\0\0\0\0\0\0\0\0x`\0áÃØ`@9Ñ®BÊÇJ@x`\0áÃØ`@—qS/@R\rû=±cR@—qS/@R\rû=±cR@9Ñ®BÊÇJ@x`\0áÃØ`@9Ñ®BÊÇJ@', 'country_flag_cn.png', 1, 0),
(49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', 170, 57, 'SA', 4.570868, -74.297333, 'COL', 1, 1570463, '2019-11-21 08:30:07', 1, '13.380502', '-66.869835', '-4.225869', '-81.728111', '\0\0\0\0\0\0\0\0\0\0\0\0\0¢zk`«·PÀ |(ÑÂ*@¢zk`«·PÀ¸\04JçÀç6á^™nTÀ¸\04JçÀç6á^™nTÀ |(ÑÂ*@¢zk`«·PÀ |(ÑÂ*@', 'country_flag_co.png', 1, 0),
(50, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', 188, 506, 'NA', 9.748917, -83.753428, 'CRC', 1, 1582289, '2019-11-21 08:30:07', 1, '11.216819', '-82.555992', '8.032975', '-85.950623', '\0\0\0\0\0\0\0\0\0\0\0\0\0œ5x_•£TÀ‡Mdæo&@œ5x_•£TÀÔ+eâ @ÓôÙ×|UÀÔ+eâ @ÓôÙ×|UÀ‡Mdæo&@œ5x_•£TÀ‡Mdæo&@', 'country_flag_cr.png', 1, 0),
(51, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', 192, 53, 'NA', 21.521757, -77.781167, 'CUB', 1, 1587640, '2019-11-21 08:30:07', 1, '23.226042', '-74.131775', '19.828083', '-84.957428', '\0\0\0\0\0\0\0\0\0\0\0\0\0ŒÛh\0oˆRÀ¾…uãÝ97@ŒÛh\0oˆRÀ¢_[?ýÓ3@”€F=UÀ¢_[?ýÓ3@”€F=UÀ¾…uãÝ97@ŒÛh\0oˆRÀ¾…uãÝ97@', 'country_flag_cu.png', 1, 0),
(52, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', 132, 238, 'AF', 15.121729, -23.605082, 'CPV', 1, 1591443, '2019-11-21 08:30:07', 1, '17.197178', '-22.669443', '14.808022', '-25.358747', '\0\0\0\0\0\0\0\0\0\0\0\0\0@‰Ï`«6Àž}åAz21@@‰Ï`«6À£å@µ-@Â‰è×Ö[9À£å@µ-@Â‰è×Ö[9Àž}åAz21@@‰Ï`«6Àž}åAz21@', 'country_flag_cv.png', 1, 0),
(53, 'CW', 'CuraÃ§ao', 'CuraÃ§ao', 'CUW', 531, 599, 'NA', 12.16957, -68.99002, 'CUW', 1, 2449908, '2019-11-21 08:30:07', 1, '12.385672', '-68.733948', '12.032745', '-69.157204', '\0\0\0\0\0\0\0\0\0\0\0\0\0¼=ù.QÀ@÷åÌvÅ(@¼=ù.QÀ\r7àóÃ(@9³]¡JQÀ\r7àóÃ(@9³]¡JQÀ@÷åÌvÅ(@¼=ù.QÀ@÷åÌvÅ(@', 'country_flag_cw.png', 1, 0),
(54, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', 162, 61, 'AS', -10.447525, 105.690449, 'CXR', 1, NULL, '2019-11-21 08:30:07', 1, '-10.412356', '105.712597', '-10.570483', '105.533277', '\0\0\0\0\0\0\0\0\0\0\0\0\0“Žr0›mZ@gž\\S Ó$À“Žr0›mZ@ÙY$%À`­Ú5!bZ@ÙY$%À`­Ú5!bZ@gž\\S Ó$À“Žr0›mZ@gž\\S Ó$À', 'country_flag_cx.png', 0, 0),
(55, 'CY', 'Cyprus', 'Republic of Cyprus', 'CYP', 196, 357, 'AS', 35.126413, 33.429859, 'CYP', 1, 1591884, '2019-11-21 08:30:07', 1, '35.701527', '34.597916', '34.633285', '32.273083', '\0\0\0\0\0\0\0\0\0\0\0\0\0¨àð‚ˆLA@h!£ËÙA@¨àð‚ˆLA@ž{QA@ú=bô\"@@ž{QA@ú=bô\"@@h!£ËÙA@¨àð‚ˆLA@h!£ËÙA@', 'country_flag_cy.png', 1, 0),
(56, 'CZ', 'Czech Republic', 'Czech Republic', 'CZE', 203, 420, 'EU', 49.817492, 15.472962, 'CZE', 1, 1596791, '2019-11-21 08:30:07', 1, '51.058887', '18.860111', '48.542915', '12.096194', '\0\0\0\0\0\0\0\0\0\0\0\0\0î<0Ü2@l”õ›‰‡I@î<0Ü2@Á=~EH@’$W@1(@Á=~EH@’$W@1(@l”õ›‰‡I@î<0Ü2@l”õ›‰‡I@', 'country_flag_cz.png', 1, 0),
(57, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', 276, 49, 'EU', 51.165691, 10.451526, 'GER', 1, 1663024, '2019-11-21 08:30:07', 1, '55.055637', '15.039889', '47.275776', '5.865639', '\0\0\0\0\0\0\0\0\0\0\0\0\0·ð¼Tl.@I¹û‡K@·ð¼Tl.@Ç‚Â L£G@Ž‘ìjv@Ç‚Â L£G@Ž‘ìjv@I¹û‡K@·ð¼Tl.@I¹û‡K@', 'country_flag_de.png', 1, 0),
(58, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', 262, 253, 'AF', 11.825138, 42.590275, 'DJI', 1, 1672891, '2019-11-21 08:30:07', 1, '12.706833', '43.416973', '10.909917', '41.773472', '\0\0\0\0\0\0\0\0\0\0\0\0\0R(__µE@–wÕæi)@R(__µE@&\Z¤àÑ%@”/h!ãD@&\Z¤àÑ%@”/h!ãD@–wÕæi)@R(__µE@–wÕæi)@', 'country_flag_dj.png', 1, 0),
(59, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', 208, 45, 'EU', 56.26392, 9.501785, 'DEN', 1, 1676414, '2019-11-21 08:30:07', 1, '57.748417', '15.158834', '54.562389', '8.075611', '\0\0\0\0\0\0\0\0\0\0\0\0\0Yü¦°RQ.@žbÕ ÌßL@Yü¦°RQ.@©PÝ\\üGK@‹o(|¶& @©PÝ\\üGK@‹o(|¶& @žbÕ ÌßL@Yü¦°RQ.@žbÕ ÌßL@', 'country_flag_dk.png', 1, 0),
(60, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', 212, 1767, 'NA', 15.414999, -61.370976, 'DMA', 1, 1679336, '2019-11-21 08:30:07', 1, '15.631809', '-61.244152', '15.201690', '-61.484108', '\0\0\0\0\0\0\0\0\0\0\0\0\0b k_@ŸNÀ£ x|C/@b k_@ŸNÀHÜcéCg.@±Ý=@÷½NÀHÜcéCg.@±Ý=@÷½NÀ£ x|C/@b k_@ŸNÀ£ x|C/@', 'country_flag_dm.png', 1, 0),
(61, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', 214, 1809, 'NA', 15.414999, -61.370976, 'DOM', 1, 1679711, '2019-11-21 08:30:07', 1, '19.929859', '-68.320000', '17.543159', '-72.003487', '\0\0\0\0\0\0\0\0\0\0\0\0\0®GázQÀ*äJ=î3@®GázQÀ/‡Ýw‹1@„½‰!9\0RÀ/‡Ýw‹1@„½‰!9\0RÀ*äJ=î3@®GázQÀ*äJ=î3@', 'country_flag_do.png', 1, 0),
(62, 'DZ', 'Algeria', 'People\'s Democratic Republic of Algeria', 'DZA', 012, 213, 'AF', 28.033886, 1.659626, 'ALG', 1, 1692127, '2019-11-22 02:53:12', 1, '37.093723', '11.979548', '18.960028', '-8.673868', '\0\0\0\0\0\0\0\0\0\0\0\0\0s¹ÁP‡õ\'@ñÿ‹B@s¹ÁP‡õ\'@Š>eÄõ2@Q¤û9Y!ÀŠ>eÄõ2@Q¤û9Y!Àñÿ‹B@s¹ÁP‡õ\'@ñÿ‹B@', 'country_flag_dz.png', 1, 0),
(63, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', 218, 593, 'SA', -1.831239, -78.183406, 'ECU', 1, 1696459, '2019-11-21 08:30:07', 1, '1.439020', '-75.184586', '-4.998823', '-81.078598', '\0\0\0\0\0\0\0\0\0\0\0\0\0*SÌAÐËRÀƒ£äÕ9÷?*SÌAÐËRÀnøÝtËþÀüáç¿ETÀnøÝtËþÀüáç¿ETÀƒ£äÕ9÷?*SÌAÐËRÀƒ£äÕ9÷?', 'country_flag_ec.png', 1, 0),
(64, 'EE', 'Estonia', 'Republic of Estonia', 'EST', 233, 372, 'EU', -1.831239, -78.183406, 'EST', 1, 1702445, '2019-11-21 08:30:07', 1, '59.676224', '28.209972', '57.516193', '21.837584', '\0\0\0\0\0\0\0\0\0\0\0\0\0b™¹À5<@™b‚ŽÖM@b™¹À5<@J¶ºœÂL@\"§¯çkÖ5@J¶ºœÂL@\"§¯çkÖ5@™b‚ŽÖM@b™¹À5<@™b‚ŽÖM@', 'country_flag_ee.png', 1, 0),
(65, 'EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', 818, 20, 'AF', 26.820553, 30.802498, 'EGY', 1, 1710826, '2019-11-21 08:30:07', 1, '31.667334', '36.898331', '21.725389', '24.698111', '\0\0\0\0\0\0\0\0\0\0\0\0\0Òýœ‚ürB@E‚©fÖª?@Òýœ‚ürB@Îàï³¹5@Uú  g·²8@Îàï³¹5@Uú g·²8@E‚©fÖª?@Òýœ‚ürB@E‚©fÖª?@', 'country_flag_eg.png', 1, 0),
(66, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', 732, 212, 'AF', 24.215527, -12.885834, 'ESH', 1, NULL, '2019-11-21 08:30:07', 1, '27.669674', '-8.670276', '20.774158', '-17.103182', '\0\0\0\0\0\0\0\0\0\0\0\0\0_–vj.W!ÀDûXÁo«;@_–vj.W!ÀÐïû7/Æ4@/‰³\"j\Z1ÀÐïû7/Æ4@/‰³\"j\Z1ÀDûXÁo«;@_–vj.W!ÀDûXÁo«;@', 'country_flag_eh.png', 1, 0),
(67, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', 232, 291, 'AF', 15.179384, 39.782334, 'ERI', 1, 1715471, '2019-11-21 08:30:07', 1, '18.003084', '43.134640', '12.359555', '36.438778', '\0\0\0\0\0\0\0\0\0\0\0\0\0á].â;‘E@$ïÊ\02@á].â;‘E@\0:Ì—¸(@&\Z¤à)8B@\0:Ì—¸(@&\Z¤à)8B@$ïÊ\02@á].â;‘E@$ïÊ\02@', 'country_flag_er.png', 1, 0),
(68, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', 724, 34, 'EU', 40.463667, -3.74922, 'ESP', 1, 1740558, '2019-11-21 08:30:07', 1, '43.791357', '4.327785', '36.000104', '-9.301516', '\0\0\0\0\0\0\0\0\0\0\0\0\0:züÞ¦O@÷:©/KåE@:züÞ¦O@¥Ljh\0B@qN`š\"À¥Ljh\0B@qN`š\"À÷:©/KåE@:züÞ¦O@÷:©/KåE@', 'country_flag_es.png', 1, 0),
(69, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', 231, 251, 'AF', 9.145, 40.489673, 'ETH', 1, 1751667, '2019-11-21 08:30:07', 1, '14.893750', '47.986179', '3.402422', '32.999939', '\0\0\0\0\0\0\0\0\0\0\0\0\0A€;þG@š™™™™É-@A€;þG@ý†‰)8@ZK\0þ@@ý†‰)8@ZK\0þ@@š™™™™É-@A€;þG@š™™™™É-@', 'country_flag_et.png', 1, 0),
(70, 'FI', 'Finland', 'Republic of Finland', 'FIN', 246, 358, 'EU', 61.92411, 25.748151, 'FIN', 1, 1760652, '2019-11-21 08:30:07', 1, '70.096054', '31.580944', '59.808777', '20.556944', '\0\0\0\0\0\0\0\0\0\0\0\0\0³Îø¾¸”?@™)­¿%†Q@³Îø¾¸”?@å`6†çM@´Éá“Ž4@å`6†çM@´Éá“Ž4@™)­¿%†Q@³Îø¾¸”?@™)­¿%†Q@', 'country_flag_fi.png', 1, 0),
(71, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', 242, 679, 'OC', -17.713371, 178.065032, 'FIJ', 1, 1763713, '2019-11-21 08:30:07', 1, '-12.480111', '-178.424438', '-20.675970', '177.129334', '\0\0\0\0\0\0\0\0\0\0\0\0\0À%\0ÿ”MfÀTææÑõ(ÀÀ%\0ÿ”MfÀ¾³^­4ÀYˆ#$f@¾³^­4ÀYˆ#$f@TææÑõ(ÀÀ%\0ÿ”MfÀTææÑõ(À', 'country_flag_fj.png', 1, 0),
(72, 'FK', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, 'SA', -51.796253, -59.523613, 'FLK', 1, 2449911, '2019-11-21 08:30:07', 1, '-51.240650', '-57.712486', '-52.360512', '-61.345192', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÎmÂ½2ÛLÀ&äƒžÍžIÀÎmÂ½2ÛLÀdèØA%.JÀ k_@/¬NÀdèØA%.JÀ k_@/¬NÀ&äƒžÍžIÀÎmÂ½2ÛLÀ&äƒžÍžIÀ', 'country_flag_fk.png', 0, 0),
(73, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', 583, 691, 'OC', 6.887457, 158.215072, 'FSM', 1, NULL, '2019-11-21 08:30:07', 1, '10.089040', '163.037170', '1.026290', '137.336480', '\0\0\0\0\0\0\0\0\0\0\0\0\0ŽÌ#0ad@Z ¦–-$@ŽÌ#0ad@²c#¯kð?Bx´qÄ*a@²c#¯kð?Bx´qÄ*a@Z ¦–-$@ŽÌ#0ad@Z ¦–-$@', 'country_flag_fm.png', 0, 0),
(74, 'FO', 'Faroe Islands', 'Faroe Islands', 'FRO', 234, 298, 'EU', 61.892635, -6.911806, 'FRO', 1, 1764944, '2019-11-21 08:30:07', 1, '62.400749', '-6.399583', '61.394943', '-7.458000', '\0\0\0\0\0\0\0\0\0\0\0\0\0x&4I,™ÀÍsD¾K3O@x&4I,™À^d~²N@¢E¶óýÔÀ^d~²N@¢E¶óýÔÀÍsD¾K3O@x&4I,™ÀÍsD¾K3O@', 'country_flag_fo.png', 1, 0),
(75, 'FR', 'France', 'French Republic', 'FRA', 250, 33, 'EU', 46.227638, 2.213749, 'FRA', 10, 1818316, '2019-11-22 02:53:41', 1, '51.092804', '9.561556', '41.371582', '-5.142222', '\0\0\0\0\0\0\0\0\0\0\0\0\0Â¾D„#@x`\0á‹I@Â¾D„#@!ä¼ÿ¯D@<Û¤¢‘À!ä¼ÿ¯D@<Û¤¢‘Àx`\0á‹I@Â¾D„#@x`\0á‹I@', 'country_flag_fr.png', 1, 1),
(76, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', 266, 241, 'AF', -0.803689, 11.609444, 'GAB', 1, 1823250, '2019-11-21 08:30:07', 1, '2.322612', '14.502347', '-3.978806', '8.695471', '\0\0\0\0\0\0\0\0\0\0\0\0\0È|@ 3-@‘`ª™µ”@È|@ 3-@Sy=˜ÔÀ¢`Æd!@Sy=˜ÔÀ¢`Æd!@‘`ª™µ”@È|@ 3-@‘`ª™µ”@', 'country_flag_ga.png', 1, 0),
(77, 'GB', 'United Kingdom', 'United Kingdom of Great Britain & Northern Ireland', 'GBR', 826, 44, 'EU', 54.127247, -6.508945, 'GBR', 1, 1829266, '2019-11-21 08:30:07', 1, '59.360249', '1.759000', '49.906193', '-8.623555', '\0\0\0\0\0\0\0\0\0\0\0\0\0¾Ÿ\Z/Ý$ü?Wµ¤£®M@¾Ÿ\Z/Ý$ü?œnÙ!þóH@ºƒØ™B?!ÀœnÙ!þóH@ºƒØ™B?!ÀWµ¤£®M@¾Ÿ\Z/Ý$ü?Wµ¤£®M@', 'country_flag_gb.png', 1, 0),
(78, 'GD', 'Grenada', 'Grenada', 'GRD', 308, 1473, 'NA', 12.1165, -61.679, 'GRN', 1, NULL, '2019-11-21 08:30:07', 1, '12.318284', '-61.576770', '11.986893', '-61.802344', '\0\0\0\0\0\0\0\0\0\0\0\0\0/¨o™ÓÉNÀ®Õö¢(@/¨o™ÓÉNÀ{M\nJù\'@\'L5³æNÀ{M\nJù\'@\'L5³æNÀ®Õö¢(@/¨o™ÓÉNÀ®Õö¢(@', 'country_flag_gd.png', 1, 0),
(79, 'GE', 'Georgia', 'Georgia', 'GEO', 268, 995, 'AS', 42.315407, 43.356892, 'GEO', 1, 1843196, '2019-11-21 08:30:07', 1, '43.586498', '46.725971', '41.053196', '40.010139', '\0\0\0\0\0\0\0\0\0\0\0\0\0l#žì\\G@¯•Ð]ËE@l#žì\\G@–#d Ï†D@µ<LD@–#d Ï†D@µ<LD@¯•Ð]ËE@l#žì\\G@¯•Ð]ËE@', 'country_flag_ge.png', 1, 0),
(80, 'GF', 'French Guiana', 'French Guiana', 'GUF', 254, 594, 'SA', 3.933889, -53.125782, 'GUF', 1, 1844961, '2019-11-21 08:30:07', 1, '5.776496', '-51.613949', '2.127094', '-54.542511', '\0\0\0\0\0\0\0\0\0\0\0\0\0¹4~á•ÎIÀ6æuÄ!@¹4~á•ÎIÀ$ìÛI@1\\\0qEKÀ$ìÛI@1\\\0qEKÀ6æuÄ!@¹4~á•ÎIÀ6æuÄ!@', 'country_flag_gf.png', 1, 0),
(81, 'GG', 'Guernsey', 'Bailiwick of Guernsey', 'GGY', 831, 441481, 'EU', 49.465691, -2.585278, 'GGY', 1, NULL, '2019-11-21 08:30:07', 1, '49.731728', '-2.157715', '49.407642', '-2.673195', '\0\0\0\0\0\0\0\0\0\0\0\0\0‰µø\0CÀ£ÈZC©ÝH@‰µø\0CÀï<ñœ-´H@Q¥f´bÀï<ñœ-´H@Q¥f´bÀ£ÈZC©ÝH@‰µø\0CÀ£ÈZC©ÝH@', 'country_flag_gg.png', 0, 0),
(82, 'GH', 'Ghana', 'Republic of Ghana', 'GHA', 288, 233, 'AF', 7.946527, -1.023194, 'GHA', 1, 1845816, '2019-11-21 08:30:07', 1, '11.173301', '1.191781', '4.736723', '-3.255420', '\0\0\0\0\0\0\0\0\0\0\0\0\0%è/ôˆó?kºžèºX&@%è/ôˆó?Øœƒgò@^ô¤\nÀØœƒgò@^ô¤\nÀkºžèºX&@%è/ôˆó?kºžèºX&@', 'country_flag_gh.png', 1, 0),
(83, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', 292, 350, 'EU', 36.137741, -5.345374, 'GIB', 1, 2449919, '2019-11-21 08:30:07', 1, '36.155439', '-5.338285', '36.109031', '-5.366261', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÑèbgZÀðÂÖlåB@ÑèbgZÀ Qºô\rB@v3£\rwÀ Qºô\rB@v3£\rwÀðÂÖlåB@ÑèbgZÀðÂÖlåB@', 'country_flag_gi.png', 1, 0),
(84, 'GL', 'Greenland', 'Greenland', 'GRL', 304, 299, 'NA', 71.706936, -42.604303, 'GRL', 1, 1846100, '2019-11-21 08:30:07', 1, '83.627357', '-11.312319', '59.777401', '-73.042030', '\0\0\0\0\0\0\0\0\0\0\0\0\0¿Ö¥FèŸ&À«zù&èT@¿Ö¥FèŸ&ÀXp?àãM@ÛÜ˜ž°BRÀXp?àãM@ÛÜ˜ž°BRÀ«zù&èT@¿Ö¥FèŸ&À«zù&èT@', 'country_flag_gl.png', 1, 0),
(85, 'GM', 'Gambia', 'Republic of the Gambia', 'GMB', 270, 220, 'AF', 13.443182, -15.310139, 'GAM', 1, NULL, '2019-11-21 08:30:07', 1, '13.826571', '-13.797793', '13.064252', '-16.825079', '\0\0\0\0\0\0\0\0\0\0\0\0\0™ô÷Rx˜+Àß¤iP4§+@™ô÷Rx˜+Àºg]£å *@Ë™`8Ó0Àºg]£å *@Ë™`8Ó0Àß¤iP4§+@™ô÷Rx˜+Àß¤iP4§+@', 'country_flag_gm.png', 1, 0),
(86, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', 324, 224, 'AF', 9.945587, -9.696645, 'GUI', 1, 1853021, '2019-11-21 08:30:07', 1, '12.676220', '-7.641071', '7.193553', '-14.926619', '\0\0\0\0\0\0\0\0\0\0\0\0\0ô§êtÀ`Í‚9Z)@ô§êtÀ¶+ôÁ2Æ@W´9ÎmÚ-À¶+ôÁ2Æ@W´9ÎmÚ-À`Í‚9Z)@ô§êtÀ`Í‚9Z)@', 'country_flag_gn.png', 1, 0),
(87, 'GP', 'Guadeloupe', 'Guadeloupe', 'GLP', 312, 590, 'NA', 16.265, -61.551, 'GLP', 1, 1854209, '2019-11-21 08:30:07', 1, '16.516848', '-61.000000', '15.867565', '-61.544765', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€NÀÅ\0‰&P„0@\0\0\0\0\0€NÀùNÌz1¼/@²€  ÜºÅNÀùNÌz1¼/@²€ ÜºÅNÀÅ\0‰&P„0@\0\0\0\0\0€NÀÅ\0‰&P„0@', 'country_flag_gp.png', 1, 0),
(88, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', 226, 240, 'AF', 1.650801, 10.267895, 'GEQ', 1, 1854226, '2019-11-21 08:30:07', 1, '2.346989', '11.335724', '0.920860', '9.346865', '\0\0\0\0\0\0\0\0\0\0\0\0\0ñö ä«&@ï’8+¢Æ@ñö ä«&@Ö9d¯wí?AJ˜±\"@Ö9d¯wí?AJ˜±\"@ï’8+¢Æ@ñö ä«&@ï’8+¢Æ@', 'country_flag_gq.png', 1, 0),
(89, 'GR', 'Greece', 'Hellenic Republic Greece', 'GRC', 300, 30, 'EU', 39.074208, 21.824312, 'GRE', 1, 1861327, '2019-11-21 08:30:07', 1, '41.748500', '28.247083', '34.802066', '19.373604', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÇeÜÔ@?<@+‡ÙÎßD@ÇeÜÔ@?<@åCªfA@ §ƒ¤_3@åCªfA@ §ƒ¤_3@+‡ÙÎßD@ÇeÜÔ@?<@+‡ÙÎßD@', 'country_flag_gr.png', 1, 0),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', 239, 500, 'AN', -54.429579, -36.587909, 'SGS', 1, NULL, '2019-11-21 08:30:07', 1, '-53.970467', '-26.229326', '-59.479259', '-38.021175', '\0\0\0\0\0\0\0\0\0\0\0\0\0\\Öµ::Àrl=C8üJÀ\\Öµ::À&¨á[X½MÀ?ÆÜµCÀ&¨á[X½MÀ?ÆÜµCÀrl=C8üJÀ\\Öµ::Àrl=C8üJÀ', 'country_flag_gs.png', 0, 0),
(91, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', 320, 502, 'NA', 15.783471, -90.230759, 'GUA', 1, 1870672, '2019-11-21 08:30:07', 1, '17.815220', '-88.223198', '13.737302', '-92.236290', '\0\0\0\0\0\0\0\0\0\0\0\0\0¢CàHVÀB²Ð1@¢CàHVÀ\0Ò¥y+@Ì—`WÀ\0Ò¥y+@Ì—`WÀB²Ð1@¢CàHVÀB²Ð1@', 'country_flag_gt.png', 1, 0),
(92, 'GU', 'Guam', 'Guam', 'GUM', 316, 1671, 'OC', 13.444304, 144.793731, 'GUM', 1, 1871917, '2019-11-21 08:30:07', 1, '13.654402', '144.956894', '13.233760', '144.618060', '\0\0\0\0\0\0\0\0\0\0\0\0\0£w*àžb@½ßhÇ\rO+@£w*àžb@Ö9d¯w*@èÞÃ%Çb@Ö9d¯w*@èÞÃ%Çb@½ßhÇ\rO+@£w*àžb@½ßhÇ\rO+@', 'country_flag_gu.png', 1, 0),
(93, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', 624, 245, 'AF', 11.803749, -15.180413, 'GBS', 1, 1872462, '2019-11-21 08:30:07', 1, '12.680789', '-13.636522', '10.924265', '-16.717535', '\0\0\0\0\0\0\0\0\0\0\0\0\0x^*6æE+Àþó4`\\)@x^*6æE+ÀÅ¬C9Ù%@A¼®_°·0ÀÅ¬C9Ù%@A¼®_°·0Àþó4`\\)@x^*6æE+Àþó4`\\)@', 'country_flag_gw.png', 1, 0),
(94, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', 328, 592, 'SA', 4.860416, -58.93018, 'GUY', 1, 1872904, '2019-11-21 08:30:07', 1, '8.557567', '-56.480251', '1.175080', '-61.384762', '\0\0\0\0\0\0\0\0\0\0\0\0\0‡oaÝx=LÀ]¨üky!@‡oaÝx=LÀZð¢¯ Íò?-_—á?±NÀZð¢¯ Íò?-_—á?±NÀ]¨üky!@‡oaÝx=LÀ]¨üky!@', 'country_flag_gy.png', 1, 0),
(95, 'HK', 'Hong Kong', 'Hong Kong Special Administrative Region of China', 'HKG', 344, 852, 'AS', 22.396428, 114.109497, 'HKG', 5, NULL, '2019-11-21 08:30:07', 1, '22.559778', '114.434753', '22.153250', '113.837753', '\0\0\0\0\0\0\0\0\0\0\0\0\0 5?þÒ›\\@2kœM6@ 5?þÒ›\\@¬Zd;\'6@HÂ¾u\\@¬Zd;\'6@HÂ¾u\\@2kœM6@ 5?þÒ›\\@2kœM6@', 'country_flag_hk.png', 0, 0),
(96, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', 334, 0, 'AN', -53.08181, 73.504158, 'HMD', 1, NULL, '2019-11-21 08:30:07', 1, '-52.909416', '73.859146', '-53.192001', '72.596535', '\0\0\0\0\0\0\0\0\0\0\0\0\0O?üvR@Å:U¾gtJÀO?üvR@Oæ}“˜JÀ×ú\"¡-&R@Oæ}“˜JÀ×ú\"¡-&R@Å:U¾gtJÀO?üvR@Å:U¾gtJÀ', 'country_flag_hm.png', 0, 0),
(97, 'HN', 'Honduras', 'Republic of Honduras', 'HND', 340, 504, 'NA', 15.199999, -86.241905, 'HON', 4, 1873576, '2019-11-21 08:30:07', 1, '16.510256', '-83.155403', '12.982411', '-89.350792', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ò¬lòÉTÀx– # ‚0@Ò¬lòÉTÀv“þö)@³ìI`sVVÀv“þö)@³ìI`sVVÀx– # ‚0@Ò¬lòÉTÀx– # ‚0@', 'country_flag_hn.png', 1, 0),
(98, 'HR', 'Croatia', 'Republic of Croatia', 'HRV', 191, 385, 'EU', 45.753343, 15.989126, 'CRO', 1, 1882326, '2019-11-21 08:30:07', 1, '46.538750', '19.427389', '42.435890', '13.493222', '\0\0\0\0\0\0\0\0\0\0\0\0\0«‘]im3@)\\ÂõDG@«‘]im3@¤SW>Ë7E@ÞU˜‡ü*@¤SW>Ë7E@ÞU˜‡ü*@)\\ÂõDG@«‘]im3@)\\ÂõDG@', 'country_flag_hr.png', 1, 0),
(99, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', 332, 509, 'NA', 18.971187, -72.285215, 'HAI', 1, 1892508, '2019-11-21 08:30:07', 1, '20.087820', '-71.613358', '18.021032', '-74.478584', '\0\0\0\0\0\0\0\0\0\0\0\0\0\\¯éAAçQÀJï_{4@\\¯éAAçQÀc+hZb2@ãÉ¡žRÀc+hZb2@ãÉ¡žRÀJï_{4@\\¯éAAçQÀJï_{4@', 'country_flag_ht.png', 1, 0),
(100, 'HU', 'Hungary', 'Republic of Hungary', 'HUN', 348, 36, 'EU', 47.162494, 19.503304, 'HUN', 1, 1909113, '2019-11-21 08:30:07', 1, '48.585667', '22.906000', '45.743610', '16.111889', '\0\0\0\0\0\0\0\0\0\0\0\0\0-²ïç6@X¬á\"÷JH@-²ïç6@A}Ëœ.ßF@;ÈëÁ¤0@A}Ëœ.ßF@;ÈëÁ¤0@X¬á\"÷JH@-²ïç6@X¬á\"÷JH@', 'country_flag_hu.png', 1, 0),
(101, 'ID', 'Indonesia', 'Republic of Indonesia', 'IDN', 360, 62, 'AS', -0.789275, 113.921327, 'INA', 1, 1917563, '2019-11-21 08:30:07', 1, '5.904417', '141.021805', '-10.941861', '95.009331', '\0\0\0\0\0\0\0\0\0\0\0\0\0u<f ² a@%És}ž@u<f ² a@vÁàš;â%À²õá˜ÀW@vÁàš;â%À²õá˜ÀW@%És}ž@u<f ² a@%És}ž@', 'country_flag_id.png', 1, 0),
(102, 'IE', 'Ireland', 'Ireland', 'IRL', 372, 353, 'EU', 53.41291, -8.24389, 'IRL', 1, 2081665, '2019-11-21 08:30:07', 1, '55.387917', '-6.002389', '51.451584', '-10.478556', '\0\0\0\0\0\0\0\0\0\0\0\0\0{CrÀþG¦C§±K@{CrÀÌ²\'Í¹I@ñ›ÂJõ$ÀÌ²\'Í¹I@ñ›ÂJõ$ÀþG¦C§±K@{CrÀþG¦C§±K@', 'country_flag_ie.png', 1, 0),
(103, 'IL', 'Israel', 'State of Israel', 'ISR', 376, 972, 'AS', 31.046051, 34.851612, 'ISR', 1, NULL, '2019-11-21 08:30:07', 1, '33.340137', '35.876804', '29.496639', '34.270279', '\0\0\0\0\0\0\0\0\0\0\0\0\0A€;ðA@l”õ›‰«@@A€;ðA@ëÆ»#=@Öå”€˜\"A@ëÆ»#=@Öå”€˜\"A@l”õ›‰«@@A€;ðA@l”õ›‰«@@', 'country_flag_il.png', 1, 0),
(104, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', 833, 441624, 'EU', 54.236107, -4.548056, 'IMN', 1, 2088027, '2019-11-21 08:30:07', 1, '54.419724', '-4.311500', '54.055916', '-4.798722', '\0\0\0\0\0\0\0\0\0\0\0\0\0åÐ\"Ûù>ÀT¬\Z„¹5K@åÐ\"Ûù>À]©gA(K@b.ä1À]©gA(K@b.ä1ÀT¬\Z„¹5K@åÐ\"Ûù>ÀT¬\Z„¹5K@', 'country_flag_im.png', 0, 0),
(105, 'IN', 'India', 'Republic of India', 'IND', 356, 91, 'AS', 20.593684, 78.96288, 'IND', 1, 2094206, '2019-11-21 08:30:07', 1, '35.504223', '97.403305', '6.747139', '68.186691', '\0\0\0\0\0\0\0\0\0\0\0\0\0\rTÆ¿ÏYX@\rraŠÀA@\rTÆ¿ÏYX@»CŠý\Z@HÝÎ¾òQ@»CŠý\Z@HÝÎ¾òQ@\rraŠÀA@\rTÆ¿ÏYX@\rraŠÀA@', 'country_flag_in.png', 1, 0),
(106, 'IO', 'British Indian Ocean Territory (Chagos Archipelago)', 'British Indian Ocean Territory (Chagos Archipelago)', 'IOT', 086, 246, 'AS', -6, 72, 'IOT', 1, NULL, '2019-11-21 08:30:07', 1, '-5.268333', '72.493164', '-7.438028', '71.259972', '\0\0\0\0\0\0\0\0\0\0\0\0\0!ä¼ÿR@ÀÍâÅÀ!ä¼ÿR@ÝíziŠÀÀx™a£ÐQ@ÝíziŠÀÀx™a£ÐQ@ÀÍâÅÀ!ä¼ÿR@ÀÍâÅÀ', 'country_flag_io.png', 0, 0),
(107, 'IQ', 'Iraq', 'Republic of Iraq', 'IRQ', 368, 964, 'AS', 33.223191, 43.679291, 'IRQ', 1, 2129896, '2019-11-21 08:30:07', 1, '37.378029', '48.575916', '29.069445', '38.795887', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ÿ·IH@DøAc°B@Ÿ·IH@èÞÃ%Ç=@á\' ßeC@èÞÃ%Ç=@á\' ßeC@DøAc°B@Ÿ·IH@DøAc°B@', 'country_flag_iq.png', 1, 0),
(108, 'IR', 'Iran', 'Islamic Republic of Iran', 'IRN', 364, 98, 'AS', 32.427908, 53.688046, 'IRI', 1, 2197459, '2019-11-21 08:30:07', 1, '39.777222', '63.317471', '25.064083', '44.047279', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ö6Åã¢¨O@©Ý¯|ãC@Ö6Åã¢¨O@Å:U¾g9@Ðdÿ<\rF@Å:U¾g9@Ðdÿ<\rF@©Ý¯|ãC@Ö6Åã¢¨O@©Ý¯|ãC@', 'country_flag_ir.png', 1, 0),
(109, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', 352, 354, 'EU', 64.963051, -19.020835, 'ISL', 1, 2206440, '2019-11-21 08:30:07', 1, '66.534630', '-13.495815', '63.393253', '-24.546524', '\0\0\0\0\0\0\0\0\0\0\0\0\0„»³vÛý*Ày]¿`7¢P@„»³vÛý*ÀæCV²O@§z2ÿè‹8ÀæCV²O@§z2ÿè‹8Ày]¿`7¢P@„»³vÛý*Ày]¿`7¢P@', 'country_flag_is.png', 1, 0),
(110, 'IT', 'Italy', 'Italian Republic', 'ITA', 380, 39, 'EU', 41.87194, 12.56738, 'ITA', 1, 2211494, '2019-11-21 08:30:07', 1, '47.095196', '18.513445', '36.652779', '6.614889', '\0\0\0\0\0\0\0\0\0\0\0\0\0sK«!qƒ2@âZía/ŒG@sK«!qƒ2@ÿA$CŽSB@:®Fv¥u\Z@ÿA$CŽSB@:®Fv¥u\Z@âZía/ŒG@sK«!qƒ2@âZía/ŒG@', 'country_flag_it.png', 1, 0),
(111, 'JE', 'Jersey', 'Bailiwick of Jersey', 'JEY', 832, 441534, 'EU', 49.214439, -2.13125, 'JEY', 1, 2226745, '2019-11-21 08:30:07', 1, '49.265057', '-2.022083', '49.169834', '-2.260028', '\0\0\0\0\0\0\0\0\0\0\0\0\0kaÚ9-\0À¶IEcí¡H@kaÚ9-\0ÀÛßÙ½•H@´Z`‰ÀÛßÙ½•H@´Z`‰À¶IEcí¡H@kaÚ9-\0À¶IEcí¡H@', 'country_flag_je.png', 0, 0),
(112, 'JM', 'Jamaica', 'Jamaica', 'JAM', 388, 1876, 'NA', 18.109581, -77.297508, 'JAM', 1, NULL, '2019-11-21 08:30:07', 1, '18.526976', '-76.180321', '17.703554', '-78.366638', '\0\0\0\0\0\0\0\0\0\0\0\0\0\rraŠSÀãÆ-æç†2@\rraŠSÀQøl´1@\"Þ:ÿv—SÀQøl´1@\"Þ:ÿv—SÀãÆ-æç†2@\rraŠSÀãÆ-æç†2@', 'country_flag_jm.png', 1, 0),
(113, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', 400, 962, 'AS', 30.585164, 36.238414, 'JOR', 1, 2228679, '2019-11-21 08:30:07', 1, '33.367668', '39.301167', '29.185888', '34.959999', '\0\0\0\0\0\0\0\0\0\0\0\0\05Ñç£Œ¦C@“ä¹¾¯@@5Ñç£Œ¦C@¸ [–/=@«˜J?ázA@¸ [–/=@«˜J?ázA@“ä¹¾¯@@5Ñç£Œ¦C@“ä¹¾¯@@', 'country_flag_jo.png', 1, 0),
(114, 'JP', 'Japan', 'Japan', 'JPN', 392, 81, 'AS', 36.204824, 138.252924, 'JPN', 1, 2229862, '2019-11-21 08:30:07', 1, '45.523140', '145.820892', '24.249472', '122.938530', '\0\0\0\0\0\0\0\0\0\0\0\0\0‰±L¿D:b@^c@öÂF@‰±L¿D:b@‰D¡eÝ?8@\'\"à¼^@‰D¡eÝ?8@\'\"à¼^@^c@öÂF@‰±L¿D:b@^c@öÂF@', 'country_flag_jp.png', 1, 0),
(115, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', 404, 254, 'AF', -0.023559, 37.906193, 'KEN', 1, 2245255, '2019-11-21 08:30:07', 1, '5.019938', '41.899078', '-4.678047', '33.908859', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÌFçüóD@dÊ‡ j@ÌFçüóD@‰šèóQ¶ÀoÖà}Uô@@‰šèóQ¶ÀoÖà}Uô@@dÊ‡ j@ÌFçüóD@dÊ‡ j@', 'country_flag_ke.png', 1, 0),
(116, 'KG', 'Kyrgyz Republic', 'Kyrgyz Republic', 'KGZ', 417, 996, 'AS', 41.20438, 74.766098, 'KGZ', 1, 2249455, '2019-11-21 08:30:07', 1, '43.238224', '80.283165', '39.172832', '69.276611', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ì—`T@¨À~žE@Ì—`T@äÙå[–C@°­Ÿþ³QQ@äÙå[–C@°­Ÿþ³QQ@¨À~žE@Ì—`T@¨À~žE@', 'country_flag_kg.png', 1, 0),
(117, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', 116, 855, 'AS', 12.565679, 104.990963, 'CAM', 1, 2257553, '2019-11-21 08:30:07', 1, '14.686417', '107.627724', '10.409083', '102.339996', '\0\0\0\0\0\0\0\0\0\0\0\0\0„ºH¡,èZ@=ÖŒr_-@„ºH¡,èZ@û²´SsÑ$@U1•~Â•Y@û²´SsÑ$@U1•~Â•Y@=ÖŒr_-@„ºH¡,èZ@=ÖŒr_-@', 'country_flag_kh.png', 1, 0),
(118, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', 296, 686, 'OC', 1.866858, -157.35992, 'KIR', 1, 2258094, '2019-11-21 08:30:07', 1, '4.719570', '-150.215347', '-11.446881', '169.556137', '\0\0\0\0\0\0\0\0\0\0\0\0\0VIdäÆbÀ»DõÖà@VIdäÆbÀUh –Íä&ÀcÒßË1e@Uh –Íä&ÀcÒßË1e@»DõÖà@VIdäÆbÀ»DõÖà@', 'country_flag_ki.png', 0, 0),
(119, 'KM', 'Comoros', 'Union of the Comoros', 'COM', 174, 269, 'AF', -11.6455, 43.3333, 'COM', 1, 2258255, '2019-11-21 08:30:07', 1, '-11.362381', '44.538223', '-12.387857', '43.215790', '\0\0\0\0\0\0\0\0\0\0\0\0\0>zÃ}äDF@0bŸ\0Š¹&À>zÃ}äDF@¢U1•Æ(Àäf¸Ÿ›E@¢U1•Æ(Àäf¸Ÿ›E@0bŸ\0Š¹&À>zÃ}äDF@0bŸ\0Š¹&À', 'country_flag_km.png', 1, 0),
(120, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Kitts and Nevis', 'KNA', 659, 1869, 'NA', 17.357822, -62.782998, 'SKN', 1, 2258645, '2019-11-21 08:30:07', 1, '17.420118', '-62.543266', '17.095343', '-62.869560', '\0\0\0\0\0\0\0\0\0\0\0\0\0®ƒƒ½‰EOÀÿunÚŒk1@®ƒƒ½‰EOÀç\Zfh1@sôø½MoOÀç\Zfh1@sôø½MoOÀÿunÚŒk1@®ƒƒ½‰EOÀÿunÚŒk1@', 'country_flag_kn.png', 1, 0),
(121, 'KP', 'North Korea', 'Democratic People\'s Republic of Korea', 'PRK', 408, 850, 'AS', 40.339852, 127.510093, 'PRK', 1, 2261750, '2019-11-21 08:30:07', 1, '43.006054', '130.674866', '37.673332', '124.315887', '\0\0\0\0\0\0\0\0\0\0\0\0\0Öå”€˜U`@G¢`Æ€E@Öå”€˜U`@Ö¬3¾/ÖB@ÒŽ~7_@Ö¬3¾/ÖB@ÒŽ~7_@G¢`Æ€E@Öå”€˜U`@G¢`Æ€E@', 'country_flag_kp.png', 0, 0),
(122, 'KR', 'South Korea', 'Republic of Korea', 'KOR', 410, 82, 'AS', 35.907757, 127.766922, 'KOR', 1, 2283909, '2019-11-21 08:30:07', 1, '38.612446', '129.584671', '33.190945', '125.887108', '\0\0\0\0\0\0\0\0\0\0\0\0\0mýôŸµ2`@sHj¡dNC@mýôŸµ2`@Ø*Áâp˜@@G¢`Æx_@Ø*Áâp˜@@G¢`Æx_@sHj¡dNC@mýôŸµ2`@sHj¡dNC@', 'country_flag_kr.png', 1, 0),
(123, 'KW', 'Kuwait', 'State of Kuwait', 'KWT', 414, 965, 'AS', 29.31166, 47.481766, 'KUW', 1, 2314781, '2019-11-21 08:30:07', 1, '30.095945', '48.431473', '28.524611', '46.555557', '\0\0\0\0\0\0\0\0\0\0\0\0\0²\rÜ:7H@ø6ýÙ>@²\rÜ:7H@2èL†<@-å}GG@2èL†<@-å}GG@ø6ýÙ>@²\rÜ:7H@ø6ýÙ>@', 'country_flag_kw.png', 1, 0),
(124, 'KY', 'Cayman Islands', 'Cayman Islands', 'CYM', 136, 1345, 'NA', 19.3133, -81.2546, 'CAY', 1, NULL, '2019-11-21 08:30:07', 1, '19.761700', '-79.727272', '19.263029', '-81.432777', '\0\0\0\0\0\0\0\0\0\0\0\0\0úÒÛŸ‹îSÀû\\mÅþÂ3@úÒÛŸ‹îSÀKæXÞUC3@]Mž²[TÀKæXÞUC3@]Mž²[TÀû\\mÅþÂ3@úÒÛŸ‹îSÀû\\mÅþÂ3@', 'country_flag_ky.png', 1, 0),
(125, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', 398, 7, 'AS', 48.019573, 66.923684, 'KAZ', 1, 2319103, '2019-11-21 08:30:07', 1, '55.451195', '87.312668', '40.936333', '46.491859', '\0\0\0\0\0\0\0\0\0\0\0\0\0^ ¤ÀÔU@2üÁÀ¹K@^ ¤ÀÔU@1•~ÂÙwD@#ŸW<õ>G@1•~ÂÙwD@#ŸW<õ>G@2üÁÀ¹K@^ ¤ÀÔU@2üÁÀ¹K@', 'country_flag_kz.png', 1, 0),
(126, 'LA', 'Lao People\'s Democratic Republic', 'Lao People\'s Democratic Republic', 'LAO', 418, 856, 'AS', 19.85627, 102.495496, 'LAO', 1, 2319965, '2019-11-21 08:30:07', 1, '22.500389', '107.697029', '13.910027', '100.093056', '\0\0\0\0\0\0\0\0\0\0\0\0\0E×…œìZ@4GV~€6@E×…œìZ@8ôïÑ+@•,\'¡ôY@8ôïÑ+@•,\'¡ôY@4GV~€6@E×…œìZ@4GV~€6@', 'country_flag_la.png', 1, 0),
(127, 'LB', 'Lebanon', 'Lebanese Republic', 'LBN', 422, 961, 'AS', 33.854721, 35.862285, 'LIB', 5, 2330805, '2019-11-21 08:30:07', 1, '34.691418', '36.639194', '33.053860', '35.114277', '\0\0\0\0\0\0\0\0\0\0\0\0\0TææÑQB@Ðîb€XA@TææÑQB@Hmâä†@@®×ô  ŽA@Hmâä†@@®×ô  ŽA@Ðîb€XA@TææÑQB@Ðîb€XA@', 'country_flag_lb.png', 1, 0),
(128, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', 662, 1758, 'NA', 13.909444, -60.978893, 'LCA', 1, 2332374, '2019-11-21 08:30:07', 1, '14.103245', '-60.874203', '13.704778', '-61.074150', '\0\0\0\0\0\0\0\0\0\0\0\0\0UˆGâåoNÀwóT‡Ü4,@UˆGâåoNÀmáy©Øh+@ÌH¿}‰NÀmáy©Øh+@ÌH¿}‰NÀwóT‡Ü4,@UˆGâåoNÀwóT‡Ü4,@', 'country_flag_lc.png', 1, 0),
(129, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', 438, 423, 'EU', 47.166, 9.555373, 'LIE', 1, 2332414, '2019-11-21 08:30:07', 1, '47.270625', '9.635643', '47.048428', '9.471674', '\0\0\0\0\0\0\0\0\0\0\0\0\0×ÜÑÿrE#@q=\n×£¢G@×ÜÑÿrE#@ø\Z‚ã2†G@<¿(Añ\"@ø\Z‚ã2†G@<¿(Añ\"@q=\n×£¢G@×ÜÑÿrE#@q=\n×£¢G@', 'country_flag_li.png', 1, 0),
(130, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', 144, 94, 'AS', 7.873054, 80.771797, 'SRI', 1, 2342506, '2019-11-21 08:30:07', 1, '9.831361', '81.881279', '5.916833', '79.652916', '\0\0\0\0\0\0\0\0\0\0\0\0\0´éàfxT@÷V$&¨©#@´éàfxT@“EÖª@@Â0`ÉéS@“EÖª@@Â0`ÉéS@÷V$&¨©#@´éàfxT@÷V$&¨©#@', 'country_flag_lk.png', 1, 0),
(131, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', 430, 231, 'AF', 6.428055, -9.429499, 'LBR', 1, 2351622, '2019-11-21 08:30:07', 1, '8.551791', '-7.365113', '4.353057', '-11.492083', '\0\0\0\0\0\0\0\0\0\0\0\0\0·`©.àuÀKt–Y„\Z!@·`©.àuÀØ~2Æ‡i@ËÕMòû&ÀØ~2Æ‡i@ËÕMòû&ÀKt–Y„\Z!@·`©.àuÀKt–Y„\Z!@', 'country_flag_lr.png', 1, 0),
(132, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', 426, 266, 'AF', -29.609988, 28.233608, 'LES', 1, 2354904, '2019-11-21 08:30:07', 1, '-28.572058', '29.465761', '-30.668964', '27.029068', '\0\0\0\0\0\0\0\0\0\0\0\0\0”Àæ<w=@Hj¡dr’<À”Àæ<w=@‹3†9A«>À1\\\0q;@‹3†9A«>À1\\\0q;@Hj¡dr’<À”Àæ<w=@Hj¡dr’<À', 'country_flag_ls.png', 1, 0),
(133, 'LT', 'Lithuania', 'Republic of Lithuania', 'LTU', 440, 370, 'EU', 55.169438, 23.881275, 'LTU', 1, 2354941, '2019-11-21 08:30:07', 1, '56.446918', '26.871944', '53.901306', '20.941528', '\0\0\0\0\0\0\0\0\0\0\0\0\0„ñÓ¸7ß:@2ÿè›49L@„ñÓ¸7ß:@#Ø¸þ]óJ@¯D úñ4@#Ø¸þ]óJ@¯D úñ4@2ÿè›49L@„ñÓ¸7ß:@2ÿè›49L@', 'country_flag_lt.png', 1, 0),
(134, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', 442, 352, 'EU', 49.815273, 6.129583, 'LUX', 1, 2355498, '2019-11-21 08:30:07', 1, '50.184944', '6.528472', '49.446583', '5.734556', '\0\0\0\0\0\0\0\0\0\0\0\0\0\'h“Ã\'\Z@´Ë·>¬I@\'h“Ã\'\Z@‹ù¹¡)¹H@ƒR´r/ð@‹ù¹¡)¹H@ƒR´r/ð@´Ë·>¬I@\'h“Ã\'\Z@´Ë·>¬I@', 'country_flag_lu.png', 1, 0),
(135, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', 428, 371, 'EU', 56.879635, 24.603189, 'LAT', 1, 2357214, '2019-11-21 08:30:07', 1, '58.082306', '28.241167', '55.668861', '20.974277', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÛßÙ½=<@Þ!Å\0‰\nM@ÛßÙ½=<@ñH¼<ÕK@¸>¬7jù4@ñH¼<ÕK@¸>¬7jù4@Þ!Å\0‰\nM@ÛßÙ½=<@Þ!Å\0‰\nM@', 'country_flag_lv.png', 1, 0),
(136, 'LY', 'Libya', 'Libya', 'LBY', 434, 218, 'AF', 26.3351, 17.228331, 'LBA', 1, 2360633, '2019-11-21 08:30:07', 1, '33.168999', '25.150612', '19.508045', '9.387020', '\0\0\0\0\0\0\0\0\0\0\0\0\0™b‚Ž&9@B]Â¡•@@™b‚Ž&9@uå³<‚3@¼ËE|\'Æ\"@uå³<‚3@¼ËE|\'Æ\"@B]Â¡•@@™b‚Ž&9@B]Â¡•@@', 'country_flag_ly.png', 1, 0),
(137, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', 504, 212, 'AF', 31.791702, -7.09262, 'MAR', 1, 2364611, '2019-11-21 08:30:07', 1, '35.922497', '-0.991750', '27.662115', '-13.168586', '\0\0\0\0\0\0\0\0\0\0\0\0\0#Ûù~j¼ï¿=Ô¶aöA@#Ûù~j¼ï¿è0_^€©;@} yçPV*Àè0_^€©;@} yçPV*À=Ô¶aöA@#Ûù~j¼ï¿=Ô¶aöA@', 'country_flag_ma.png', 1, 0),
(138, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', 492, 377, 'EU', 43.738418, 7.424616, 'MON', 1, 2377282, '2019-11-21 08:30:07', 1, '43.751967', '7.439939', '43.724728', '7.408962', '\0\0\0\0\0\0\0\0\0\0\0\0\0•ð„^Â@ëUdt@àE@•ð„^Â@l?ãÃÜE@:=ïÆ¢@l?ãÃÜE@:=ïÆ¢@ëUdt@àE@•ð„^Â@ëUdt@àE@', 'country_flag_mc.png', 1, 0),
(139, 'MD', 'Moldova', 'Republic of Moldova', 'MDA', 498, 373, 'EU', 47.411631, 28.369885, 'MDA', 1, 2378534, '2019-11-21 08:30:07', 1, '48.490166', '30.135445', '45.468887', '26.618944', '\0\0\0\0\0\0\0\0\0\0\0\0\0h†¬\">@:ÎmÂ½>H@h†¬\">@B=}¼F@0.sž:@B=}¼F@0.sž:@:ÎmÂ½>H@h†¬\">@:ÎmÂ½>H@', 'country_flag_md.png', 1, 0),
(140, 'ME', 'Montenegro', 'Montenegro', 'MNE', 499, 382, 'EU', 42.708678, 19.37439, 'MNE', 1, 2379075, '2019-11-21 08:30:07', 1, '43.570137', '20.358833', '41.850166', '18.461306', '\0\0\0\0\0\0\0\0\0\0\0\0\0¿¹¿zÜ[4@ªžÌ?úÈE@¿¹¿zÜ[4@èO=ÒìD@Örg&v2@èO=ÒìD@Örg&v2@ªžÌ?úÈE@¿¹¿zÜ[4@ªžÌ?úÈE@', 'country_flag_me.png', 1, 0),
(141, 'MF', 'Saint Martin', 'Saint Martin (French part)', 'MAF', 663, 590, 'NA', 18.08255, -63.052251, 'MAF', 1, NULL, '2019-11-21 08:30:07', 1, '18.130354', '-63.012993', '18.052231', '-63.152767', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ù /Á©OÀç6á^!2@Ù /Á©OÀ^ÖÄ_\r2@:tzÞ“OÀ^ÖÄ_\r2@:tzÞ“OÀç6á^!2@Ù /Á©OÀç6á^!2@', 'country_flag_mf.png', 0, 0),
(142, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', 450, 261, 'AF', -18.766947, 46.869107, 'MAD', 3, 2387594, '2019-11-21 08:30:07', 1, '-11.945433', '50.483780', '-25.608952', '43.224876', '\0\0\0\0\0\0\0\0\0\0\0\0\0½:Ç€ì=I@KOËä\'À½:Ç€ì=I@sÕ<Gä›9ÀáÓœ¼ÈœE@sÕ<Gä›9ÀáÓœ¼ÈœE@KOËä\'À½:Ç€ì=I@KOËä\'À', 'country_flag_mg.png', 1, 0),
(143, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', 584, 692, 'OC', 7.131474, 171.184478, 'MHL', 1, 2394482, '2019-11-21 08:30:07', 1, '14.620000', '171.931808', '5.587639', '165.524918', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÖÄ_Ñ}e@=\n×£p=-@ÖÄ_Ñ}e@¥j»  ¾Y@žbÕ Ì°d@¥j» ¾Y@žbÕ Ì°d@=\n×£p=-@ÖÄ_Ñ}e@=\n×£p=-@', 'country_flag_mh.png', 0, 0),
(144, 'MK', 'Macedonia', 'Republic of Macedonia', 'MKD', 807, 389, 'EU', 41.608635, 21.745275, 'MKD', 1, 2394623, '2019-11-21 08:30:07', 1, '42.361805', '23.038139', '40.860195', '20.464695', '\0\0\0\0\0\0\0\0\0\0\0\0\0À³=zÃ  7@¿CQ O.E@À³=zÃ 7@c—¨Þ\ZnD@^c@öv4@c—¨Þ\ZnD@^c@öv4@¿CQ O.E@À³=zÃ 7@¿CQ O.E@', 'country_flag_mk.png', 1, 0),
(145, 'ML', 'Mali', 'Republic of Mali', 'MLI', 466, 223, 'AF', 17.570692, -3.996166, 'MLI', 1, 2401944, '2019-11-21 08:30:07', 1, '25.000002', '4.244968', '10.159513', '-12.242614', '\0\0\0\0\0\0\0\0\0\0\0\0\0 D2äØú@Aï!\0\09@ D2äØú@Þ’°«Q$@~\Z÷æ7|(ÀÞ’°«Q$@~\Z÷æ7|(ÀAï!\0\09@ D2äØú@Aï!\0\09@', 'country_flag_ml.png', 1, 0),
(146, 'MM', 'Myanmar', 'Republic of the Union of Myanmar', 'MMR', 104, 95, 'AS', 21.913965, 95.956223, 'MYA', 1, NULL, '2019-11-21 08:30:07', 1, '28.543249', '101.176781', '9.784583', '92.189278', '\0\0\0\0\0\0\0\0\0\0\0\0\0xcAaPKY@¯•Ð]‹<@xcAaPKY@ÁþëÜ´‘#@Œöx!W@ÁþëÜ´‘#@Œöx!W@¯•Ð]‹<@xcAaPKY@¯•Ð]‹<@', 'country_flag_mm.png', 1, 0),
(147, 'MN', 'Mongolia', 'Mongolia', 'MNG', 496, 976, 'AS', 46.862496, 103.846656, 'MGL', 1, NULL, '2019-11-21 08:30:07', 1, '52.154251', '119.924309', '41.567638', '87.749664', '\0\0\0\0\0\0\0\0\0\0\0\0\0€™ïà\'û]@\n0,¾J@€™ïà\'û]@Âûª\\¨ÈD@D¿¶~úïU@Âûª\\¨ÈD@D¿¶~úïU@\n0,¾J@€™ïà\'û]@\n0,¾J@', 'country_flag_mn.png', 1, 0);
INSERT INTO `geocountries` (`id`, `code`, `name`, `full_name`, `iso3`, `number`, `dialing_code`, `continent_code`, `latitude`, `longitude`, `ioc_code`, `popularity`, `capital_city`, `last_modified`, `used_by_safe_charge`, `bounding_box_north`, `bounding_box_east`, `bounding_box_south`, `bounding_box_west`, `bounding_box`, `flag_icon`, `search_hotels`, `published`) VALUES
(148, 'MO', 'Macao', 'Macao Special Administrative Region of China', 'MAC', 446, 853, 'AS', 22.198745, 113.543873, 'MAC', 1, NULL, '2019-11-21 08:30:07', 1, '22.222334', '113.565834', '22.180389', '113.528946', '\0\0\0\0\0\0\0\0\0\0\0\0\0À=ÏŸ6d\\@óÉŠáê86@À=ÏŸ6d\\@ãŽ7ù-.6@gÖR@Úa\\@ãŽ7ù-.6@gÖR@Úa\\@óÉŠáê86@À=ÏŸ6d\\@óÉŠáê86@', 'country_flag_mo.png', 1, 0),
(149, 'MP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'MNP', 580, 1670, 'OC', 15.0979, 145.6739, 'MNP', 1, 2439755, '2019-11-21 08:30:07', 1, '20.553440', '146.065280', '14.110230', '144.886260', '\0\0\0\0\0\0\0\0\0\0\0\0\0§\"ÆBb@YLl>®4@§\"ÆBb@\np8,@xî=\\b@\np8,@xî=\\b@YLl>®4@§\"ÆBb@YLl>®4@', 'country_flag_mp.png', 1, 0),
(150, 'MQ', 'Martinique', 'Martinique', 'MTQ', 474, 596, 'NA', 14.641528, -61.024174, 'MTQ', 1, 2440227, '2019-11-21 08:30:07', 1, '14.878819', '-60.815510', '14.392262', '-61.230118', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÎÇµ¡bhNÀô4`ôÁ-@ÎÇµ¡bhNÀWíšÖÈ,@G²tNÀWíšÖÈ,@G²tNÀô4`ôÁ-@ÎÇµ¡bhNÀô4`ôÁ-@', 'country_flag_mq.png', 1, 0),
(151, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', 478, 222, 'AF', 21.00789, -10.940835, 'MTN', 1, 2440450, '2019-11-21 08:30:07', 1, '27.298073', '-4.827674', '14.715547', '-17.066521', '\0\0\0\0\0\0\0\0\0\0\0\0\0~ÿæÅ‰OÀÒ\ZƒNL;@~ÿæÅ‰OÀx€\'-\\n-@J/…1Àx€\'-\\n-@J/…1ÀÒ\ZƒNL;@~ÿæÅ‰OÀÒ\ZƒNL;@', 'country_flag_mr.png', 1, 0),
(152, 'MS', 'Montserrat', 'Montserrat', 'MSR', 500, 1664, 'NA', 16.742498, -62.187366, 'MSR', 1, 2441219, '2019-11-21 08:30:07', 1, '16.824060', '-62.144100', '16.674769', '-62.241382', '\0\0\0\0\0\0\0\0\0\0\0\0\0C­iÞqOÀñ˜õÒ0@C­iÞqOÀÉZC©½¬0@êëùšåOÀÉZC©½¬0@êëùšåOÀñ˜õÒ0@C­iÞqOÀñ˜õÒ0@', 'country_flag_ms.png', 0, 0),
(153, 'MT', 'Malta', 'Republic of Malta', 'MLT', 470, 356, 'EU', 35.937496, 14.375416, 'MLT', 1, 2441268, '2019-11-21 08:30:07', 1, '36.082153', '14.576492', '35.806184', '14.183425', '\0\0\0\0\0\0\0\0\0\0\0\0\0¯Ïœõ)\'-@W\"Pýƒ\nB@¯Ïœõ)\'-@|G  1çA@ ‰°áé],@|G 1çA@ ‰°áé],@W\"Pýƒ\nB@¯Ïœõ)\'-@W\"Pýƒ\nB@', 'country_flag_mt.png', 1, 0),
(154, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', 480, 230, 'AF', -20.348404, 57.552152, 'MRI', 1, 2441529, '2019-11-21 08:30:07', 1, '-10.319255', '63.500179', '-20.525717', '56.512718', '\0\0\0\0\0\0\0\0\0\0\0\0\0°’ÝÀO@A+0du£$À°’ÝÀO@„ó©c•†4À Q¾ AL@„ó©c•†4À Q¾ AL@A+0du£$À°’ÝÀO@A+0du£$À', 'country_flag_mu.png', 1, 0),
(155, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', 462, 960, 'AS', 3.987028, 73.497775, 'MDV', 1, 2441948, '2019-11-21 08:30:07', 1, '7.091587', '73.637276', '-0.692694', '72.693222', '\0\0\0\0\0\0\0\0\0\0\0\0\0¥¡F!ÉhR@{ô†ûÈ]@¥¡F!ÉhR@eU„›Œ*æ¿‰·Î¿],R@eU„›Œ*æ¿‰·Î¿],R@{ô†ûÈ]@¥¡F!ÉhR@{ô†ûÈ]@', 'country_flag_mv.png', 1, 0),
(156, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', 454, 265, 'AF', -13.254308, 34.301525, 'MAW', 1, 2444108, '2019-11-21 08:30:07', 1, '-9.367541', '35.916821', '-17.125000', '32.673950', '\0\0\0\0\0\0\0\0\0\0\0\0\0œ¤ùcZõA@Öà}U.¼\"Àœ¤ùcZõA@\0\0\0\0\0 1ÀÑ‘\\þCV@@\0\0\0\0\0 1ÀÑ‘\\þCV@@Öà}U.¼\"Àœ¤ùcZõA@Öà}U.¼\"À', 'country_flag_mw.png', 1, 0),
(157, 'MX', 'Mexico', 'United Mexican States', 'MEX', 484, 52, 'NA', 23.634501, -102.552784, 'MEX', 1, 7207, '2019-11-22 02:53:12', 1, '32.716759', '-86.703392', '14.532866', '-118.453949', '\0\0\0\0\0\0\0\0\0\0\0\0\0\'á_­UÀHÂ¾[@@\'á_­UÀùLöÏÓ-@RC€\r]ÀùLöÏÓ-@RC€\r]ÀHÂ¾[@@\'á_­UÀHÂ¾[@@', 'country_flag_mx.png', 1, 0),
(158, 'MY', 'Malaysia', 'Malaysia', 'MYS', 458, 60, 'AS', 4.210484, 101.975766, 'MAS', 1, 47801, '2019-11-21 08:30:07', 1, '7.363417', '119.267502', '0.855222', '99.643448', '\0\0\0\0\0\0\0\0\0\0\0\0\0UgµÀÑ]@â=–#t@UgµÀÑ]@;\Z‡ú]ë?N+…@.éX@;\Z‡ú]ë?N+…@.éX@â=–#t@UgµÀÑ]@â=–#t@', 'country_flag_my.png', 1, 0),
(159, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', 508, 258, 'AF', -18.665695, 35.529562, 'MOZ', 1, 71191, '2019-11-21 08:30:07', 1, '-10.471883', '40.842995', '-26.868685', '30.217319', '\0\0\0\0\0\0\0\0\0\0\0\0\0„Ø™BçkD@¥  ¦šñ$À„Ø™BçkD@˜†á#bÞ:À§ÌÍ7¢7>@˜†á#bÞ:À§ÌÍ7¢7>@¥ ¦šñ$À„Ø™BçkD@¥ ¦šñ$À', 'country_flag_mz.png', 1, 0),
(160, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', 516, 264, 'AF', -22.95764, 18.49041, 'NAM', 1, 83993, '2019-11-21 08:30:07', 1, '-16.959894', '25.256701', '-28.971430', '11.715630', '\0\0\0\0\0\0\0\0\0\0\0\0\0ºÙ(·A9@j ùœ»õ0ÀºÙ(·A9@qZð¢¯ø<À®,gn\'@qZð¢¯ø<À®,gn\'@j ùœ»õ0ÀºÙ(·A9@j ùœ»õ0À', 'country_flag_na.png', 1, 0),
(161, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', 540, 687, 'OC', -20.904305, 165.618042, 'NCL', 1, 85572, '2019-11-21 08:30:07', 1, '-19.549778', '168.129135', '-22.698000', '163.564667', '\0\0\0\0\0\0\0\0\0\0\0\0\0›8¹ß!e@oB@¾Œ3À›8¹ß!e@¦›Ä °²6À,D‡Àrd@¦›Ä °²6À,D‡Àrd@oB@¾Œ3À›8¹ß!e@oB@¾Œ3À', 'country_flag_nc.png', 1, 0),
(162, 'NE', 'Niger', 'Republic of Niger', 'NER', 562, 227, 'AF', 17.607789, 8.081666, 'NIG', 1, 91661, '2019-11-21 08:30:07', 1, '23.525026', '15.995643', '11.696975', '0.166250', '\0\0\0\0\0\0\0\0\0\0\0\0\0ûVëÄý/@¹Œ›\Zh†7@ûVëÄý/@[B>èÙd\'@Ház®GÅ?[B>èÙd\'@Ház®GÅ?¹Œ›\Zh†7@ûVëÄý/@¹Œ›\Zh†7@', 'country_flag_ne.png', 0, 0),
(163, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', 574, 672, 'OC', -29.040835, 167.954712, 'NFK', 1, 2449922, '2019-11-21 08:30:07', 1, '-28.995171', '167.997737', '-29.063077', '167.915432', '\0\0\0\0\0\0\0\0\0\0\0\0\0K %víÿd@xíÒ†Ãþ<ÀK %víÿd@:!tÐ%=ÀÈ¶8Kýd@:!tÐ%=ÀÈ¶8Kýd@xíÒ†Ãþ<ÀK %víÿd@xíÒ†Ãþ<À', 'country_flag_nf.png', 1, 0),
(164, 'NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', 566, 234, 'AF', 9.081999, 8.675277, 'NGR', 1, 112728, '2019-11-21 08:30:07', 1, '13.892007', '14.680073', '4.277144', '2.668432', '\0\0\0\0\0\0\0\0\0\0\0\0\0É;‡2\\-@,›9$µÈ+@É;‡2\\-@h!£Ë@ŠÌ\\àòX@h!£Ë@ŠÌ\\àòX@,›9$µÈ+@É;‡2\\-@,›9$µÈ+@', 'country_flag_ng.png', 1, 0),
(165, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', 558, 505, 'NA', 12.865416, -85.207229, 'NCA', 1, 123745, '2019-11-21 08:30:07', 1, '15.025909', '-82.738289', '10.707543', '-87.690308', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ç @¯TÀXÇñC\r.@Ç @¯TÀ{Cj%@³\n›.ìUÀ{Cj%@³\n›.ìUÀXÇñC\r.@Ç @¯TÀXÇñC\r.@', 'country_flag_ni.png', 1, 0),
(166, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', 528, 31, 'EU', 52.132633, 5.291266, 'NED', 1, 131952, '2019-11-21 08:30:07', 1, '53.512196', '7.227944', '50.753918', '3.362556', '\0\0\0\0\0\0\0\0\0\0\0\0\0Gå&jé@.’v£ÁJ@Gå&jé@Ðîb€`I@¥¿—Âƒæ\n@Ðîb€`I@¥¿—Âƒæ\n@.’v£ÁJ@Gå&jé@.’v£ÁJ@', 'country_flag_nl.png', 1, 0),
(167, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', 578, 47, 'EU', 60.472024, 8.468946, 'NOR', 1, 135571, '2019-11-21 08:30:07', 1, '71.188110', '31.078053', '57.977917', '4.650167', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÍZ\nHû?@<ƒ†þ ÌQ@ÍZ\nHû?@é™^b,ýL@ôÀÇ`Å™@é™^b,ýL@ôÀÇ`Å™@<ƒ†þ  ÌQ@ÍZ\nHû?@<ƒ†þ  ÌQ@', 'country_flag_no.png', 1, 0),
(168, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', 524, 977, 'AS', 28.394857, 84.124008, 'NEP', 1, 140997, '2019-11-21 08:30:07', 1, '30.433390', '88.199333', '26.356722', '80.056274', '\0\0\0\0\0\0\0\0\0\0\0\0\0Þ\03ßÁV@×i¤¥òn>@Þ\03ßÁV@ƒÃ\"R[:@^gCþ™T@ƒÃ\"R[:@^gCþ™T@×i¤¥òn>@Þ\03ßÁV@×i¤¥òn>@', 'country_flag_np.png', 1, 0),
(169, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', 520, 674, 'OC', -0.522778, 166.931503, 'NRU', 1, 196835, '2019-11-21 08:30:07', 1, '-0.504306', '166.945282', '-0.552333', '166.899033', '\0\0\0\0\0\0\0\0\0\0\0\0\0ëo À?Þd@ƒ¦%VF#à¿ëo À?Þd@ØpA¶¬á¿Ë ÚàÄÜd@ØpA¶¬á¿Ë ÚàÄÜd@ƒ¦%VF#à¿ëo À?Þd@ƒ¦%VF#à¿', 'country_flag_nr.png', 0, 0),
(170, 'NU', 'Niue', 'Niue', 'NIU', 570, 683, 'OC', -19.054445, -169.867233, 'NIU', 1, 2449920, '2019-11-21 08:30:07', 1, '-18.951069', '-169.775177', '-19.152193', '-169.951004', '\0\0\0\0\0\0\0\0\0\0\0\0\0‘óþ?Î8eÀK=Byó2À‘óþ?Î8eÀ®Õö&3À°ËðŸn>eÀ®Õö&3À°ËðŸn>eÀK=Byó2À‘óþ?Î8eÀK=Byó2À', 'country_flag_nu.png', 0, 0),
(171, 'NZ', 'New Zealand', 'New Zealand', 'NZL', 554, 64, 'OC', -40.900557, 174.885971, 'NZL', 2, 196951, '2019-11-21 08:30:07', 1, '-34.389668', '-180.000000', '-47.286026', '166.715500', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€fÀ&\Z¤à1AÀ\0\0\0\0\0€fÀ!çýœ¤GÀ7‰A`åÖd@!çýœ¤GÀ7‰A`åÖd@&\Z¤à1AÀ\0\0\0\0\0€fÀ&\Z¤à1AÀ', 'country_flag_nz.png', 1, 0),
(172, 'OM', 'Oman', 'Sultanate of Oman', 'OMN', 512, 968, 'AS', 21.512583, 55.923255, 'OMA', 1, 199276, '2019-11-21 08:30:07', 1, '26.387972', '59.836582', '16.645750', '51.882000', '\0\0\0\0\0\0\0\0\0\0\0\0\0\r6uëM@ƒÃ\"Rc:@\r6uëM@Zd;ßO¥0@7‰A`åðI@Zd;ßO¥0@7‰A`åðI@ƒÃ\"Rc:@\r6uëM@ƒÃ\"Rc:@', 'country_flag_om.png', 1, 0),
(173, 'PA', 'Panama', 'Republic of Panama', 'PAN', 591, 507, 'NA', 8.537981, -80.782127, 'PAN', 1, NULL, '2019-11-21 08:30:07', 1, '9.637514', '-77.174110', '7.197906', '-83.051445', '\0\0\0\0\0\0\0\0\0\0\0\0\0úDž$KSÀú{)<hF#@úDž$KSÀŒºÖÞ§Ê@¼\"øßJÃTÀŒºÖÞ§Ê@¼\"øßJÃTÀú{)<hF#@úDž$KSÀú{)<hF#@', 'country_flag_pa.png', 1, 0),
(174, 'PE', 'Peru', 'Republic of Peru', 'PER', 604, 51, 'SA', -9.189967, -75.015152, 'PER', 1, 211905, '2019-11-21 08:30:07', 1, '-0.012977', '-68.677986', '-18.349728', '-81.326744', '\0\0\0\0\0\0\0\0\0\0\0\0\0VIdd+QÀw1Ít¯“Š¿VIdd+QÀØ~2Æ‡Y2ÀƒŠª_éTTÀØ~2Æ‡Y2ÀƒŠª_éTTÀw1Ít¯“Š¿VIdd+QÀw1Ít¯“Š¿', 'country_flag_pe.png', 1, 0),
(175, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', 258, 689, 'OC', -17.679742, -149.406843, 'PYF', 1, 238396, '2019-11-21 08:30:07', 1, '-7.903573', '-134.929825', '-27.653572', '-152.877167', '\0\0\0\0\0\0\0\0\0\0\0\0\0\ZÀ[ ÁÝ`ÀÆ1’=BÀ\ZÀ[ ÁÝ`ÀÑ”~P§;À,D‡ÀcÀÑ”~P§;À,D‡ÀcÀÆ1’=BÀ\ZÀ[ ÁÝ`ÀÆ1’=BÀ', 'country_flag_pf.png', 1, 0),
(176, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', 598, 675, 'OC', -6.314993, 143.95555, 'PNG', 1, 241073, '2019-11-21 08:30:07', 1, '-1.318639', '155.963440', '-11.657861', '140.842865', '\0\0\0\0\0\0\0\0\0\0\0\0\0u€Ô~c@«®C5%õ¿u€Ô~c@² 0(ÓP\'À->Àøša@² 0(ÓP\'À->Àøša@«®C5%õ¿u€Ô~c@«®C5%õ¿', 'country_flag_pg.png', 1, 0),
(177, 'PH', 'Philippines', 'Republic of the Philippines', 'PHL', 608, 63, 'AS', 12.879721, 121.774017, 'PHI', 1, 259498, '2019-11-22 02:53:28', 1, '21.120611', '126.601524', '4.643306', '116.931557', '\0\0\0\0\0\0\0\0\0\0\0\0\0•ð„^¦_@±‰Ì\\à5@•ð„^¦_@EHÝÎ¾’@W@¡ž;]@EHÝÎ¾’@W@¡ž;]@±‰Ì\\à5@•ð„^¦_@±‰Ì\\à5@', 'country_flag_ph.png', 1, 1),
(178, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', 586, 92, 'AS', 30.375321, 69.345116, 'PAK', 1, 292942, '2019-11-21 08:30:07', 1, '37.097000', '77.840919', '23.786722', '60.878613', '\0\0\0\0\0\0\0\0\0\0\0\0\0qåìÑuS@#Ûù~jŒB@qåìÑuS@1íœfÉ7@”k\ndvpN@1íœfÉ7@”k\ndvpN@#Ûù~jŒB@qåìÑuS@#Ûù~jŒB@', 'country_flag_pk.png', 1, 0),
(179, 'PL', 'Poland', 'Republic of Poland', 'POL', 616, 48, 'EU', 51.919438, 19.145136, 'POL', 1, 362462, '2019-11-21 08:30:07', 1, '54.839138', '24.150749', '49.006363', '14.123000', '\0\0\0\0\0\0\0\0\0\0\0\0\0›çˆ|—&8@Yj½ßhkK@›çˆ|—&8@Ås¶€Ð€H@åÐ\"Ûù>,@Ås¶€Ð€H@åÐ\"Ûù>,@Yj½ßhkK@›çˆ|—&8@Yj½ßhkK@', 'country_flag_pl.png', 1, 0),
(180, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', 666, 508, 'NA', 46.8852, -56.3159, 'SPM', 1, 398655, '2019-11-21 08:30:07', 1, '47.146286', '-56.252991', '46.786041', '-56.420658', '\0\0\0\0\0\0\0\0\0\0\0\0\0W—Sb LÀlîè¹’G@W—Sb LÀV(ÒýœdG@€fØ5LÀV(ÒýœdG@€fØ5LÀlîè¹’G@W—Sb LÀlîè¹’G@', 'country_flag_pm.png', 0, 0),
(181, 'PN', 'Pitcairn Islands', 'Pitcairn Islands', 'PCN', 612, 64, 'OC', -24.376491, -128.324347, 'PCN', 1, NULL, '2019-11-21 08:30:07', 1, '-24.315865', '-124.772850', '-24.672565', '-128.346436', '\0\0\0\0\0\0\0\0\0\0\0\0\0¬­Ø_v1_ÀwóT‡ÜP8À¬­Ø_v1_À*oG8-¬8ÀEó\0`À*oG8-¬8ÀEó\0`ÀwóT‡ÜP8À¬­Ø_v1_ÀwóT‡ÜP8À', 'country_flag_pn.png', 0, 0),
(182, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', 630, 1787, 'NA', 18.220833, -66.590149, 'PUR', 1, 400468, '2019-11-21 08:30:07', 1, '18.520166', '-65.242737', '17.926405', '-67.942726', '\0\0\0\0\0\0\0\0\0\0\0\0\0Þ!Å\0‰OPÀ»}V™)…2@Þ!Å\0‰OPÀÓÙÉà(í1@°ÅnŸUüPÀÓÙÉà(í1@°ÅnŸUüPÀ»}V™)…2@Þ!Å\0‰OPÀ»}V™)…2@', 'country_flag_pr.png', 1, 0),
(183, 'PS', 'Palestinian Territory', 'Occupied Palestinian Territory', 'PSE', 275, 970, 'AS', 31.94657, 35.302723, 'PLE', 1, NULL, '2019-11-21 08:30:07', 1, '32.546387', '35.573296', '31.216541', '34.216660', '\0\0\0\0\0\0\0\0\0\0\0\0\0¼viÃaÉA@Óú[ðE@@¼viÃaÉA@>>!;o7?@ù,Ïƒ»A@>>!;o7?@ù,Ïƒ»A@Óú[ðE@@¼viÃaÉA@Óú[ðE@@', 'country_flag_ps.png', 1, 0),
(184, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', 620, 351, 'EU', 39.399872, -8.224454, 'POR', 20, 405805, '2019-11-21 08:30:07', 1, '42.154311', '-6.189159', '36.961250', '-9.500527', '\0\0\0\0\0\0\0\0\0\0\0\0\0ìùšå²ÁÀß4}vÀE@ìùšå²ÁÀ×£p=\n{B@­‡/E\0#À×£p=\n{B@­‡/E\0#Àß4}vÀE@ìùšå²ÁÀß4}vÀE@', 'country_flag_pt.png', 1, 0),
(185, 'PW', 'Palau', 'Republic of Palau', 'PLW', 585, 680, 'OC', 7.51498, 134.58252, 'PLW', 1, 418192, '2019-11-21 08:30:07', 1, '8.469660', '134.723070', '2.803600', '131.117880', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0W²c#×`@zˆFwð @\0W²c#×`@Y†8ÖÅm@HG¬Åc`@Y†8ÖÅm@HG¬Åc`@zˆFwð @\0W²c#×`@zˆFwð @', 'country_flag_pw.png', 1, 0),
(186, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', 600, 595, 'SA', -23.442503, -58.443832, 'PAR', 2, 419809, '2019-11-21 08:30:07', 1, '-19.294041', '-54.259354', '-27.608738', '-62.647076', '\0\0\0\0\0\0\0\0\0\0\0\0\0\nƒ2!KÀâ®^EFK3À\nƒ2!KÀÕé@Ö›;ÀdébÓROÀÕé@Ö›;ÀdébÓROÀâ®^EFK3À\nƒ2!KÀâ®^EFK3À', 'country_flag_py.png', 1, 0),
(187, 'QA', 'Qatar', 'State of Qatar', 'QAT', 634, 974, 'AS', 25.354826, 51.183884, 'QAT', 1, 420257, '2019-11-21 08:30:07', 1, '26.154722', '51.636639', '24.482944', '50.757221', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ø-c}ÑI@ÂøiÜ›\':@Ø-c}ÑI@§ÌÍ7¢{8@l#žì`I@§ÌÍ7¢{8@l#žì`I@ÂøiÜ›\':@Ø-c}ÑI@ÂøiÜ›\':@', 'country_flag_qa.png', 1, 0),
(188, 'RE', 'RÃ©union', 'RÃ©union', 'REU', 638, 262, 'AF', -21.115141, 55.536384, 'REU', 1, 420334, '2019-11-21 08:30:07', 1, '-20.868391', '55.838194', '-21.383747', '55.212192', '\0\0\0\0\0\0\0\0\0\0\0\0\0\nÚäðIëK@$aßNÞ4À\nÚäðIëK@(ðN>=b5À†<‚)›K@(ðN>=b5À†<‚)›K@$aßNÞ4À\nÚäðIëK@$aßNÞ4À', 'country_flag_re.png', 1, 0),
(189, 'RO', 'Romania', 'Romania', 'ROU', 642, 40, 'EU', 45.943161, 24.96676, 'ROU', 5, 433089, '2019-11-21 08:30:07', 1, '48.266945', '29.691055', '43.627304', '20.269972', '\0\0\0\0\0\0\0\0\0\0\0\0\0¾¼\0ûè°=@Ujö@+\"H@¾¼\0ûè°=@3SZKÐE@ñÕŽâE4@3SZKÐE@ñÕŽâE4@Ujö@+\"H@¾¼\0ûè°=@Ujö@+\"H@', 'country_flag_ro.png', 1, 0),
(190, 'RS', 'Serbia', 'Republic of Serbia', 'SRB', 688, 381, 'EU', 44.016521, 21.005859, 'SRB', 1, 435877, '2019-11-21 08:30:07', 1, '46.181389', '23.004997', '42.232216', '18.817020', '\0\0\0\0\0\0\0\0\0\0\0\0\0ÿ“¿{G7@Um7Á7G@ÿ“¿{G7@ÑÍþ@¹E@Œ-9(Ñ2@ÑÍþ@¹E@Œ-9(Ñ2@Um7Á7G@ÿ“¿{G7@Um7Á7G@', 'country_flag_rs.png', 1, 0),
(191, 'RU', 'Russian Federation', 'Russian Federation', 'RUS', 643, 7, 'EU', 61.52401, 105.318756, 'RUS', 1, 476139, '2019-11-21 08:30:07', 1, '81.857361', '-169.050000', '41.188862', '19.250000', '\0\0\0\0\0\0\0\0\0\0\0\0\0š™™™™!eÀj÷«\0ßvT@š™™™™!eÀ„ºH¡,˜D@\0\0\0\0\0@3@„ºH¡,˜D@\0\0\0\0\0@3@j÷«\0ßvT@š™™™™!eÀj÷«\0ßvT@', 'country_flag_ru.png', 1, 0),
(192, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', 646, 250, 'AF', -1.940278, 29.873888, 'RWA', 1, 588293, '2019-11-21 08:30:07', 1, '-1.053481', '30.895958', '-2.840679', '28.856794', '\0\0\0\0\0\0\0\0\0\0\0\0\0î–ä€]å>@iQŸäÛð¿î–ä€]å>@Íx[éµ¹À¶hÚVÛ<@Íx[éµ¹À¶hÚVÛ<@iQŸäÛð¿î–ä€]å>@iQŸäÛð¿', 'country_flag_rw.png', 1, 0),
(193, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', 682, 966, 'AS', 23.885942, 45.079162, 'KSA', 1, 595334, '2019-11-21 08:30:07', 1, '32.158333', '55.666584', '15.614250', '34.495693', '\0\0\0\0\0\0\0\0\0\0\0\0\0¸àŸRÕK@TpxAD@@¸àŸRÕK@Ñ\"Ûù~:/@–íCÞr?A@Ñ\"Ûù~:/@–íCÞr?A@TpxAD@@¸àŸRÕK@TpxAD@@', 'country_flag_sa.png', 1, 0),
(194, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', 090, 677, 'OC', -9.64571, 160.156194, 'SOL', 1, 596394, '2019-11-21 08:30:07', 1, '-6.589611', '166.980865', '-11.850555', '155.508606', '\0\0\0\0\0\0\0\0\0\0\0\0\0Pÿ>cßd@ŠriüÂ[\ZÀPÿ>cßd@æèñ{³\'À”€Fpc@æèñ{³\'À”€Fpc@ŠriüÂ[\ZÀPÿ>cßd@ŠriüÂ[\ZÀ', 'country_flag_sb.png', 1, 0),
(195, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', 690, 248, 'AF', -4.679574, 55.491977, 'SEY', 1, 596405, '2019-11-21 08:30:07', 1, '-4.283717', '56.297703', '-9.753867', '46.204769', '\0\0\0\0\0\0\0\0\0\0\0\0\0æuÄ!&L@åF‘µ†\"ÀæuÄ!&L@8ýÚú#ÀßÞ5\ZG@8ýÚú#ÀßÞ5\ZG@åF‘µ†\"ÀæuÄ!&L@åF‘µ†\"À', 'country_flag_sc.png', 1, 0),
(196, 'SD', 'Sudan', 'Republic of Sudan', 'SDN', 729, 249, 'AF', 12.862807, 30.217636, 'SUD', 1, 600032, '2019-11-21 08:30:07', 1, '22.232220', '38.607498', '8.684721', '21.827774', '\0\0\0\0\0\0\0\0\0\0\0\0\0U1•~ÂMC@%zÅr;6@U1•~ÂMC@ÒÄ;À“^!@§z2ÿèÓ5@ÒÄ;À“^!@§z2ÿèÓ5@%zÅr;6@U1•~ÂMC@%zÅr;6@', 'country_flag_sd.png', 1, 0),
(197, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', 752, 46, 'EU', 60.128161, 18.643501, 'SWE', 1, 608643, '2019-11-21 08:30:07', 1, '69.062500', '24.156292', '55.337112', '11.118694', '\0\0\0\0\0\0\0\0\0\0\0\0\0^ ¤À(8@\0\0\0\0\0DQ@^ ¤À(8@i‹k|&«K@}vÀuÅ<&@i‹k|&«K@}vÀuÅ<&@\0\0\0\0\0DQ@^ ¤À(8@\0\0\0\0\0DQ@', 'country_flag_se.png', 1, 0),
(198, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', 702, 65, 'AS', 1.352083, 103.819836, 'SIN', 5, 2449847, '2019-11-21 08:30:07', 1, '1.471278', '104.007469', '1.258556', '103.638275', '\0\0\0\0\0\0\0\0\0\0\0\0\0÷®A_z\0Z@I0ÕÌZŠ÷?÷®A_z\0Z@ôÂ#ô?®¶bÙèY@ôÂ#ô?®¶bÙèY@I0ÕÌZŠ÷?÷®A_z\0Z@I0ÕÌZŠ÷?', 'country_flag_sg.png', 1, 0),
(199, 'SH', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', 654, 290, 'AF', 0, 0, 'SHN', 1, 631087, '2019-11-21 08:30:07', 1, '-7.887815', '-5.638753', '-16.019543', '-14.421230', '\0\0\0\0\0\0\0\0\0\0\0\0\07ã4DŽÀÌ—`À7ã4DŽÀ Ý!Å\00À+0du«×,À Ý!Å\00À+0du«×,ÀÌ—`À7ã4DŽÀÌ—`À', 'country_flag_sh.png', 0, 0),
(200, 'SI', 'Slovenia', 'Republic of Slovenia', 'SVN', 705, 386, 'EU', 46.151241, 14.995463, 'SLO', 1, 632762, '2019-11-21 08:30:07', 1, '46.876628', '16.610631', '45.421813', '13.375334', '\0\0\0\0\0\0\0\0\0\0\0\0\0}ì.PRœ0@a§X5pG@}ì.PRœ0@Šè÷ýµF@[\'.Ç+À*@Šè÷ýµF@[\'.Ç+À*@a§X5pG@}ì.PRœ0@a§X5pG@', 'country_flag_si.png', 1, 0),
(201, 'SJ', 'Svalbard & Jan Mayen Islands', 'Svalbard & Jan Mayen Islands', 'SJM', 744, 47, 'EU', 0, 0, 'SJM', 1, 634513, '2019-11-21 08:30:07', 1, '80.762085', '33.287334', '79.220306', '17.699389', '\0\0\0\0\0\0\0\0\0\0\0\0\0²ƒJ\\Ç¤@@kñ)\0Æ0T@²ƒJ\\Ç¤@@4GV~ÎS@¡.R(³1@4GV~ÎS@¡.R(³1@kñ)\0Æ0T@²ƒJ\\Ç¤@@kñ)\0Æ0T@', 'country_flag_sj.png', 1, 0),
(202, 'SK', 'Slovakia (Slovak Republic)', 'Slovakia (Slovak Republic)', 'SVK', 703, 421, 'EU', 48.669026, 19.699024, 'SVK', 1, 638925, '2019-11-21 08:30:07', 1, '49.603168', '22.570444', '47.728111', '16.847750', '\0\0\0\0\0\0\0\0\0\0\0\0\0\r34ž’6@2ÿè›4ÍH@\r34ž’6@ÎmÂ½2ÝG@/Ý$Ù0@ÎmÂ½2ÝG@/Ý$Ù0@2ÿè›4ÍH@\r34ž’6@2ÿè›4ÍH@', 'country_flag_sk.png', 1, 0),
(203, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', 694, 232, 'AF', 8.460555, -11.779889, 'SLE', 1, 643044, '2019-11-21 08:30:07', 1, '10.000000', '-10.284238', '6.929611', '-13.307631', '\0\0\0\0\0\0\0\0\0\0\0\0\0—¤¤‡‘$À\0\0\0\0\0\0$@—¤¤‡‘$Àç,òë·@¸xxÏ*Àç,òë·@¸xxÏ*À\0\0\0\0\0\0$@—¤¤‡‘$À\0\0\0\0\0\0$@', 'country_flag_sl.png', 1, 0),
(204, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', 674, 378, 'EU', 43.94236, 12.457777, 'SMR', 1, 644218, '2019-11-21 08:30:07', 1, '43.992237', '12.516532', '43.893709', '12.403539', '\0\0\0\0\0\0\0\0\0\0\0\0\0É¬Þáv)@Êp<ŸÿE@É¬Þáv)@’weòE@3Rï©œÎ(@’weòE@3Rï©œÎ(@Êp<ŸÿE@É¬Þáv)@Êp<ŸÿE@', 'country_flag_sm.png', 1, 0),
(205, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', 686, 221, 'AF', 14.497401, -14.452362, 'SEN', 1, 651540, '2019-11-21 08:30:07', 1, '16.691633', '-11.355887', '12.307275', '-17.535236', '\0\0\0\0\0\0\0\0\0\0\0\0\0¢$$Ò6¶&À™Õ;Ü±0@¢$$Ò6¶&À¾Á&S(@Q¤û9‰1À¾Á&S(@Q¤û9‰1À™Õ;Ü±0@¢$$Ò6¶&À™Õ;Ü±0@', 'country_flag_sn.png', 1, 0),
(206, 'SO', 'Somalia', 'Somali Republic', 'SOM', 706, 252, 'AF', 5.152149, 46.199616, 'SOM', 1, 654031, '2019-11-21 08:30:07', 1, '11.979166', '51.412636', '-1.674868', '40.986595', '\0\0\0\0\0\0\0\0\0\0\0\0\0}“¦AÑ´I@Ôµö>Uõ\'@}“¦AÑ´I@ðÞQcBÌú¿Õ²µ¾H~D@ðÞQcBÌú¿Õ²µ¾H~D@Ôµö>Uõ\'@}“¦AÑ´I@Ôµö>Uõ\'@', 'country_flag_so.png', 0, 0),
(207, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', 740, 597, 'SA', 3.919305, -56.027783, 'SUR', 1, 657468, '2019-11-21 08:30:07', 1, '6.004546', '-53.977493', '1.831145', '-58.086563', '\0\0\0\0\0\0\0\0\0\0\0\0\0Óˆ™}ýJÀ{Oå´§@Óˆ™}ýJÀ#¾³^Lý?–MÀ#¾³^Lý?–MÀ{Oå´§@Óˆ™}ýJÀ{Oå´§@', 'country_flag_sr.png', 1, 0),
(208, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', 728, 211, 'AF', 7.963092, 30.15893, 'SSD', 1, 659345, '2019-11-21 08:30:07', 0, '12.219149', '35.940552', '3.493394', '24.140274', '\0\0\0\0\0\0\0\0\0\0\0\0\0üdøA@÷æ7L4p(@üdøA@KW°xò@§z2ÿè#8@KW°xò@§z2ÿè#8@÷æ7L4p(@üdøA@÷æ7L4p(@', 'country_flag_ss.png', 1, 0),
(209, 'ST', 'Sao Tome and Principe', 'Democratic Republic of Sao Tome and Principe', 'STP', 678, 239, 'AF', 0.18636, 6.613081, 'STP', 1, 662556, '2019-11-21 08:30:07', 1, '1.701323', '7.466374', '0.024766', '6.470170', '\0\0\0\0\0\0\0\0\0\0\0\0\0žÑV%‘Ý@öëNwž8û?žÑV%‘Ý@m\06 B\\™?Å=–>tá@m\06 B\\™?Å=–>tá@öëNwž8û?žÑV%‘Ý@öëNwž8û?', 'country_flag_st.png', 1, 0),
(210, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', 222, 503, 'NA', 13.794185, -88.89653, 'ESA', 1, 663153, '2019-11-21 08:30:07', 1, '14.445067', '-87.692162', '13.148679', '-90.128662', '\0\0\0\0\0\0\0\0\0\0\0\0\0,bØaLìUÀÃcÒßã,@,bØaLìUÀ74e§L*@;Šÿ;ˆVÀ74e§L*@;Šÿ;ˆVÀÃcÒßã,@,bØaLìUÀÃcÒßã,@', 'country_flag_sv.png', 1, 0),
(211, 'SX', 'Sint Maarten (Dutch part)', 'Sint Maarten (Dutch part)', 'SXM', 534, 1721, 'NA', 18.08255, -63.052251, 'SXM', 1, 2449933, '2019-11-21 08:30:07', 0, '18.070248', '-63.012993', '18.011692', '-63.144039', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ù /Á©OÀœÞÅû2@Ù /Á©OÀõŸ5?þ2@,µÞo’OÀõŸ5?þ2@,µÞo’OÀœÞÅû2@Ù /Á©OÀœÞÅû2@', 'country_flag_sx.png', 1, 0),
(212, 'SY', 'Syrian Arab Republic', 'Syrian Arab Republic', 'SYR', 760, 963, 'AS', 34.802075, 38.996815, 'SYR', 1, 671008, '2019-11-21 08:30:07', 1, '37.319138', '42.385029', '32.310665', '35.727222', '\0\0\0\0\0\0\0\0\0\0\0\0\0|Y¡H1E@—t”ƒÙ¨B@|Y¡H1E@„çÞÃ\'@@BwIœÝA@„çÞÃ\'@@BwIœÝA@—t”ƒÙ¨B@|Y¡H1E@—t”ƒÙ¨B@', 'country_flag_sy.png', 1, 0),
(213, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', 748, 268, 'AF', -26.522503, 31.465866, 'SWZ', 1, 675371, '2019-11-21 08:30:07', 1, '-25.719648', '32.137260', '-27.317101', '30.794107', '\0\0\0\0\0\0\0\0\0\0\0\0\0D†U¼‘@@¾¡ðÙ:¸9ÀD†U¼‘@@f‡ø‡-Q;ÀQ†ª˜JË>@f‡ø‡-Q;ÀQ†ª˜JË>@¾¡ðÙ:¸9ÀD†U¼‘@@¾¡ðÙ:¸9À', 'country_flag_sz.png', 1, 0),
(214, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', 796, 1649, 'NA', 21.694025, -71.797928, 'TCA', 1, 2449904, '2019-11-21 08:30:07', 1, '21.961878', '-71.123642', '21.422626', '-72.483871', '\0\0\0\0\0\0\0\0\0\0\0\0\0_š\"ÀéÇQÀì½ø¢=ö5@_š\"ÀéÇQÀvp°71l5@æ¾÷RÀvp°71l5@æ¾÷RÀì½ø¢=ö5@_š\"ÀéÇQÀì½ø¢=ö5@', 'country_flag_tc.png', 1, 0),
(215, 'TD', 'Chad', 'Republic of Chad', 'TCD', 148, 235, 'AF', 15.454166, 18.732207, 'CHA', 1, 678597, '2019-11-21 08:30:07', 1, '23.450369', '24.002661', '7.441068', '13.473475', '\0\0\0\0\0\0\0\0\0\0\0\0\0ƒù+d®\08@Ù!þaKs7@ƒù+d®\08@ž?mT§Ã@|ò°Pkò*@ž?mT§Ã@|ò°Pkò*@Ù!þaKs7@ƒù+d®\08@Ù!þaKs7@', 'country_flag_td.png', 1, 0),
(216, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', 260, 0, 'AN', -49.280366, 69.348557, 'ATF', 1, 685785, '2019-11-21 08:30:07', 1, '-37.790722', '77.598808', '-49.735184', '50.170258', '\0\0\0\0\0\0\0\0\0\0\0\0\0S%ÊÞRfS@&å`6åBÀS%ÊÞRfS@oEb‚\ZÞHÀñðžËI@oEb‚\ZÞHÀñðžËI@&å`6åBÀS%ÊÞRfS@&å`6åBÀ', 'country_flag_tf.png', 0, 0),
(217, 'TG', 'Togo', 'Togolese Republic', 'TGO', 768, 228, 'AF', 8.619543, 0.824782, 'TOG', 1, 685910, '2019-11-21 08:30:07', 1, '11.138977', '1.806693', '6.104417', '-0.147324', '\0\0\0\0\0\0\0\0\0\0\0\0\0˜Në6èü?ÚÊKþ\'G&@˜Në6èü?ò•@Jìj@X<õHƒÛÂ¿ò•@Jìj@X<õHƒÛÂ¿ÚÊKþ\'G&@˜Në6èü?ÚÊKþ\'G&@', 'country_flag_tg.png', 1, 0),
(218, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', 764, 66, 'AS', 15.870032, 100.992541, 'THA', 1, 699450, '2019-11-21 08:30:07', 1, '20.463194', '105.639389', '5.610000', '97.345642', '\0\0\0\0\0\0\0\0\0\0\0\0\0×¿ëhZ@´Éá“v4@×¿ëhZ@q=\n×£p@ð‡ŸÿVX@q=\n×£p@ð‡ŸÿVX@´Éá“v4@×¿ëhZ@´Éá“v4@', 'country_flag_th.png', 1, 0),
(219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', 762, 992, 'AS', 38.861034, 71.276093, 'TJK', 1, 740462, '2019-11-21 08:30:07', 1, '41.042252', '75.137222', '36.674137', '67.387138', '\0\0\0\0\0\0\0\0\0\0\0\0\0«’È>ÈÈR@ewƒh…D@«’È>ÈÈR@JVB@|BvÞÆØP@JVB@|BvÞÆØP@ewƒh…D@«’È>ÈÈR@ewƒh…D@', 'country_flag_tj.png', 1, 0),
(220, 'TK', 'Tokelau', 'Tokelau', 'TKL', 772, 690, 'OC', -9.2002, -171.8484, 'TKL', 1, NULL, '2019-11-21 08:30:07', 1, '-8.553614', '-171.211426', '-9.381111', '-172.500336', '\0\0\0\0\0\0\0\0\0\0\0\0\0Åpu\0ÄfeÀ*7QKs!ÀÅpu\0ÄfeÀ®J\"û Ã\"À^ ¤ÀeÀ®J\"û Ã\"À^ ¤ÀeÀ*7QKs!ÀÅpu\0ÄfeÀ*7QKs!À', 'country_flag_tk.png', 0, 0),
(221, 'TL', 'Timor-Leste', 'Democratic Republic of Timor-Leste', 'TLS', 626, 670, 'AS', -8.874217, 125.727539, 'TLS', 1, 741250, '2019-11-21 08:30:07', 1, '-8.135834', '127.308594', '-9.463627', '124.046097', '\0\0\0\0\0\0\0\0\0\0\0\0\0zoÀÓ_@¦^·ŒE ÀzoÀÓ_@Ï¥„`í\"ÀfÜÔ@ó_@Ï¥„`í\"ÀfÜÔ@ó_@¦^·ŒE ÀzoÀÓ_@¦^·ŒE À', 'country_flag_tl.png', 1, 0),
(222, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', 795, 993, 'AS', 38.969719, 59.556278, 'TKM', 1, 741870, '2019-11-21 08:30:07', 1, '42.795555', '66.684303', '35.141083', '52.441444', '\0\0\0\0\0\0\0\0\0\0\0\0\0€cÏžË«P@«•  ¿ÔeE@€cÏžË«P@Ã‚û’A@ù«<8J@Ã‚û’A@ù«<8J@«• ¿ÔeE@€cÏžË«P@«• ¿ÔeE@', 'country_flag_tm.png', 0, 0),
(223, 'TN', 'Tunisia', 'Tunisian Republic', 'TUN', 788, 216, 'AF', 33.886917, 9.537499, 'TUN', 1, 742117, '2019-11-21 08:30:07', 1, '37.543915', '11.598278', '30.240417', '7.524833', '\0\0\0\0\0\0\0\0\0\0\0\0\0$Õw~Q2\'@äf¸ŸÅB@$Õw~Q2\'@gð÷‹=>@?rkÒm@gð÷‹=>@?rkÒm@äf¸ŸÅB@$Õw~Q2\'@äf¸ŸÅB@', 'country_flag_tn.png', 1, 0),
(224, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', 776, 676, 'OC', -21.178986, -175.198242, 'TGA', 1, NULL, '2019-11-21 08:30:07', 1, '-15.562988', '-173.907578', '-21.455057', '-175.682266', '\0\0\0\0\0\0\0\0\0\0\0\0\06’á\n½eÀ¶ö? /À6’á\n½eÀÝÐ”~t5À‡¥ÕõeÀÝÐ”~t5À‡¥ÕõeÀ¶ö? /À6’á\n½eÀ¶ö? /À', 'country_flag_to.png', 1, 0),
(225, 'TR', 'Turkey', 'Republic of Turkey', 'TUR', 792, 90, 'AS', 38.963745, 35.243322, 'TUR', 1, 765270, '2019-11-22 02:53:12', 1, '42.107613', '44.834999', '35.815418', '25.668501', '\0\0\0\0\0\0\0\0\0\0\0\0\0«˜J?ájF@îÏECÆ\rE@«˜J?ájF@íHõ_èA@âW¬á\"«9@íHõ_èA@âW¬á\"«9@îÏECÆ\rE@«˜J?ájF@îÏECÆ\rE@', 'country_flag_tr.png', 1, 0),
(226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', 780, 1868, 'NA', 10.691803, -61.222503, 'TTO', 1, 788978, '2019-11-21 08:30:07', 1, '11.338342', '-60.517933', '10.036105', '-61.923771', '\0\0\0\0\0\0\0\0\0\0\0\0\0tBè KBNÀú¹¡);­&@tBè KBNÀ´qÄZ|$@\"ÿÌ >öNÀ´qÄZ|$@\"ÿÌ >öNÀú¹¡);­&@tBè KBNÀú¹¡);­&@', 'country_flag_tt.png', 1, 0),
(227, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', 798, 688, 'OC', -10.728072, 179.472656, 'TUV', 1, 789288, '2019-11-21 08:30:07', 1, '-5.641972', '179.863281', '-10.801169', '176.064865', '\0\0\0\0\0\0\0\0\0\0\0\0\0CÈyÿŸ{f@uÊ£a‘ÀCÈyÿŸ{f@W#»Ò2š%Àö´Ã_f@W#»Ò2š%Àö´Ã_f@uÊ£a‘ÀCÈyÿŸ{f@uÊ£a‘À', 'country_flag_tv.png', 0, 0),
(228, 'TW', 'Taiwan', 'Taiwan, Province of China', 'TWN', 158, 886, 'AS', 23.69781, 120.960515, 'TPE', 1, 790925, '2019-11-21 08:30:07', 1, '25.300290', '122.006740', '21.896607', '119.534691', '\0\0\0\0\0\0\0\0\0\0\0\0\0Íä›mn€^@ÛP1ÎßL9@Íä›mn€^@[]N  ˆå5@Ë™`8â]@[]N ˆå5@Ë™`8â]@ÛP1ÎßL9@Íä›mn€^@ÛP1ÎßL9@', 'country_flag_tw.png', 1, 0),
(229, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', 834, 255, 'AF', -6.369028, 34.888822, 'TAN', 1, 806369, '2019-11-21 08:30:07', 1, '-0.990736', '40.443222', '-11.745696', '29.327168', '\0\0\0\0\0\0\0\0\0\0\0\0\0o»8D@ ßû´ï¿o»8D@„¹ÝË}\'À8L4HÁS=@„¹ÝË}\'À8L4HÁS=@ ßû´ï¿o»8D@ ßû´ï¿', 'country_flag_tz.png', 1, 0),
(230, 'UA', 'Ukraine', 'Ukraine', 'UKR', 804, 380, 'EU', 48.379433, 31.16558, 'UKR', 1, 820988, '2019-11-21 08:30:07', 1, '52.369362', '40.207390', '44.390415', '22.128889', '\0\0\0\0\0\0\0\0\0\0\0\0\0;ÂiÁ‹\ZD@M1AG/J@;ÂiÁ‹\ZD@odù1F@lÐ—Þþ 6@odù1F@lÐ—Þþ 6@M1AG/J@;ÂiÁ‹\ZD@M1AG/J@', 'country_flag_ua.png', 1, 0),
(231, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', 800, 256, 'AF', 1.373333, 32.290275, 'UGA', 1, 836753, '2019-11-21 08:30:07', 1, '4.214427', '35.036049', '-1.484050', '29.573252', '\0\0\0\0\0\0\0\0\0\0\0\0\0Ùî@„A@„aÀ’Û@Ùî@„A@‘z6«¾÷¿Ù] ¤À’=@‘z6«¾÷¿Ù] ¤À’=@„aÀ’Û@Ùî@„A@„aÀ’Û@', 'country_flag_ug.png', 1, 0),
(232, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', 581, 246, 'OC', 5.88062, -162.075923, 'UMI', 1, NULL, '2019-11-21 08:30:07', 1, '28.219814', '166.654526', '-0.389006', '-177.392029', '\0\0\0\0\0\0\0\0\0\0\0\0\07Œ‚àñÔd@ô3õºE8<@7Œ‚àñÔd@]¨ükyåØ¿­Âf€‹,fÀ]¨ükyåØ¿­Âf€‹,fÀô3õºE8<@7Œ‚àñÔd@ô3õºE8<@', 'no_flag.png', 0, 0),
(233, 'US', 'United States of America', 'United States of America', 'USA', 840, 1, 'NA', 39.99041, -101.381753, 'USA', 1, 849585, '2019-11-21 08:30:07', 1, '49.388611', '-66.954811', '24.544245', '-124.733253', '\0\0\0\0\0\0\0\0\0\0\0\0\0·˜Ÿ½PÀÔîW¾±H@·˜Ÿ½PÀóì£S‹8@i¬ýí._Àóì£S‹8@i¬ýí._ÀÔîW¾±H@·˜Ÿ½PÀÔîW¾±H@', 'country_flag_us.png', 1, 0),
(234, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', 858, 598, 'SA', -32.522779, -55.765835, 'URU', 1, 952054, '2019-11-21 08:30:07', 1, '-30.082224', '-53.073933', '-34.980816', '-58.442722', '\0\0\0\0\0\0\0\0\0\0\0\0\0.Œô¢v‰JÀAòÎ¡>À.Œô¢v‰JÀ_²ñ`‹}AÀ œO«8MÀ_²ñ`‹}AÀ œO«8MÀAòÎ¡>À.Œô¢v‰JÀAòÎ¡>À', 'country_flag_uy.png', 1, 0),
(235, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', 860, 998, 'AS', 41.377491, 64.585262, 'UZB', 1, 953765, '2019-11-21 08:30:07', 1, '45.575001', '73.132278', '37.184444', '55.996639', '\0\0\0\0\0\0\0\0\0\0\0\0\0½þ$>wHR@jý¡™ÉF@½þ$>wHR@ÂøiÜ›—B@†uãÝ‘ÿK@ÂøiÜ›—B@†uãÝ‘ÿK@jý¡™ÉF@½þ$>wHR@jý¡™ÉF@', 'country_flag_uz.png', 1, 0),
(236, 'VA', 'Holy See (Vatican City State)', 'Holy See (Vatican City State)', 'VAT', 336, 379, 'EU', 41.902433, 12.451229, 'VAT', 1, 2449841, '2019-11-21 08:30:07', 1, '41.907438', '12.458375', '41.900280', '12.445707', '\0\0\0\0\0\0\0\0\0\0\0\0\0¦›Ä °ê(@æ’ªí&ôD@¦›Ä °ê(@Ÿ`<óD@Q29µ3ä(@Ÿ`<óD@Q29µ3ä(@æ’ªí&ôD@¦›Ä °ê(@æ’ªí&ôD@', 'country_flag_va.png', 0, 0),
(237, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, 'NA', 13.253383, -61.19595, 'VIN', 1, 956252, '2019-11-21 08:30:07', 1, '13.377834', '-61.113880', '12.583985', '-61.460903', '\0\0\0\0\0\0\0\0\0\0\0\0\0‘Õ­ž“ŽNÀ<¢BusÁ*@‘Õ­ž“ŽNÀ‰µø\0+)@lÐ—ÞþºNÀ‰µø\0+)@lÐ—ÞþºNÀ<¢BusÁ*@‘Õ­ž“ŽNÀ<¢BusÁ*@', 'country_flag_vc.png', 1, 0),
(238, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', 862, 58, 'SA', 6.42375, -66.58973, 'VEN', 1, 960886, '2019-11-21 08:30:07', 1, '12.201903', '-59.803780', '0.626311', '-73.354073', '\0\0\0\0\0\0\0\0\0\0\0\0\0æ–VCâæMÀ·í{Ô_g(@æ–VCâæMÀu\0Ä]½\nä?bÙÌ!©VRÀu\0Ä]½\nä?bÙÌ!©VRÀ·í{Ô_g(@æ–VCâæMÀ·í{Ô_g(@', 'country_flag_ve.png', 1, 0),
(239, 'VG', 'British Virgin Islands', 'British Virgin Islands', 'VGB', 092, 1284, 'NA', 18.420695, -64.639968, 'IVB', 1, NULL, '2019-11-21 08:30:07', 1, '18.757221', '-64.268768', '18.383711', '-64.713128', '\0\0\0\0\0\0\0\0\0\0\0\0\0†²~3PÀ,ØF<ÙÁ2@†²~3PÀTâ:b2@)wŸã£-PÀTâ:b2@)wŸã£-PÀ,ØF<ÙÁ2@†²~3PÀ,ØF<ÙÁ2@', 'country_flag_vg.png', 1, 0),
(240, 'VI', 'United States Virgin Islands', 'United States Virgin Islands', 'VIR', 850, 1340, 'NA', 18.335765, -64.896335, 'ISV', 1, 974486, '2019-11-21 08:30:07', 1, '18.415382', '-64.565193', '17.673931', '-65.101333', '\0\0\0\0\0\0\0\0\0\0\0\0\0f»B,$PÀÚX‰yVj2@f»B,$PÀµÂô½†¬1@[@h=|FPÀµÂô½†¬1@[@h=|FPÀÚX‰yVj2@f»B,$PÀÚX‰yVj2@', 'country_flag_vi.png', 1, 0),
(241, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', 704, 84, 'AS', 14.058324, 108.277199, 'VIE', 5, 988823, '2019-11-21 08:30:07', 1, '23.388834', '109.464638', '8.559611', '102.148224', '\0\0\0\0\0\0\0\0\0\0\0\0\0¥ž¡¼][@§’ Šc7@¥ž¡¼][@¶ö>U…!@Þ„€|‰Y@¶ö>U…!@Þ„€|‰Y@§’ Šc7@¥ž¡¼][@§’ Šc7@', 'country_flag_vn.png', 1, 0),
(242, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', 548, 678, 'OC', -15.376706, 166.959158, 'VAN', 1, NULL, '2019-11-21 08:30:07', 1, '-13.073444', '169.904785', '-20.248945', '166.524979', '\0\0\0\0\0\0\0\0\0\0\0\0\0*¬ÿó<e@Ã,´sš%*À*¬ÿó<e@²€  Üº?4ÀÇ‚Â ÌÐd@²€ Üº?4ÀÇ‚Â ÌÐd@Ã,´sš%*À*¬ÿó<e@Ã,´sš%*À', 'country_flag_vu.png', 1, 0),
(243, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', 876, 681, 'OC', -14.2938, -178.1165, 'WLF', 1, 1005676, '2019-11-21 08:30:07', 0, '-13.216758', '-176.161743', '-14.314560', '-178.184811', '\0\0\0\0\0\0\0\0\0\0\0\0\0lë§ÿ,fÀñJ’çún*Àlë§ÿ,fÀpB!¡,ÀÂøéEfÀpB!¡,ÀÂøéEfÀñJ’çún*Àlë§ÿ,fÀñJ’çún*À', 'country_flag_wf.png', 0, 0),
(244, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', 882, 685, 'OC', -13.759029, -172.104629, 'SAM', 1, 1005764, '2019-11-21 08:30:07', 1, '-13.432207', '-171.415741', '-14.040939', '-172.798599', '\0\0\0\0\0\0\0\0\0\0\0\0\0gÓÀMmeÀ]4d<JÝ*ÀgÓÀMmeÀCäôõ,ÀÉs}Ž™eÀCäôõ,ÀÉs}Ž™eÀ]4d<JÝ*ÀgÓÀMmeÀ]4d<JÝ*À', 'country_flag_ws.png', 1, 0),
(245, 'XK', 'Kosovo', 'Kosovo', 'KOS', 381, 383, 'EU', 42.6, 20.85, 'KOS', 1, 1006150, '2019-11-21 08:30:07', 0, '43.268250', '21.803351', '41.856370', '19.977482', '\0\0\0\0\0\0\0\0\0\0\0\0\0z5@i¨Í5@u“V¢E@z5@i¨Í5@D£;ˆíD@½m¦B<ú3@D£;ˆíD@½m¦B<ú3@u“V¢E@z5@i¨Í5@u“V¢E@', 'country_flag_xk.png', 1, 0),
(246, 'YE', 'Yemen', 'Yemen', 'YEM', 887, 967, 'AS', 15.552727, 48.516388, 'YEM', 1, 1009175, '2019-11-21 08:30:07', 1, '18.999999', '54.530539', '12.111091', '42.532539', '\0\0\0\0\0\0\0\0\0\0\0\0\0S ³³èCK@_9ïÿÿ2@S ³³èCK@‡Âgëà8(@\Zlê<*DE@‡Âgëà8(@\Zlê<*DE@_9ïÿÿ2@S ³³èCK@_9ïÿÿ2@', 'country_flag_ye.png', 1, 0),
(247, 'YT', 'Mayotte', 'Mayotte', 'MYT', 175, 262, 'AF', -12.8275, 45.166244, 'MYT', 1, 1037028, '2019-11-21 08:30:07', 1, '-12.648891', '45.292950', '-13.000132', '45.037960', '\0\0\0\0\0\0\0\0\0\0\0\0\0}®¶b¥F@eVïp;L)À}®¶b¥F@¼]/M\0*À0GßÛ„F@¼]/M\0*À0GßÛ„F@eVïp;L)À}®¶b¥F@eVïp;L)À', 'country_flag_yt.png', 1, 0),
(248, 'ZA', 'South Africa', 'Republic of South Africa', 'ZAF', 710, 27, 'AF', -30.559482, 22.937506, 'RSA', 1, 1039363, '2019-11-21 08:30:07', 1, '-22.126612', '32.895973', '-34.839828', '16.458021', '\0\0\0\0\0\0\0\0\0\0\0\0\0¬ŒF>¯r@@úGß¤i 6À¬ŒF>¯r@@î!á{kAÀ—á?Ý@u0@î!á{kAÀ—á?Ý@u0@úGß¤i 6À¬ŒF>¯r@@úGß¤i 6À', 'country_flag_za.png', 1, 0),
(249, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', 894, 260, 'AF', -13.133897, 27.849332, 'ZAM', 1, 1053939, '2019-11-21 08:30:07', 1, '-8.224360', '33.705704', '-18.079473', '21.999371', '\0\0\0\0\0\0\0\0\0\0\0\0\0T8‚TÚ@@¦]Pßr ÀT8‚TÚ@@>ê¯WX2À!’!ÇÖÿ5@>ê¯WX2À!’!ÇÖÿ5@¦]Pßr ÀT8‚TÚ@@¦]Pßr À', 'country_flag_zm.png', 1, 0),
(250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', 716, 263, 'AF', -19.015438, 29.154857, 'ZIM', 1, 1058677, '2019-11-21 08:30:07', 1, '-15.608835', '33.056305', '-22.417738', '25.237028', '\0\0\0\0\0\0\0\0\0\0\0\0\0÷Ì’\05‡@@\0R›8¹7/À÷Ì’\05‡@@äK¨àðj6À}<ôÝ­<9@äK¨àðj6À}<ôÝ­<9@\0R›8¹7/À÷Ì’\05‡@@\0R›8¹7/À', 'country_flag_zw.png', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `geocities`
--
ALTER TABLE `geocities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_code` (`state_code`),
  ADD KEY `accent` (`accent`),
  ADD KEY `latitude` (`latitude`),
  ADD KEY `longitude` (`longitude`),
  ADD KEY `order_display` (`order_display`),
  ADD KEY `name` (`name`),
  ADD KEY `country_code` (`country_code`) USING BTREE;

--
-- Indexes for table `geocountries`
--
ALTER TABLE `geocountries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `continent_code` (`continent_code`),
  ADD KEY `name` (`name`),
  ADD KEY `code` (`code`),
  ADD KEY `dialing_code` (`dialing_code`),
  ADD KEY `IDX_geocountries_ISO3` (`iso3`),
  ADD KEY `iso3` (`iso3`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `geocities`
--
ALTER TABLE `geocities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6453749;

--
-- AUTO_INCREMENT for table `geocountries`
--
ALTER TABLE `geocountries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
