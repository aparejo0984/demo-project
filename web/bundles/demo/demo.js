$(document).ready(function() { 

	$body = $("body");
	$(document).on({
    	ajaxStart: function() { $body.addClass("loading");    },
     	ajaxStop: function() { $body.removeClass("loading"); }    
	});

	$('#weatherTable').html('');   
    $('#country').on('change', function(){
    	$('#weatherTable').html(''); 
        var countryID = $(this).val();
        var cityURL = $("#pathURL").val();

  		var myData = new Object();
		myData['country'] = countryID;

        if(countryID){
            $.ajax({
                type:'POST',
                url: cityURL,
                data: myData,
                success:function(response){
					if(response.success){
                		$('#city').html(response.html);
						if($('#generate').is(':disabled')){
    						$("#generate").attr("disabled", false);
						}
                	}else{
                		$('#city').html('<option value="">No cities found</option>'); 
                		$("#generate").attr("disabled", true);
                	}
                }
            }); 
        }else{
            $('#city').html('<option value="">Select country first</option>'); 
        }
    });

    $('#generate').on('click', function(){
    	$('#weatherTable').html('');
        var generateURL = $("#generateURL").val();
  		var myData = new Object();
		myData['country'] = $("#country").val();
		myData['city'] = $('#city').val();

        $.ajax({
            type:'POST',
            url: generateURL,
            data: myData,
            success:function(response){
                console.log(response);
                if(response.success){
                	$('#weatherTable').html(response.html);
                }else{
                	$('#weatherTable').html(response.message);
                }
            }
        }); 
    });
});

